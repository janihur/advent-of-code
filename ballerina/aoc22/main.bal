import aoc22.day01;
import aoc22.day02;
import aoc22.day03;
import aoc22.day04;
import aoc22.day05;
import aoc22.day06;
import aoc22.day07;
import aoc22.day08;
import aoc22.day09;
import aoc22.day10;

import ballerina/constraint;
import ballerina/io;
import ballerina/test;

public type CommandLineOptions record {|
    @constraint:Int {
        minValue: 1,
        maxValue: 25
    }
    readonly int day;
    @constraint:Int {
        minValue: 1,
        maxValue: 2
    }
    int part?; // optional field can't be readonly
|};

public function main(*CommandLineOptions userGivenOptions) returns error? {
    final PuzzleId puzzleId = toPuzzleId(check constraint:validate(userGivenOptions));
    match puzzleId {
        { day: 1 } => { run(puzzleId, day01:run, expected = [69528, 206152]); }
        { day: 2 } => { run(puzzleId, day02:run, expected = [12794, 14979]); }
        { day: 3 } => { run(puzzleId, day03:run, expected = [7428, 2650]); }
        { day: 4 } => { run(puzzleId, day04:run, expected = [466, 865]); }
        { day: 5 } => { run(puzzleId, day05:run, expected = ["TLNGFGMFN", "FGLQJCMBD"]); }
        { day: 6 } => { run(puzzleId, day06:run, expected = [1876, 2202]); }
        { day: 7 } => { run(puzzleId, day07:run, expected = [1583951, 214171]); }
        { day: 8 } => { run(puzzleId, day08:run, expected = [1736, 268800]); }
        { day: 9 } => { run(puzzleId, day09:run, expected = [6026, 2273]); }
        { day: 10 } => { run(puzzleId, day10:run, expected = [13220, "RUAKHBEK"]); }
        { day: var day } => {
            return error(string`not implemented: (day ${day})`);
        }
    }
}

type PuzzleId record {|
    @constraint:Int {
        minValue: 1,
        maxValue: 25
    }
    readonly int day;
    readonly boolean part1;
    readonly boolean part2;
|};

function toPuzzleId(CommandLineOptions options) returns PuzzleId => {
    day: options.day,
    part1: options?.part is () || options?.part == 1,
    part2: options?.part is () || options?.part == 2
};

function run(
     PuzzleId puzzleId
    ,function(record {| boolean part1; boolean part2; |}) returns record {| (int|string)? part1; (int|string)? part2; |} runner
    ,(int|string)[2] expected
) {
    var answers = runner({ part1: puzzleId.part1, part2: puzzleId.part2 });

    var toStr = function ((int|string)? value) returns string { 
        if value is int {
            return value.toString();
        } else if value is string{
            return string`"${value}"`;
        } else {
            return "";
        }
    };

    if answers.part1 !is () {
        io:println(string`(day ${puzzleId.day})(part 1)(answer ${toStr(answers.part1)})`);
        test:assertEquals(answers.part1, expected[0], string`wrong answer to part 1`);
    }

    if answers.part2 !is () {
        io:println(string`(day ${puzzleId.day})(part 2)(answer ${toStr(answers.part2)})`);
        test:assertEquals(answers.part2, expected[1], string`wrong answer to part 2`);
    }
}
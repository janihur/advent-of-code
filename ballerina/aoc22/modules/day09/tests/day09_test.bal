import ballerina/test;

final Motion[] PUZZLE_DATA_PART1_1 = [
    { direction: RIGHT, steps: 4 },
    { direction: UP,    steps: 4 },
    { direction: LEFT,  steps: 3 },
    { direction: DOWN,  steps: 1 },
    { direction: RIGHT, steps: 4 },
    { direction: DOWN,  steps: 1 },
    { direction: LEFT,  steps: 5 },
    { direction: RIGHT, steps: 2 }
];

final Motion[] PUZZLE_DATA_PART1_2 = [
    { direction: LEFT,  steps: 4 },
    { direction: DOWN,  steps: 4 },
    { direction: RIGHT, steps: 3 },
    { direction: UP,    steps: 1 },
    { direction: LEFT,  steps: 4 },
    { direction: UP,    steps: 1 },
    { direction: RIGHT, steps: 5 },
    { direction: LEFT,  steps: 2 }
];

final Motion[] PUZZLE_DATA_PART2 = [
    { direction: RIGHT, steps: 5 },
    { direction: UP,    steps: 8 },
    { direction: LEFT,  steps: 8 },
    { direction: DOWN,  steps: 3 },
    { direction: RIGHT, steps: 17 },
    { direction: DOWN,  steps: 10 },
    { direction: LEFT,  steps: 25 },
    { direction: UP,    steps: 20 }
];

@test:Config {}
function test_isAdjacent() {
    test:assertEquals(isAdjacent({x:0, y:0},{x:0, y:0}), true);

    test:assertEquals(isAdjacent({x:0, y:0},{x:1, y:0}), true);
    test:assertEquals(isAdjacent({x:0, y:0},{x:0, y:1}), true);
    test:assertEquals(isAdjacent({x:0, y:0},{x:1, y:1}), true);

    test:assertEquals(isAdjacent({x:0, y:0},{x:2, y:1}), false);
    test:assertEquals(isAdjacent({x:0, y:0},{x:1, y:2}), false);

    test:assertEquals(isAdjacent({x:0, y:0},{x:-1, y:0}), true);
    test:assertEquals(isAdjacent({x:0, y:0},{x:0, y:-1}), true);
    test:assertEquals(isAdjacent({x:0, y:0},{x:-1, y:-1}), true);

    test:assertEquals(isAdjacent({x:0, y:0},{x:-2, y:-1}), false);
    test:assertEquals(isAdjacent({x:0, y:0},{x:-1, y:-2}), false);

    test:assertEquals(isAdjacent({x:-1, y:-1},{x:0, y:0}), true);
    test:assertEquals(isAdjacent({x:-1, y:-1},{x:1, y:0}), false);
    test:assertEquals(isAdjacent({x:-1, y:-1},{x:0, y:1}), false);
    test:assertEquals(isAdjacent({x:-1, y:-1},{x:1, y:1}), false);

    test:assertEquals(isAdjacent({x:-1, y:-1},{x:-3, y:-1}), false);
    test:assertEquals(isAdjacent({x:-1, y:-1},{x:-1, y:-3}), false);
}

@test:Config {}
function test_part_1() {
    test:assertEquals(part1(PUZZLE_DATA_PART1_1), 13);
    test:assertEquals(part1(PUZZLE_DATA_PART1_2), 13);
}

@test:Config {}
function test_part_2() {
    test:assertEquals(part2(PUZZLE_DATA_PART1_1), 1);
    test:assertEquals(part2(PUZZLE_DATA_PART1_2), 1);
    test:assertEquals(part2(PUZZLE_DATA_PART2), 36);
}

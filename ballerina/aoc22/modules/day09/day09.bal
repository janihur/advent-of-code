import ballerina/io;

enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT
}

type Motion record {|
    Direction direction;
    int steps;
|};

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; int? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2022-09");
    Motion[] puzzleData = stringData.map(function (string line) returns Motion {
        final int steps = checkpanic 'int:fromString(line.substring(2));
        if line[0] == "U" {
            return { direction: UP, steps };    
        } else if line[0] == "R" {
            return { direction: RIGHT, steps };    
        } else if line[0] == "D" {
            return { direction: DOWN, steps };    
        } else { // if line[0] == "L" {
            return { direction: LEFT, steps };    
        }
    });
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; int? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

// ----------------------------------------------------------------------------

type Coord record {|
    int x;
    int y;
|};

function newCoordWithMotion(Coord oldCoord, Motion motion) returns Coord {
    if motion.direction == UP {
        return { x: oldCoord.x, y: oldCoord.y + motion.steps };
    } else if motion.direction == RIGHT {
        return { x: oldCoord.x + motion.steps, y: oldCoord.y };
    } else if motion.direction == DOWN {
        return { x: oldCoord.x, y: oldCoord.y - motion.steps };
    } else { // if motion.direction == LEFT {
        return { x: oldCoord.x - motion.steps, y: oldCoord.y };
    }
}

function newCoordSingleStep(Coord oldCoord, Direction direction) returns Coord {
    if direction == UP {
        return { x: oldCoord.x, y: oldCoord.y + 1 };
    } else if direction == RIGHT {
        return { x: oldCoord.x + 1, y: oldCoord.y };
    } else if direction == DOWN {
        return { x: oldCoord.x, y: oldCoord.y - 1 };
    } else { // if direction == LEFT {
        return { x: oldCoord.x - 1, y: oldCoord.y };
    }
}

function isAdjacent(Coord a, Coord b) returns boolean {
    final int xdiff = 'int:abs(a.x - b.x);
    final int ydiff = 'int:abs(a.y - b.y);
    if xdiff > 1 || ydiff > 1 {
        return false;
    }
    return true;
}

// ----------------------------------------------------------------------------

function part1(Motion[] motions) returns int {
    Coord head = { x:0, y:0 };
    Coord tail = { x:0, y:0 };
 
    CoordMap tailsVisited = new;
    tailsVisited.push(tail);
 
    foreach Motion motion in motions {
        foreach var _ in 1 ... motion.steps { // single step motion
            Coord newHead = move(head, headSteps.get(motion.direction));//newCoordSingleStep(head, motion.direction);
            if !isAdjacent(tail, newHead) { // tail moves too
                tail = head.clone();
                tailsVisited.push(tail);
            }
            head = newHead.clone();
        }
    }
    return tailsVisited.length();
}

// ----------------------------------------------------------------------------

final map<Coord> headSteps = { 
    UP:    { x: 0, y: 1 },
    RIGHT: { x: 1, y: 0 },
    DOWN:  { x: 0, y:-1 },
    LEFT:  { x:-1, y: 0 }
};

function move(Coord position, Coord step) returns Coord => {
    x: position.x + step.x,
    y: position.y + step.y
};

function getTailStep(Coord head) returns Coord {
    match head {
        {x: 0, y: 2} => { return {x: 0,y: 1}; } // up
        {x: 1, y: 2} => { return {x: 1,y: 1}; } // up right
        {x: 2, y: 2} => { return {x: 1,y: 1}; } // up right right
        {x: 2, y: 1} => { return {x: 1,y: 1}; } // right top
        {x: 2, y: 0} => { return {x: 1,y: 0}; } // right middle
        {x: 2, y:-1} => { return {x: 1,y:-1}; } // right bottom
        {x: 2, y:-2} => { return {x: 1,y:-1}; } // down right right
        {x: 1, y:-2} => { return {x: 1,y:-1}; } // down right
        {x: 0, y:-2} => { return {x: 0,y:-1}; } // down
        {x:-1, y:-2} => { return {x:-1,y:-1}; } // down left
        {x:-2, y:-2} => { return {x:-1,y:-1}; } // down left left
        {x:-2, y:-1} => { return {x:-1,y:-1}; } // left bottom
        {x:-2, y: 0} => { return {x:-1,y: 0}; } // left middle
        {x:-2, y: 1} => { return {x:-1,y: 1}; } // left top
        {x:-2, y: 2} => { return {x:-1,y: 1}; } // up left left
        {x:-1, y: 2} => { return {x:-1,y: 1}; } // up left
        _ => { panic error(string`Missing match. This is implementation error: (head ${head.toString()})`); }
    }
}

function part2(Motion[] motions) returns int {
    // HEAD: rope[0]
    // TAIL: rope[9]
    Coord[] rope = [{x:0,y:0},{x:0,y:0},{x:0,y:0},{x:0,y:0},{x:0,y:0},{x:0,y:0},{x:0,y:0},{x:0,y:0},{x:0,y:0},{x:0,y:0}];

    CoordMap tailsVisited = new;
    tailsVisited.push(rope[9]);

    foreach Motion motion in motions {
        foreach var _ in 1 ... motion.steps { // single step motion
            rope[0] = move(rope[0], headSteps.get(motion.direction));
            foreach int i in 1...9 {
                Coord head = rope[i-1];
                Coord tail = rope[i];
                if !isAdjacent(tail, head) {
                    Coord step = getTailStep(
                        { x: head.x - tail.x, y: head.y - tail.y }
                    );
                    rope[i] = move(tail, step);
                } else {
                    break;
                }
            }
            tailsVisited.push(rope[9]);
        }
    }

    return tailsVisited.length();
}

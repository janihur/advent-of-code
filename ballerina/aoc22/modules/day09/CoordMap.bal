class CoordMap {
    private map<int> coords = {};
    function init() {
        
    }
    function push(Coord coord) {
        string key = string`${coord.x}:${coord.y}`;
        if self.coords.hasKey(key) {
            self.coords[key] = self.coords.get(key) + 1;
        } else {
            self.coords[key] = 1;
        }
    }
    function length() returns int {
        return self.coords.length();
    }
    function getCoords() returns readonly & map<int> {
        return self.coords.cloneReadOnly();
    }
}
import ballerina/io;

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; string? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] puzzleData = checkpanic io:fileReadLines("../../inputs/2022-10");
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; string? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

function part1(string[] program) returns int {
    int[] samplingCycles = [20, 60, 100, 140, 180, 220, 0];
    int samplingCycle = samplingCycles.shift();

    int[] signalStrengths = [];
    
    int regXValue = 1;
    int cycleCounter = 0;

    var sample = function() {
        if samplingCycles.length() > 0 {
            if cycleCounter == samplingCycle {
                signalStrengths.push(cycleCounter * regXValue);
                samplingCycle = samplingCycles.shift(); // panics on empty list
            }
        }
    };

    foreach var instruction in program {
        sample();
        if instruction.startsWith("noop") {
            cycleCounter += 1;
        } else { // addx
            cycleCounter += 1;
            sample();
            cycleCounter += 1;
            sample(); // adding this took me long time to realize :(
            int value = checkpanic 'int:fromString(instruction.substring(5));
            regXValue += value;
        }
    }

    return 'int:sum(...signalStrengths);
}

function part2(string[] program) returns string {
    string[][] crt = [
        [".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".","."],
        [".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".","."],
        [".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".","."],
        [".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".","."],
        [".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".","."],
        [".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".",".","."]
    ];

    int regXValue = 1; // the register represents sprite middle point
    int cycleCounter = 1;
    int crtRow = 0;
    int crtPixel = 0;

    var drawPixel = function() {
        if int:abs(regXValue - crtPixel) < 2 {
            crt[crtRow][crtPixel] = "#"; // lit
        } else {
            crt[crtRow][crtPixel] = "."; // dark
        }

        if cycleCounter % 240 == 0 { // begin of 1st row
            crtRow = 0;
            crtPixel = 0;
        } else if cycleCounter % 40 == 0 { // next row
            crtRow += 1;
            crtPixel = 0;
        } else { // next pixel
            crtPixel += 1;
        }
    };

    foreach var instruction in program {
        drawPixel();
        if instruction.startsWith("noop") {
            cycleCounter += 1;
        } else {
            cycleCounter += 1;
            drawPixel();
            cycleCounter += 1;
            int value = checkpanic 'int:fromString(instruction.substring(5));
            regXValue += value;
        }
    }

    foreach var row in crt {
        io:println(string:'join("", ...row));
    }
    
    return "RUAKHBEK"; // visually verified and then hardcoded
}

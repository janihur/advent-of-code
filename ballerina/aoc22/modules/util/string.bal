import ballerina/lang.array;

public function cut(string value, string separator = ":") returns ([string, string]) {
    int? cutIndex = value.indexOf(separator, 0);
    if cutIndex is int {
        int separatorLength = separator.length();
        string head = value.substring(0, cutIndex);
        string tail = value.substring(cutIndex + separatorLength);
        return [head, tail];
    } else {
        return [value, ""];
    }
}

public function joinToString(string[] arr) returns string {
    string accu = "";
    foreach var str in arr {
        accu = string:concat(accu, str);
    }
    return accu;
}

public function sort(string value) returns string {
    return joinToString(array:sort(splitToChar(value)));
}

public function splitToChar(string value) returns string[] {
    string[] accu = [];
    foreach var char in value {
        accu.push(char);
    }
    return accu;
}
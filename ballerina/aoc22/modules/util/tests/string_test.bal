import ballerina/test;

@test:Config {}
function test_cut_1() {
    test:assertEquals(cut(""), ["", ""]);
    test:assertEquals(cut(":"), ["", ""]);
    test:assertEquals(cut("::"), ["", ":"]);
    test:assertEquals(cut("key"), ["key", ""]);
    test:assertEquals(cut("key:"), ["key", ""]);
    test:assertEquals(cut("key:value"), ["key","value"]);
}

@test:Config {}
function test_cut_2() {
    test:assertEquals(cut("", ":separator:"), ["", ""]);
    test:assertEquals(cut(":separator:", ":separator:"), ["", ""]);
    test:assertEquals(cut(":separator::separator:", ":separator:"), ["", ":separator:"]);
    test:assertEquals(cut("key", ":separator:"), ["key", ""]);
    test:assertEquals(cut("key:separator:", ":separator:"), ["key", ""]);
    test:assertEquals(cut("key:separator:value", ":separator:"), ["key", "value"]);
}

@test:Config {}
function test_joinToString() {
    test:assertEquals(joinToString(["abc", "def", "ghi"]), "abcdefghi");
}

@test:Config {}
function test_sort() {
    test:assertEquals(sort("ihgfedcba"), "abcdefghi");
}

@test:Config {}
function test_splitToChar() {
    test:assertEquals(splitToChar("abc"), ["a", "b", "c"]);
}
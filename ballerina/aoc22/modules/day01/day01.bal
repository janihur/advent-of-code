import ballerina/io;
import ballerina/lang.array;

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; int? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2022-01");
    int?[] puzzleData = stringData.map(function (string line) returns int? {
        int|error t = 'int:fromString(line);
        return t is int ? t : ();
    });
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; int? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

function part1(int?[] caloryList) returns int {
    int maxCalories = 0;
    int currentCalories = 0;
    foreach var calory in caloryList {
        if calory is int {
            currentCalories += calory;
        } else {
            if currentCalories > maxCalories {
                maxCalories = currentCalories;
            }
            currentCalories = 0;
        }
    }
    return 'int:max(currentCalories, maxCalories);
}

// Saves 3 greatest added ints
class Top3Int {
    private int[3] n = [0, 0, 0];

    public function add(int n) {
        if n > self.n[0] {
            self.n[2] = self.n[1];
            self.n[1] = self.n[0];
            self.n[0] = n;
        } else if n > self.n[1] {
            self.n[2] = self.n[1];
            self.n[1] = n;
        } else if n > self.n[2] {
            self.n[2] = n;
        }
    }

    public function sum() returns int {
        return 'int:sum(...self.n);
    }
}

// version 2
function part2(int?[] caloryList) returns int {
    Top3Int top3ElfCalories = new;

    int currentElfCalories = 0;
    foreach var calory in caloryList {
        if calory is int {
            currentElfCalories += calory;
        } else {
            top3ElfCalories.add(currentElfCalories);
            currentElfCalories = 0;
        }
    }
    return top3ElfCalories.sum();
}

function part2v1(int?[] caloryList) returns int {
    int[] caloriesPerElf = [];

    int currentElfCalories = 0;
    foreach var calory in caloryList {
        if calory is int {
            currentElfCalories += calory;
        } else {
            caloriesPerElf.push(currentElfCalories);
            currentElfCalories = 0;
        }
    }
    return 'int:sum(...caloriesPerElf.sort(array:DESCENDING).slice(0,3));
}
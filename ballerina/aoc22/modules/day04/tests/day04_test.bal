import ballerina/test;

final [Range, Range][] PUZZLE_DATA = [
    [{min: 2, max: 4}, {min: 6, max: 8}],
    [{min: 2, max: 3}, {min: 4, max: 5}],
    [{min: 5, max: 7}, {min: 7, max: 9}],
    [{min: 2, max: 8}, {min: 3, max: 7}],
    [{min: 6, max: 6}, {min: 4, max: 6}],
    [{min: 2, max: 6}, {min: 4, max: 8}],
    [{min: 7, max: 7}, {min: 4, max: 6}]
];

@test:Config {}
function test_part_2() {
    test:assertEquals(part2(PUZZLE_DATA), 4);
}

import ballerina/io;
import aoc22.util;

type Range record {|
    int min;
    int max;
|};

function toRange(string s) returns Range {
    [string, string] t = util:cut(s, "-");
    return {
        min: checkpanic 'int:fromString(t[0]),
        max: checkpanic 'int:fromString(t[1])
    };
}

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; int? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2022-04");
    [Range, Range][] puzzleData = stringData.map(function (string line) returns [Range, Range] {
        [string, string] t = util:cut(line, ",");
        return [
            toRange(t[0]),
            toRange(t[1])
        ];
    });
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; int? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

function part1([Range, Range][] sectionAssignmentPairs) returns int {
    int overlaps = 0;
    foreach var assignmentPair in sectionAssignmentPairs {
        if assignmentPair[0].min >= assignmentPair[1].min &&
           assignmentPair[0].max <= assignmentPair[1].max {
            overlaps += 1;
        } else if assignmentPair[1].min >= assignmentPair[0].min &&
                  assignmentPair[1].max <= assignmentPair[0].max {
            overlaps += 1;
        }
    }
    return overlaps;
}

function part2([Range, Range][] sectionAssignmentPairs) returns int {
    return sectionAssignmentPairs.reduce(
        function (int total, [Range, Range] assignmentPair) returns int {
            return total + (
                (assignmentPair[0].max >= assignmentPair[1].min &&
                 assignmentPair[0].min <= assignmentPair[1].max   ) ? 1 : 0
            );
    }, 0);
}

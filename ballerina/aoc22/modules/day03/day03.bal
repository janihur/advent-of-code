import aoc22.util;
import ballerina/io;

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; int? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2022-03");
    [string, string][] puzzleData = stringData.map(function (string line) returns [string, string] {
        int breakPoint = line.length() / 2;
        return [line.substring(0, breakPoint), line.substring(breakPoint)];
    });
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; int? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

function getPriority(string item) returns int {
    // codepoint values
    // a - z : 97 - 122
    // A - Z : 65 - 90
    int cp = item.getCodePoint(0);
    if cp >= 97 { // lowecase
        return cp - 96;
    } else { // uppercase
        return cp - 38;
    }
}

function part1([string, string][] itemsOfAllRucksacks) returns int {
    int totalPriority = 0;
    foreach var rucksackItems in itemsOfAllRucksacks {
        util:StringSet firstCompartmentItems = new;
        foreach var item in rucksackItems[0] {
            firstCompartmentItems.add(item);
        }
        util:StringSet secondCompartmentItems = new;
        foreach var item in rucksackItems[1] {
            secondCompartmentItems.add(item);
        }
        util:StringSet commonItems = util:intersection(firstCompartmentItems, secondCompartmentItems);
        totalPriority += getPriority(commonItems.values()[0]);
    }
    return totalPriority;
}

function collectUniqueItems([string, string] items) returns util:StringSet {
    util:StringSet uniqueItems = new;
    foreach var item in items[0] {
        uniqueItems.add(item);
    }
    foreach var item in items[1] {
        uniqueItems.add(item);
    }
    return uniqueItems;
}

function part2([string, string][] itemsOfAllRucksacks) returns int {
    int totalPriority = 0;
    int i = 0;
    while i < itemsOfAllRucksacks.length() {
        util:StringSet firstElfItems = collectUniqueItems(itemsOfAllRucksacks[i]);
        i += 1;
        util:StringSet secondElfItems = collectUniqueItems(itemsOfAllRucksacks[i]);
        i += 1;
        util:StringSet thirdElfItems = collectUniqueItems(itemsOfAllRucksacks[i]);
        i += 1;

        util:StringSet commonItems = util:intersection(
            util:intersection(firstElfItems, secondElfItems),
            thirdElfItems
        );
        totalPriority += getPriority(commonItems.values()[0]);
    }
    return totalPriority;
}

import ballerina/regex;
import ballerina/io;

type Rearrangement record {|
    int amount;
    int 'from;
    int to;
|};

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| string? part1; string? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2022-05");
    
    string:Char[9][] crateStacksInitialSetup = [[],[],[],[],[],[],[],[],[]];
    do {
        [int, int][] stackColumnMap = [
            [0, 1],
            [1, 5],
            [2, 9],
            [3, 13],
            [4, 17],
            [5, 21],
            [6, 25],
            [7, 29],
            [8, 33]
        ];
        foreach var line in stringData.slice(0, 8).reverse() {
            foreach var pair in stackColumnMap {
                if line[pair[1]] != " " {
                    crateStacksInitialSetup[pair[0]].push(line[pair[1]]);
                }
            }
        }
    }

    Rearrangement[] rearrangementProcedure = stringData.slice(10).map(function (string line) returns Rearrangement {
        string[] t = regex:split(line, " ");
        return {
            amount: checkpanic 'int:fromString(t[1]),
            'from:  checkpanic 'int:fromString(t[3]) - 1,
            to:     checkpanic 'int:fromString(t[5]) - 1
        };
    });

    // run puzzle solvers -----------------------------------------------------
    final record {| string? part1; string? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(crateStacksInitialSetup, rearrangementProcedure);
    }
    if parts.part2 {
        answers.part2 = part2(crateStacksInitialSetup, rearrangementProcedure);
    }
    return answers;
}

function part1(string:Char[9][] crateStacksInitialSetup, Rearrangement[] rearrangementProcedure) returns string {
    var stacks = crateStacksInitialSetup.clone();

    foreach var rearrangement in rearrangementProcedure {
        foreach int move in 1...rearrangement.amount {
            var crate = stacks[rearrangement.'from].pop();
            stacks[rearrangement.to].push(crate);
        }
    }

    return stacks.reduce(function(string accu, string[] stack) returns string {
        return accu + stack.pop();
    }, "");
}

function part2(string:Char[9][] crateStacksInitialSetup, Rearrangement[] rearrangementProcedure) returns string {
    var stacks = crateStacksInitialSetup.clone();

    foreach var rearrangement in rearrangementProcedure {
        var numberOfCrates = rearrangement.amount;
        var fromStack = stacks[rearrangement.'from];
        var crates = fromStack.slice(fromStack.length() - numberOfCrates, fromStack.length());
        fromStack.setLength(fromStack.length() - numberOfCrates); // remove
        stacks[rearrangement.to].push(...crates);
    }

    return stacks.reduce(function(string accu, string[] stack) returns string {
        return accu + stack.pop();
    }, "");
}

import ballerina/io;

import ballerina/regex;
import aoc22.util;

type File record {|
    string name;
    int size;
|};

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; int? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2022-07");
    //var puzzleData = stringData;
    map<File[]> puzzleData = {};
    do {
        string cwd = "";
        foreach var line in stringData {
            if line.startsWith("$ cd") { // change directory
                string newDir = line.substring(5);
                match newDir {
                    "/" => { // go to root
                        cwd = "/"; 
                    }
                    ".." => { // go up one level
                        int? index = cwd.lastIndexOf("/");
                        if index is int {
                            if index == 0 { // will return to root
                                cwd = "/";
                            } else {
                                cwd = cwd.substring(0, index);
                            }
                        }
                    }
                    _ => { // enter to newDir
                        if cwd == "/" {
                            cwd += newDir;
                        } else {
                            cwd += "/" + newDir;
                        }
                    }
                }
            } else if line.startsWith("$ ls") { // list
                // nothing to do
            } else { // ls-command output
                if line.startsWith("dir") {
                    // skip
                } else { // file
                    string[] t = util:cut(line, " ");
                    File file = {
                        name: t[1],
                        size: checkpanic 'int:fromString(t[0])
                    };
                    if puzzleData.hasKey(cwd) {
                        puzzleData.get(cwd).push(file);
                    } else {
                        puzzleData[cwd] = [file];
                    }
                }
            }
            //io:println(cwd);
        }
    }
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; int? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

function countChar(string value, string:Char search) returns int {
    int count = 0;
    foreach var char in value {
        if char == search {
            count += 1;
        }
    }
    return count;
}

// from: /a/b/c
// to: /a/b/c
//     /a/b
//     /a
//     /
function allPathsOf(string path) returns string[] {
    string[] accu = [];
    string[] components = regex:split(path, "/");
    
    int len = components.length();
    foreach int i in 1 ... len {
        string[] x = components.slice(0, i);
        accu.push(string:'join("/", ...x));
    }

    if accu.length() > 0 && accu[0] == "" {
        accu[0] = "/";
    }

    return accu;
}

// FIXME: bug - add unit tests
function du(map<File[]> fileSystem) returns map<int> {
    map<int> directorySizes = {};

    foreach var dir in fileSystem.keys() {
        int dirSize = fileSystem.get(dir).reduce(function (int total, File file) returns int {
            return total + file.size;
        }, 0);

        foreach var dir2 in allPathsOf(dir) {
            if directorySizes.hasKey(dir2) {
                directorySizes[dir2] = directorySizes.get(dir2) + dirSize;
            } else {
                directorySizes[dir2] = dirSize;
            }
        }

    }

    return directorySizes;
}

function part1(map<File[]> fileSystem) returns int {
    map<int> directorySizes = du(fileSystem);

    return directorySizes.reduce(function (int total, int size) returns int {
        if size <= 100000 {
            return total + size;
        } else {
            return total;
        }
    }, 0);
}

function part2(map<File[]> fileSystem) returns int {
    final map<int> directorySizes = du(fileSystem);

    final int totalSpace = 70000000;
    final int recuiredSpace = 30000000;
    final int bugfix = 505645; // FIXME: du() bug: files of root dir are not included
    final int unusedSpace = totalSpace - directorySizes.get("/") - bugfix;
    final int spaceToBeFreed = recuiredSpace - unusedSpace;

    return (
        from int size in directorySizes
        where size >= spaceToBeFreed
        order by size ascending
        limit 1
        select size
    )[0];
}

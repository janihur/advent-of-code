import ballerina/io;

import aoc22.util;

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; int? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2022-06");
    string puzzleData = stringData[0];
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; int? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

// 1) sort the string
// 2) invalid marker if two consecutive chars are equal
function isStartOfPacketMarker(string candidate) returns boolean {
    string sorted = util:sort(candidate);
    return (sorted[0] != sorted[1]) && (sorted[1] != sorted[2]) && (sorted[2] != sorted[3]);
}

function part1(string dataStreamBuffer) returns int {
    int wbeg = 0; // observation window begin index
    int wend = 3; // observation window end index
    while wend < dataStreamBuffer.length() - 1 {
        if isStartOfPacketMarker(dataStreamBuffer.substring(wbeg, wend + 1)) {
            return wend + 1;
        }
        wbeg += 1;
        wend += 1;
    }
    return 0;
}

// 1) sort the string
// 2) invalid marker if two consecutive chars are equal
function isStartOfMessageMarker(string candidate) returns boolean {
    string sorted = util:sort(candidate);
    int i = 1;
    while i < sorted.length() - 1 {
        if sorted[i-1] == sorted[i] {
            return false;
        }
        i += 1;
    }
    return true;
}

function part2(string dataStreamBuffer) returns int {
    int wbeg =  0; // observation window begin index
    int wend = 13; // observation window end index
    while wend < dataStreamBuffer.length() - 1 {
        if isStartOfMessageMarker(dataStreamBuffer.substring(wbeg, wend + 1)) {
            return wend + 1;
        }
        wbeg += 1;
        wend += 1;
    }
    return 0;
}

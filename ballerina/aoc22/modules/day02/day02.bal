import ballerina/io;

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; int? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] puzzleData = checkpanic io:fileReadLines("../../inputs/2022-02");
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; int? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

function part1(string[] strategyGuide) returns int {
    map<int> scores = {
        // rock (A, X) paper (B, Y) scissors(C, Z) -> conclude result
        "A X": 1 + 3, // rock - rock     -> draw
        "A Y": 2 + 6, // rock - paper    -> win
        "A Z": 3 + 0, // rock - scissors -> lose
        "B X": 1 + 0, // paper - rock     -> lose
        "B Y": 2 + 3, // paper - paper    -> draw
        "B Z": 3 + 6, // paper - scissors -> win
        "C X": 1 + 6, // scissors - rock     -> win
        "C Y": 2 + 0, // scissors - paper    -> lose
        "C Z": 3 + 3  // scissors - scissors -> draw
    };
    return strategyGuide.reduce(function(int total, string round) returns int {
        return total + scores.get(round);
    }, 0);
}

function part2(string[] strategyGuide) returns int {
    map<int> scores = {
        // rock (A) paper (B) scissors (C)
        // lose (X) draw (Y) win (Z)
        // -> conclude your choise (rock, paper, scissors)
        "A X": 3 + 0, // lose -> rock - scissors
        "A Y": 1 + 3, // draw -> rock - rock
        "A Z": 2 + 6, // win  -> rock - paper
        "B X": 1 + 0, // lose -> paper - rock
        "B Y": 2 + 3, // draw -> paper - paper
        "B Z": 3 + 6, // win  -> paper - scissors
        "C X": 2 + 0, // lose -> scissors - paper
        "C Y": 3 + 3, // draw -> scissors - scissors
        "C Z": 1 + 6  // win  -> scissors - rock
    };
    return strategyGuide.reduce(function(int total, string round) returns int {
        return total + scores.get(round);
    }, 0);
}

import ballerina/test;

final int[][] PUZZLE_DATA = [
    [3, 0, 3, 7, 3],
    [2, 5, 5, 1, 2],
    [6, 5, 3, 3, 2],
    [3, 3, 5, 4, 9],
    [3, 5, 3, 9, 0]
];

@test:Config {}
function test_part_1() {
    test:assertEquals(part1(PUZZLE_DATA), 21);
}

@test:Config {}
function test_part_2() {
    test:assertEquals(part2(PUZZLE_DATA), 8);
}

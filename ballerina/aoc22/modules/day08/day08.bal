import aoc22.util;
import ballerina/io;

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; int? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2022-08");
    int[][] puzzleData = stringData.map(function (string line) returns int[] {
        int[] heights = [];
        foreach var height in line {
            heights.push(checkpanic 'int:fromString(height));
        }
        return heights;
    });
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; int? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

type Coord record {|
    int x;
    int y;
|};

function isVisible(int[][] heightMap, int y, int x, int mapHeight, int mapWidth) returns boolean {
    // io:print(heightMap[y][x], ":");
    final int height = heightMap[y][x];

    // look north
    var visibleFromNorth = function() returns boolean {
        foreach int i in 0 ... y-1 {
            // io:print(string`(N:${heightMap[i][x]})`);
            if height <= heightMap[i][x] {
                return false;
            }
        }
        return true;
    };

    if visibleFromNorth() {
        return true;
    }

    // look east
    var visibleFromEast = function() returns boolean {
        foreach int i in x+1 ..< mapWidth {
            // io:print(string`(E:${heightMap[y][i]})`);
            if height <= heightMap[y][i] {
                return false;
            }
        }
        return true;
    };

    if visibleFromEast() {
        return true;
    }
    
    // look south
    var visibleFromSouth = function() returns boolean {
        foreach int i in y+1 ..< mapHeight {
            // io:print(string`(S:${heightMap[i][x]})`);
            if height <= heightMap[i][x] {
                return false;
            }
        }
        return true;
    };

    if visibleFromSouth() {
        return true;
    }

    // look west
    var visibleFromWest = function() returns boolean {
        foreach int i in 0 ... x-1 {
            // io:print(string`(W:${heightMap[y][i]})`);
            if height <= heightMap[y][i] {
                return false;
            }
        }
        return true;
    };

    if visibleFromWest() {
        return true;
    }

    // io:println();
    return false;
}

function getVisibleTrees(int[][] heightMap, int mapHeight, int mapWidth) returns Coord[] {
    final int xMin = 1;
    final int xMax = mapWidth - 1;
    final int yMin = 1;
    final int yMax = mapHeight - 1;

    Coord[] visibleTrees = [];

    foreach int y in yMin..<yMax {
        // io:println(heightMap[y]);
        foreach int x in xMin..<xMax {
            boolean isVisible_ = isVisible(heightMap, y, x, mapHeight, mapWidth);
            if isVisible_ {
                visibleTrees.push({x, y});
            }
        }
        // io:println();
    }

    return visibleTrees;
}

function part1(int[][] heightMap) returns int {
    final int width = heightMap[0].length();
    final int height = heightMap.length();
    final int edgeCount = (2 * width) + (2 * (height - 2));

    Coord[] visibleTrees = getVisibleTrees(heightMap, height, width);

    return edgeCount + visibleTrees.length();
}

function mul(int... xList) returns int {
    int r = 1;
    foreach var x in xList {
        r = r * x;
    }
    return r;
}

function getScenicScore(int[][] heightMap, Coord coord) returns int {
    int[] scenicScores = [];

    final int height = heightMap[coord.y][coord.x];

    final int xMin = 0;
    final int xMax = heightMap[0].length();
    final int yMin = 0;
    final int yMax = heightMap.length();

    int distance = 0;
    
    // north
    foreach int i in util:indexRange(coord.y, yMin) {
        distance += 1;
        if height <= heightMap[i][coord.x] {
            break;
        }
    }
    scenicScores.push(distance);
    distance = 0;

    // east
    foreach int i in util:indexRange(coord.x+1, xMax) {
        distance += 1;
        if height <= heightMap[coord.y][i] {
            break;
        }
    }
    scenicScores.push(distance);
    distance = 0;

    // south
    foreach int i in util:indexRange(coord.y+1, yMax) {
        distance += 1;
        if height <= heightMap[i][coord.x] {
            break;
        }
    }
    scenicScores.push(distance);
    distance = 0;

    // west
    foreach int i in util:indexRange(coord.x, xMin) {
        distance += 1;
        if height <= heightMap[coord.y][i] {
            break;
        }
    }
    scenicScores.push(distance);
    distance = 0;

    return mul(...scenicScores);
}

function part2(int[][] heightMap) returns int {
    final int width = heightMap[0].length();
    final int height = heightMap.length();

    Coord[] visibleTrees = getVisibleTrees(heightMap, height, width);

    int topScenicScore = 0;

    foreach var coord in visibleTrees {
        final int scenicScore = getScenicScore(heightMap, coord);
        if scenicScore > topScenicScore {
            topScenicScore = scenicScore;
        }
    }

    return topScenicScore;
}

#!/bin/bash

# set -x

# usage: add-module.sh <YEAR_2_DIGITS> <DAY>

declare -r YEAR="$1"; shift
declare -r DAY=$(printf "%02d" "$1"); shift

declare -r package="aoc${YEAR}"
declare -r module="day${DAY}"

if [ ! -d ${package} ]
then
    echo "package directory doesn't exist: ${package}"
    exit 1
fi

if [ -d ${package}/modules/${module} ]
then
    echo "module already exists: ${package}/modules/${module}"
    exit 1
fi

# own module template
declare -r templates=templates/22

mkdir -p ${package}/modules/${module}/tests
awk -v YEAR=${YEAR} -v DAY=${DAY} -- '{gsub(/<YEAR>/, YEAR); gsub(/<DAY>/, DAY)}1' ${templates}/template.bal > ${package}/modules/${module}/${module}.bal
cp ${templates}/tests/template_test.bal ${package}/modules/${module}/tests/${module}_test.bal

echo "added the following module:"
tree ${package}/modules/${module}/

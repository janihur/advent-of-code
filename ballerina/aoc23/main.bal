import aoc23.day01;
import aoc23.day02;
import aoc23.day03;
import aoc23.day04;
import aoc23.day05;

import ballerina/constraint;
import ballerina/io;
import ballerina/test;

public type CommandLineOptions record {|
    @constraint:Int {
        minValue: 1,
        maxValue: 25
    }
    readonly int day;
    @constraint:Int {
        minValue: 1,
        maxValue: 2
    }
    int part?; // optional field can't be readonly
|};

public function main(*CommandLineOptions userGivenOptions) returns error? {
    final PuzzleId puzzleId = toPuzzleId(check constraint:validate(userGivenOptions));
    match puzzleId {
        {day: 1} => {
            run(puzzleId, day01:run, expected = [53194, 54249]);
        }
        {day: 2} => {
            run(puzzleId, day02:run, expected = [2716, 72227]);
        }
        {day: 3} => {
            run(puzzleId, day03:run, expected = [528799, 0]);
        }
        {day: 4} => {
            run(puzzleId, day04:run, expected = [23750, 13261850]);
        }
        {day: 5} => {
            run(puzzleId, day05:run, expected = [403695602, 0]);
        }
        {day: var day} => {
            return error(string `not implemented: (day ${day})`);
        }
    }
}

type PuzzleId record {|
    @constraint:Int {
        minValue: 1,
        maxValue: 25
    }
    readonly int day;
    readonly boolean part1;
    readonly boolean part2;
|};

function toPuzzleId(CommandLineOptions options) returns PuzzleId => {
    day: options.day,
    part1: options?.part is () || options?.part == 1,
    part2: options?.part is () || options?.part == 2
};

function run(
        PuzzleId puzzleId
    , function (record {|boolean part1; boolean part2;|}) returns record {|(int|string)? part1; (int|string)? part2;|} runner
    , (int|string)[2] expected
) {
    var answers = runner({part1: puzzleId.part1, part2: puzzleId.part2});

    var toStr = function((int|string)? value) returns string {
        if value is int {
            return value.toString();
        } else if value is string {
            return string `"${value}"`;
        } else {
            return "";
        }
    };

    if answers.part1 !is () {
        io:println(string `(day ${puzzleId.day})(part 1)(answer ${toStr(answers.part1)})`);
        test:assertEquals(answers.part1, expected[0], string `wrong answer to part 1`);
    }

    if answers.part2 !is () {
        io:println(string `(day ${puzzleId.day})(part 2)(answer ${toStr(answers.part2)})`);
        test:assertEquals(answers.part2, expected[1], string `wrong answer to part 2`);
    }
}

import ballerina/io;
import ballerina/lang.regexp;

function unwrapGroup(regexp:Groups? g) returns string? {
    string? str = ();
    if g is regexp:Groups {
        regexp:Span? span = g[1];
        if span !is () {
            str = span.substring();
        }
    }
    return str;
}

function strToInt(string? s) returns int {
    if s is string {
        return checkpanic int:fromString(s);
    }
    return 0;
}

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; int? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2023-02");
    Game[] puzzleData = stringData.map(function (string line0) returns Game {
        string[] line1 = re`: `.split(line0);
        string[] line2 = re`;`.split(line1[1]);

        Set[] sets = [];

        foreach string set in line2 {
            Set set2 = {
                red: 0,
                green: 0,
                blue: 0
            };

            var findBy = function(string:RegExp re) returns int {
                string? s = unwrapGroup(re.findGroups(set));
                return strToInt(s);
            };

            set2.red   = findBy(re`(\d+)\sred`);
            set2.green = findBy(re`(\d+)\sgreen`);
            set2.blue  = findBy(re`(\d+)\sblue`);

            sets.push(set2);
        }

        return {
            id: checkpanic int:fromString(line1[0].substring(5)),
            sets: sets
        };
    });
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; int? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

type Set record {
    int red;
    int green;
    int blue;
};

type Game record {
    int id;
    Set[] sets;
};

function part1(Game[] games) returns int {
    Game[] possibles = games.filter(function (Game game) returns boolean {
        foreach Set set in game.sets {
            if set.red > 12 || set.green > 13 || set.blue > 14 {
                return false;
            }
        }
        return true;
    });
    return sum(possibles.'map(n => n.id));
}

function part2(Game[] games) returns int {
    Set[] minSets = games.'map(function (Game game) returns Set {
        return game.sets.reduce(function (Set accu, Set curr) returns Set {
            if curr.red > accu.red {
                accu.red = curr.red;
            }
            if curr.green > accu.green {
                accu.green = curr.green;
            }
            if curr.blue > accu.blue {
                accu.blue = curr.blue;
            }
            return accu;
        },{
            red:0,
            green:0,
            blue:0
        });
    });
    return sum(minSets.'map(set => set.red * set.green * set.blue));
}

function sum(int[] arr) returns int {
    return arr.reduce(function(int accu, int n) returns int {
        return accu + n;
    }, 0);
}

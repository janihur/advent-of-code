import ballerina/constraint;
import ballerina/io;
import aoc23.util;

type PartNumberCandidate record {|
    string number;
    Coord[] coords;
|};

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; int? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2023-03");
    int y = 0;
    EngineSchematic puzzleData = checkpanic constraint:validate(
        stringData.reduce(function (EngineSchematic accu, string line) returns EngineSchematic {
            int x = 0;
            PartNumberCandidate? numberCandidate = ();
            foreach string:Char char in line {
                match char {
                    "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9" => { // part of a part number
                        if numberCandidate is () {
                            numberCandidate = { number: char, coords: [{x, y}]};
                        } else {
                            numberCandidate.number += char;
                            numberCandidate.coords.push({x, y});
                        }
                    }
                    "." => { // skip
                        if numberCandidate !is () {
                            accu.partNumbers.push({
                                number: checkpanic int:fromString(numberCandidate.number),
                                coords: numberCandidate.coords
                            });
                        }
                        numberCandidate = ();
                    }
                    _ => { // symbol
                        if numberCandidate !is () {
                            accu.partNumbers.push({
                                number: checkpanic int:fromString(numberCandidate.number),
                                coords: numberCandidate.coords
                            });
                        }
                        numberCandidate = ();
                        accu.symbols.push({symbol: char, coord: {x, y}});
                    }
                }
                x += 1;
            }
            if numberCandidate !is () {
                accu.partNumbers.push({
                    number: checkpanic int:fromString(numberCandidate.number),
                    coords: numberCandidate.coords
                });
            }
            y += 1;
            return accu;
        }, {partNumbers: [], symbols: []})
    );
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; int? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

// ----------------------------------------------------------------------------

type EngineSchematic record {|
    PartNumber[] partNumbers;
    Symbol[] symbols;
|};
type PartNumber record {|
    @constraint:Int {minValue: 0} int number;
    Coord[] coords;
|};

type Symbol record {|
    @constraint:String {length: 1} string symbol;
    Coord coord;
|};

type Coord record {|
    @constraint:Int {minValue: 0, maxValue: 139} int x;
    @constraint:Int {minValue: 0, maxValue: 139} int y;
|};

// ----------------------------------------------------------------------------

function isAdjacent(Coord a, Coord b) returns boolean {
    final int xdiff = 'int:abs(a.x - b.x);
    final int ydiff = 'int:abs(a.y - b.y);
    if xdiff > 1 || ydiff > 1 {
        return false;
    }
    return true;
}

function getAdjacentCoords(Coord c, int xMin = 0, int xMax = 139, int yMin = 0, int yMax = 139) returns Coord[] {
    Coord[] adjacents = [];
    Coord candidate = {x: c.x-1, y: c.y-1};
    var isValid = function () returns boolean => candidate.x >= xMin && candidate.x <= xMax && candidate.y >= yMin && candidate.y <= yMax;
    
    if isValid() {adjacents.push(candidate);}
    candidate = {x: c.x  , y: c.y-1};
    if isValid() {adjacents.push(candidate);}
    candidate = {x: c.x+1, y: c.y-1};
    if isValid() {adjacents.push(candidate);}
    candidate = {x: c.x-1, y: c.y };
    if isValid() {adjacents.push(candidate);}
    candidate = {x: c.x+1, y: c.y };
    if isValid() {adjacents.push(candidate);}
    candidate = {x: c.x-1, y: c.y+1};
    if isValid() {adjacents.push(candidate);}
    candidate = {x: c.x  , y: c.y+1};
    if isValid() {adjacents.push(candidate);}
    candidate = {x: c.x+1, y: c.y+1};
    if isValid() {adjacents.push(candidate);}
    
    return adjacents;
}
// ----------------------------------------------------------------------------
function part1(EngineSchematic engineSchematic) returns int {
    Coord[] symbolCoords = 
        from Symbol symbol in engineSchematic.symbols
        select symbol.coord;

    var isPartNumber = function(PartNumber partNumber) returns boolean {
        foreach Coord numberCoord in partNumber.coords {
            foreach Coord adjacent in getAdjacentCoords(numberCoord) {
                Coord[] matches = 
                    from Coord symbol in symbolCoords
                    where (adjacent.x == symbol.x) && (adjacent.y == symbol.y)
                    select symbol;
                if matches.length() > 0 {
                    return true;
                }
            }
        }
        return false;
    };

    int[] actualPartNumbers = engineSchematic.partNumbers
        .filter( x => isPartNumber(x))
        .'map(x => x.number);

    return util:sum(actualPartNumbers);
}

type PartNumberT record {|
    readonly Coord coord;
    readonly int number;
|};

// TODO: WIP because a compiler bug:
// https://github.com/ballerina-platform/ballerina-lang/issues/41819
function part2(EngineSchematic engineSchematic) returns int {
    // Coord[] allGearSymbols =
    //     from Symbol symbol in engineSchematic.symbols
    //     where symbol.symbol == "*"
    //     select symbol.coord;

    foreach PartNumber partNumber in engineSchematic.partNumbers {
        io:println(partNumber);
    }

    table<PartNumberT> key(coord) partNumbers = table key(coord) [];
    foreach PartNumber partNumber in engineSchematic.partNumbers {
        if partNumber.number == 739 {
            io:println("processing 739");
        }
        foreach Coord coord in partNumber.coords {
            if partNumber.number == 739 {
                io:println("processing 739 coordinate: ", coord);
            }
            partNumbers.add({
                coord: coord.cloneReadOnly(),
                number: partNumber.number
            });
        }
    }
    foreach PartNumberT partNumber in partNumbers {
        io:println(partNumber);
    }
    //io:println(partNumbers);

    int x2 = 0;
    // foreach Coord gearSymbolCoord in allGearSymbols {
    //     int[] x = from Coord adjacent in getAdjacentCoords(gearSymbolCoord)
    //     join PartNumberT partNumber in partNumbers on adjacent.cloneReadOnly() equals partNumber.coord
    //     order by partNumber.number
    //     select partNumber.number;
    //     //io:print(gearSymbolCoord);
    //     util:IntSet intSet = new;
    //     intSet.addArray(x);
    //     //io:print(x);
    //     //io:print(intSet);
    //     int[] unique = intSet.values();
    //     if unique.length() == 2 {
    //         x2 += unique[0] * unique[1];
    //         //io:print(" ",x2);
    //     }
    //     //io:println();
    // }

    // from PartNumber partNumber in engineSchematic.partNumbers
    // select {}

    // //io:println(allGearSymbols);
    // var fun1 = function(Coord gearSymbolCoord) returns int {
    //     foreach Coord adjacent in getAdjacentCoords(gearSymbolCoord) {
    //         foreach PartNumber partNumber in engineSchematic.partNumbers {
                
    //         }
    //         // PartNumber[] partNumbers = from PartNumber partNumber in engineSchematic.partNumbers
    //         // where partNumber.
    //         // select partNumber;
    //     }
    // };
    //int x = 0;
    // foreach Coord gearSymbolCoord in allGearSymbols {
    //     x += fun1(gearSymbolCoord);
    // }
    return x2;
}

public class IntSet {
    map<boolean> set;

    public function init() {
        self.set = {};
    }

    public function add(int value) {
        //string key = value.toString();
        self.set[value.toString()] = true;
    }
    public function addArray(int[] array) {
        foreach int value in array {
            self.add(value);
        }
    }

    public function has(int value) returns boolean {
        boolean? hasValue = self.set[value.toString()];
        return hasValue is () ? false : hasValue;
    }

    public function values() returns int[] {
        int[] accu = [];
        foreach var key in self.set.keys().sort() {
            accu.push(checkpanic int:fromString(key));
        }
        return accu;
    }

    // public function remove(string value) {
    //     // check for existence first because removing non-existing value will panic
    //     if self.has(value) {
    //         var _ = self.set.remove(value);
    //     }
    // }

    public function toString() returns string {
        return self.values().toString();
    }
}

public function union(IntSet a, IntSet b) returns IntSet {
    IntSet c = new;
    foreach var item in a.values() {
        c.add(item);
    }
    foreach var item in b.values() {
        c.add(item);
    }
    return c;
}

public function intersection(IntSet a, IntSet b) returns IntSet {
    IntSet c = new;
    foreach var item in a.values() {
        if b.has(item) {
            c.add(item);
        }
    }
    foreach var item in b.values() {
        if a.has(item) {
            c.add(item);
        }
    }
    return c;
}

// a - b = c
public function difference(IntSet a, IntSet b) returns IntSet {
    IntSet c = new;
    foreach var item in a.values() {
        if !b.has(item) {
            c.add(item);
        }
    }
    return c;    
}
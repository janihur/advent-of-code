import aoc23.util;
import ballerina/io;

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; int? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2023-04");
    Scratchcard[] puzzleData = [];
    foreach string line in stringData {
        string[] splittedLine = re`\s\|\s`.split(re`:\s`.split(line)[1]);
        string[] winningNumbers = re`\s+`.split(splittedLine[0].trim());
        string[] myNumbers = re`\s+`.split(splittedLine[1].trim());

        var toInt = function(string s) returns int => checkpanic int:fromString(s);
        puzzleData.push({
            winningNumbers: winningNumbers.'map(toInt).sort(),
            myNumbers: myNumbers.'map(toInt).sort()
        });
    }
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; int? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

type Scratchcard record {|
    int[] winningNumbers;
    int[] myNumbers;
|};

function part1(Scratchcard[] scratchcards) returns int {
    return scratchcards.reduce(function (int accu, Scratchcard card) returns int {
        util:IntSet a = new; a.addArray(card.myNumbers);
        util:IntSet b = new; b.addArray(card.winningNumbers);
        util:IntSet c = util:intersection(a, b); // wins on card
        match c.values().length() {
            0     => { return accu; }
            1     => { return accu + 1; }
            var p => { return accu + util:power(2, p-1); }
        }
    }, 0);
}

function part2(Scratchcard[] scratchcards) returns int {
    int[] wins = scratchcards.'map(function (Scratchcard card) returns int {
        util:IntSet a = new; a.addArray(card.myNumbers);
        util:IntSet b = new; b.addArray(card.winningNumbers);
        util:IntSet c = util:intersection(a, b); // wins on card
        return c.values().length();
    });
    int[214] cards = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
    foreach int i in 0..<wins.length() {
        int winsOnCard = wins[i];
        foreach int j in i+1...i+winsOnCard {
            cards[j] += cards[i];
        }
    }
    return util:sum(cards);
}

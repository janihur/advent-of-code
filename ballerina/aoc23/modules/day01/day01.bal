import ballerina/io;

public function run(
        record {|boolean part1; boolean part2;|} parts
) returns record {|int? part1; int? part2;|} {
    // read puzzle data -------------------------------------------------------
    string[] puzzleData = checkpanic io:fileReadLines("../../inputs/2023-01");
    // run puzzle solvers -----------------------------------------------------
    final record {|int? part1; int? part2;|} answers = {part1: (), part2: ()};
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

function part1(string[] calibrationDocument) returns int {
    int[] calibrationValues = calibrationDocument.map(function(string line) returns int {
        string allNumbers = "";
        foreach string c in line {
            match c {
                "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9" => {
                    allNumbers = string:concat(allNumbers, c);
                }
            }
        }
        string firstDigit = allNumbers[0];
        string lastDigit = allNumbers[allNumbers.length() - 1];
        return checkpanic int:fromString(firstDigit + lastDigit);
    });
    return sum(calibrationValues);
}

// note the numbers spelled with letters might overlap, e.g.
// eightwo
// eighthree
function part2(string[] calibrationDocument) returns int {
    int[] calibrationValues = calibrationDocument.map(function(string line) returns int {
        string allNumbers = "";
        foreach int i in 0 ..< line.length() {
            string char = line[i];
            match char {
                "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9" => {
                    allNumbers = string:concat(allNumbers, char);
                }
                "o"|"t"|"f"|"s"|"e"|"n" => {
                    if line.substring(i).startsWith("one") {
                        allNumbers = allNumbers.concat("1");
                    } else if line.substring(i).startsWith("two") {
                        allNumbers = allNumbers.concat("2");
                    } else if line.substring(i).startsWith("three") {
                        allNumbers = allNumbers.concat("3");
                    } else if line.substring(i).startsWith("four") {
                        allNumbers = allNumbers.concat("4");
                    } else if line.substring(i).startsWith("five") {
                        allNumbers = allNumbers.concat("5");
                    } else if line.substring(i).startsWith("six") {
                        allNumbers = allNumbers.concat("6");
                    } else if line.substring(i).startsWith("seven") {
                        allNumbers = allNumbers.concat("7");
                    } else if line.substring(i).startsWith("eight") {
                        allNumbers = allNumbers.concat("8");
                    } else if line.substring(i).startsWith("nine") {
                        allNumbers = allNumbers.concat("9");
                    }
                }
            }
        }
        string firstDigit = allNumbers[0];
        string lastDigit = allNumbers[allNumbers.length() - 1];
        return checkpanic int:fromString(firstDigit + lastDigit);
    });
    return sum(calibrationValues);
}

function sum(int[] arr) returns int {
    return arr.reduce(function(int accu, int n) returns int {
        return accu + n;
    }, 0);
}

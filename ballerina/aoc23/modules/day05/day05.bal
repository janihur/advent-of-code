import ballerina/io;

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; int? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2023-05");
    Almanac puzzleData = { seeds: [], maps: table[]};
    var toInt = function(string s) returns int => checkpanic int:fromString(s);
    int i = 0;
    {
        string[] seeds = re`\s`.split(stringData[i].substring(7));
        puzzleData.seeds = seeds.'map(toInt);
    }
    i += 3;
    while true {
        string line = stringData[i];
        if line.length() == 0 { break; }
        string[] ranges = re`\s`.split(line);
        puzzleData.maps.add({
            step: 1,
            mapType: "seed-to-soil",
            destinationRangeStart: toInt(ranges[0]),
            sourceRangeStart: toInt(ranges[1]),
            rangeLength: toInt(ranges[2])
        });
        i += 1;
    }
    i += 2;
    while true {
        string line = stringData[i];
        if line.length() == 0 { break; }
        string[] ranges = re`\s`.split(line);
        puzzleData.maps.add({
            step: 2,
            mapType: "soil-to-fertilizer",
            destinationRangeStart: toInt(ranges[0]),
            sourceRangeStart: toInt(ranges[1]),
            rangeLength: toInt(ranges[2])
        });
        i += 1;
    }
    i += 2;
    while true {
        string line = stringData[i];
        if line.length() == 0 { break; }
        string[] ranges = re`\s`.split(line);
        puzzleData.maps.add({
            step: 3,
            mapType: "fertilizer-to-water",
            destinationRangeStart: toInt(ranges[0]),
            sourceRangeStart: toInt(ranges[1]),
            rangeLength: toInt(ranges[2])
        });
        i += 1;
    }
    i += 2;
    while true {
        string line = stringData[i];
        if line.length() == 0 { break; }
        string[] ranges = re`\s`.split(line);
        puzzleData.maps.add({
            step: 4,
            mapType: "water-to-light",
            destinationRangeStart: toInt(ranges[0]),
            sourceRangeStart: toInt(ranges[1]),
            rangeLength: toInt(ranges[2])
        });
        i += 1;
    }
    i += 2;
    while true {
        string line = stringData[i];
        if line.length() == 0 { break; }
        string[] ranges = re`\s`.split(line);
        puzzleData.maps.add({
            step: 5,
            mapType: "light-to-temperature",
            destinationRangeStart: toInt(ranges[0]),
            sourceRangeStart: toInt(ranges[1]),
            rangeLength: toInt(ranges[2])
        });
        i += 1;
    }
    i += 2;
    while true {
        string line = stringData[i];
        if line.length() == 0 { break; }
        string[] ranges = re`\s`.split(line);
        puzzleData.maps.add({
            step: 6,
            mapType: "temperature-to-humidity",
            destinationRangeStart: toInt(ranges[0]),
            sourceRangeStart: toInt(ranges[1]),
            rangeLength: toInt(ranges[2])
        });
        i += 1;
    }
    i += 2;
    while true {
        if i >= stringData.length() { break; }
        string line = stringData[i];
        //if line.length() == 0 { break; }
        string[] ranges = re`\s`.split(line);
        puzzleData.maps.add({
            step: 7,
            mapType: "humidity-to-location",
            destinationRangeStart: toInt(ranges[0]),
            sourceRangeStart: toInt(ranges[1]),
            rangeLength: toInt(ranges[2])
        });
        i += 1;
    }
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; int? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

type Almanac record {|
    int[] seeds;
    table<Map> maps;
|};

type Map record {|
    int step;
    string mapType;
    int destinationRangeStart;
    int sourceRangeStart;
    int rangeLength;
|};

function find(Almanac almanac, string type_, int source_) returns int {
    int[] destination = from Map m in almanac.maps
        where m.mapType == type_
        let int sourceMin = m.sourceRangeStart
        let int sourceMax = sourceMin + m.rangeLength - 1
        where source_ >= sourceMin && source_ <= sourceMax
        order by sourceMin
        select m.destinationRangeStart + (source_ - m.sourceRangeStart);
    if destination.length() == 0 {
        destination = [source_];
    }
    return destination[0];
}

function part1(Almanac almanac) returns int {
    var find = function(string type_, int source_) returns int {
        return find(almanac, type_, source_);
    };
    int[] locations = almanac.seeds.'map(function(int seed) returns int {
        int soil = find("seed-to-soil", seed);
        int fertilizer = find("soil-to-fertilizer", soil);
        int water = find("fertilizer-to-water", fertilizer);
        int light = find("water-to-light", water);
        int temperature = find("light-to-temperature", light);
        int humidity = find("temperature-to-humidity", temperature);
        int location = find("humidity-to-location", humidity);
        return location;
    });
    return locations.sort()[0];
}

type Map2 record {|
    readonly string type_;
    readonly int begin;
    int length;
    // end (inclusive) = begin + length - 1
    int adjustment;
|};
function part2(Almanac almanac) returns int {
    table<record {|int begin; int length;|}> seedRanges = table [
        {begin: almanac.seeds[0],  length: almanac.seeds[1]},
        {begin: almanac.seeds[2],  length: almanac.seeds[3]},
        {begin: almanac.seeds[4],  length: almanac.seeds[5]},
        {begin: almanac.seeds[6],  length: almanac.seeds[7]},
        {begin: almanac.seeds[8],  length: almanac.seeds[9]},
        {begin: almanac.seeds[10], length: almanac.seeds[11]},
        {begin: almanac.seeds[12], length: almanac.seeds[13]},
        {begin: almanac.seeds[14], length: almanac.seeds[15]},
        {begin: almanac.seeds[16], length: almanac.seeds[17]},
        {begin: almanac.seeds[18], length: almanac.seeds[19]}
    ];
    seedRanges = from var seedRange in seedRanges order by seedRange.begin select seedRange;
    // io:println("seedRanges ---");
    // seedRanges.forEach(x => io:println(x));

    // io:println("maps ---");
    // almanac.maps.forEach(m => io:println(m));

    table<Map2> key(type_, begin) maps = table key(type_, begin)
    from var map_ in almanac.maps
    order by map_.step, map_.sourceRangeStart
    select {
        type_: map_.mapType,
        begin: map_.sourceRangeStart,
        length: map_.rangeLength,
        adjustment: map_.destinationRangeStart - map_.sourceRangeStart
    };
    io:println("maps ---");
    foreach var m in maps {
        io:println(m);
    }

    var isInRange = function(Map2 map_, record {|int begin; int length;|} range) returns boolean {
        readonly & int mapBegin = map_.begin;
        readonly & int mapEnd   = map_.begin + map_.length - 1;
        readonly & int rangeBegin = range.begin;
        readonly & int rangeEnd   = range.begin + range.length - 1;
        return (mapBegin <= rangeBegin) && (rangeEnd <= mapEnd);
    };

    foreach var seedRange in seedRanges {
        io:println("---");
        io:println("seedRange: ", seedRange);

        table<Map2> seedToSoilMaps = from var m in maps where m.type_ == "seed-to-soil" select m;
        io:println("seedToSoilMaps: ", seedToSoilMaps);

        // 1) find the matching range
        foreach var map_ in seedToSoilMaps {
            boolean inRange = isInRange(map_, seedRange);
            io:println(map_, ": ", inRange);
            if inRange {
            }
        }

        // 2) range splitting?

        // 3) default range if no specific range found
    }

    return 0;
}

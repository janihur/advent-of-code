import ballerina/io;
import ballerina/test;

type Bag object {
    private map<boolean> bag;

    function __init() {
        self.bag = {};
    }
    function add(int a) {
        self.bag[a.toString()] = true;
    }
    function has(int a) returns boolean {
        return self.bag.hasKey(a.toString());
    }
};

type Frequency record {
    int change;
};

function part1(table<Frequency> frequencies) returns int {
    int accu = 0;
    foreach var f in frequencies {
        accu += f.change;
    }
    return accu;
}

function part2(table<Frequency> frequencies) returns int {
    Bag alreadySeen = new();
    boolean quit = false;
    int accu = 0;
    while (!quit) {
        foreach var f in frequencies {
            accu += f.change;
            if (alreadySeen.has(accu)) {
                quit = true;
                break;
            } else {
                alreadySeen.add(accu);
            }
        }
    }
    return accu;
}

public function main() {
    // test data --------------------------------------------------------------
    table<Frequency> testData = table {
        {change},
        [{1},{-2},{3},{1}]
    };

    // puzzle data ------------------------------------------------------------
    var csv = checkpanic io:openReadableCsvFile("../inputs/2018-01");
    var puzzleData = <table<Frequency>> checkpanic csv.getTable(Frequency);

    // part 1 -----------------------------------------------------------------
    int a1 = part1(testData);
    io:println("part 1   test data answer: ", a1);
    test:assertEquals(a1, 3);

    int a2 = part1(puzzleData);
    io:println("part 1 puzzle data answer: ", a2);
    test:assertEquals(a2, 466);

    // part 2 -----------------------------------------------------------------
    int a3 = part2(testData);
    io:println("part 2   test data answer: ", a3);
    test:assertEquals(a3, 2);

    int a4 = part2(puzzleData);
    io:println("part 2 puzzle data answer: ", a4);
    test:assertEquals(a4, 750);
}
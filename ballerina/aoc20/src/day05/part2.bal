function part2(string[] boardingPasses) returns int? {
    int[] x = [];
    foreach var pass in boardingPasses {
        var pos = calculatePosition(pass);
        x.push(pos.row * 8 + pos.column);
    }

    x = x.sort();

    int y = x[0];
    foreach var i in 1 ... x.length() - 1 {
        if y+1 != x[i] {
            return y+1;
        }
        y = x[i];
    }
    return ();
}

import ballerina/lang.array;
// import ballerina/io;
import ballerina/test;

function part1(string[] boardingPasses) returns int {
    return part1Imp2(boardingPasses);
}

// IMPLEMENTATION 2 -----------------------------------------------------------

function part1Imp2(string[] boardingPasses) returns int {
    return boardingPasses.map(
        function(string boardingPass) returns int {
           var [row, column] = calculatePosition2(boardingPass);
           return row * 8 + column;
        }
    ).reduce(max, 0);
}

function max(int accu, int curr) returns int {
    return curr > accu ? curr : accu;
}

function calculatePosition2(string boardingPass) returns [int, int] {
    var row = binarySpacePartitioner(
        // function (string instruction, int partitionSize, [int, int] range) returns [int, int] {
        //     var [min, max] = range;
        //     match instruction {
        //         "F" => {
        //             // lower half
        //             max -= partitionSize;
        //         }
        //         "B" => {
        //             // upper half
        //             min += partitionSize;
        //         }
        //     }
        //     return [min, max];
        // },
        partitioner              = partitionerBuilder(lowInstruction = "F", highInstruction = "B"),
        initialPartitionSize     = 128,
        partitioningInstructions = boardingPass.substring(0, 7)
    );

    var column = binarySpacePartitioner(
        // function (string instruction, int partitionSize, [int, int] range) returns [int, int] {
        //     var [min, max] = range;
        //     match instruction {
        //         "L" => {
        //             // lower half
        //             max -= partitionSize;
        //         }
        //         "R" => {
        //             // upper half
        //             min += partitionSize;
        //         }
        //     }
        //     return [min, max];
        // },
        partitioner              = partitionerBuilder(lowInstruction = "L", highInstruction = "R"),
        initialPartitionSize     = 8,
        partitioningInstructions = boardingPass.substring(7)
    );

    return [row, column];
}

function binarySpacePartitioner(
    function (string, int, [int, int]) returns [int, int] partitioner,
    int initialPartitionSize, 
    string partitioningInstructions
) returns int {
    int min = 0;
    int max = initialPartitionSize - 1;
    int currentPartitionSize = initialPartitionSize;

    foreach var instruction in partitioningInstructions {
        currentPartitionSize /= 2;
        [min, max] = partitioner(instruction, currentPartitionSize, [min, max]);
    }
    
    test:assertEquals(min, max);
    return min;
}

function partitionerBuilder(
    string lowInstruction, 
    string highInstruction
) returns function (string, int, [int, int]) returns [int, int] {
    return function (string instruction, int partitionSize, [int, int] range) returns [int, int] {
        var [min, max] = range;
        match instruction {
            lowInstruction => {
                // lower half
                max -= partitionSize;
            }
            highInstruction => {
                // upper half
                min += partitionSize;
            }
            _ => {
                test:assertFail(string`Invalid instruction: ${instruction}. Supported instructions are ${lowInstruction} (low) and ${highInstruction} (high).`);
            }
        }
        return [min, max];
    };
}

// IMPLEMENTATION 1 -----------------------------------------------------------

function part1Imp1(string[] boardingPasses) returns int {
    int[] seatIds = [];

    foreach var pass in boardingPasses {
        var pos = calculatePosition(pass);
        seatIds.push(pos.row * 8 + pos.column);
    }

    return seatIds.sort(array:DESCENDING)[0];
}

type SeatPosition record {|
    int row;
    int column;
|};

function calculatePosition(string boardingPass) returns SeatPosition {
    SeatPosition pos = {
        row: 0,
        column: 0
    };

    {
        int minRow = 0;
        int maxRow = 127;
        int rowRange = 128;
        foreach var letter in boardingPass.substring(0, 7) {
            rowRange /= 2;

            match letter {
                "F" => {
                    // lower half
                    maxRow -= rowRange;
                }
                "B" => {
                    // upper half
                    minRow += rowRange;
                }
            }
            // io:println(string`letter: ${letter} min: ${minRow} max: ${maxRow} range: ${rowRange}`);
        }

        test:assertEquals(minRow, maxRow);
        pos.row = minRow;
    }

    {
        int minColumn = 0;
        int maxColumn = 7;
        int columnRange = 8;
        foreach var letter in boardingPass.substring(7) {
            columnRange /= 2;

            match letter {
                "L" => {
                    // lower half
                    maxColumn -= columnRange;
                }
                "R" => {
                    // upper half
                    minColumn += columnRange;
                }
            }
            // io:println(string`letter: ${letter} min: ${minColumn} max: ${maxColumn} range: ${columnRange}`);
        }

        test:assertEquals(minColumn, maxColumn);
        pos.column = minColumn;

    }

    return pos;
}

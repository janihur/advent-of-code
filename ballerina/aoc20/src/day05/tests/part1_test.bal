import ballerina/test;

@test:Config {}
function test1CalculatePosition() {
    var a = calculatePosition("FBFBBFFRLR");
    test:assertEquals(a, <SeatPosition>{row: 44, column: 5});
}

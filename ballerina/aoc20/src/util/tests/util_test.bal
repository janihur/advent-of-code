import ballerina/test;

@test:Config {}
function test_cut_1() {
    test:assertEquals(cut(""), ["", ""]);
    test:assertEquals(cut(":"), ["", ""]);
    test:assertEquals(cut("::"), ["", ":"]);
    test:assertEquals(cut("key"), ["key", ""]);
    test:assertEquals(cut("key:"), ["key", ""]);
    test:assertEquals(cut("key:value"), ["key","value"]);
}

@test:Config {}
function test_cut_2() {
    test:assertEquals(cut("", ":separator:"), ["", ""]);
    test:assertEquals(cut(":separator:", ":separator:"), ["", ""]);
    test:assertEquals(cut(":separator::separator:", ":separator:"), ["", ":separator:"]);
    test:assertEquals(cut("key", ":separator:"), ["key", ""]);
    test:assertEquals(cut("key:separator:", ":separator:"), ["key", ""]);
    test:assertEquals(cut("key:separator:value", ":separator:"), ["key", "value"]);
}

@test:Config {}
function test_concat_1() {
    test:assertEquals(concat(["a"], ["b"]), ["a","b"]);
}

@test:Config {}
function test_int2str_1() {
    test:assertEquals(int2str(-123), "-123");
    test:assertEquals(int2str(123), "123");
}

@test:Config {}
function test_max_1() {
    test:assertEquals(max(1,2), 2);
    test:assertEquals(max(2,2), 2);
    test:assertEquals(max(3,2), 3);
}

@test:Config {}
function test_split_1() {
    test:assertEquals(split(""), [""]);
    test:assertEquals(split(","), [""]);
    test:assertEquals(split(",,"), ["",""]);
    test:assertEquals(split(",a"), ["","a"]);
    test:assertEquals(split("a"), ["a"]);
    test:assertEquals(split("a,"), ["a"]);
    test:assertEquals(split("a,b"), ["a","b"]);
    test:assertEquals(split("a,b,c"), ["a","b","c"]);
    test:assertEquals(split("a,,c"), ["a","","c"]);
}

@test:Config {}
function test_str2int_1() {
    // TODO: happy paths only
    test:assertEquals(str2int("-123"), -123);
    test:assertEquals(str2int("123"), 123);
}

@test:Config {}
function test_sum_1() {
    test:assertEquals(sum(1,2), 3);
    test:assertEquals(sum(100,200), 300);
}

@test:Config {}
function test_SetInt_1() {
    SetInt s = new();

    s.add(1);
    s.add(2);
    s.add(1);

    test:assertEquals(s.has(1), true);
    test:assertEquals(s.has(2), true);
    test:assertEquals(s.has(3), false);

    test:assertEquals(s.values(), [1,2]);
}

@test:Config {}
function test_SetInt2_1() {
    SetInt2 s = new();

    s.add(1);
    s.add(2);
    s.add(1);

    test:assertEquals(s.has(1), true);
    test:assertEquals(s.has(2), true);
    test:assertEquals(s.has(3), false);

    test:assertEquals(s.values(), [1,2]);

    s.remove(1);
    s.remove(3);

    test:assertEquals(s.has(1), true);
    test:assertEquals(s.has(2), true);
    test:assertEquals(s.has(3), false);

    test:assertEquals(s.values(), [1,2]);

    s.remove(1);

    test:assertEquals(s.has(1), false);
    test:assertEquals(s.has(2), true);
    test:assertEquals(s.has(3), false);

    test:assertEquals(s.values(), [2]);
}

@test:Config {}
function test_SetString_1() {
    SetString s = new();

    s.add("a");
    s.add("b");
    s.add("a");

    test:assertEquals(s.has("a"), true);
    test:assertEquals(s.has("b"), true);
    test:assertEquals(s.has("c"), false);

    test:assertEquals(s.values(), ["a","b"]);
}

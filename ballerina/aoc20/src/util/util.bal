import ballerina/io;
import ballerina/lang.'int;

public function cut(string value, string separator = ":") returns ([string, string]) {
    int? cutIndex = value.indexOf(separator, 0);
    if cutIndex is int {
        int separatorLength = separator.length();
        string head = value.substring(0, cutIndex);
        string tail = value.substring(cutIndex + separatorLength);
        return [head, tail];
    } else {
        return [value, ""];
    }
}

public function concat(string[] a, string[] b) returns string[] {
    var c = a.clone();
    foreach var item in b {
        c.push(item);
    }
    return c;
}

public function int2str(int value) returns string {
    return value.toString();
}

public function max(int a, int b) returns int {
    return a > b ? a : b;
}

public function readIntList(string filename) returns int[] {
    io:ReadableByteChannel byteChannel = checkpanic io:openReadableFile(filename);
    io:ReadableCharacterChannel characterChannel = new (byteChannel, "UTF-8");
    io:ReadableTextRecordChannel delimitedRecordChannel =
        new (characterChannel, rs = "\\n", fs = "\\n");

    int[] data = [];
    while (delimitedRecordChannel.hasNext()) {
        string[] records = checkpanic delimitedRecordChannel.getNext();
        data.push(str2int(records[0]));
    }

    return data;
}

public function split(string value, string separator = ",") returns string[] {
    string[] accu = [];
    var [head, tail] = cut(value, separator);
    accu.push(head);
    if tail.length() > 0 {
        accu = concat(accu, split(tail, separator));
    }
    return accu;
}

public function str2int(string value) returns int {
    return checkpanic 'int:fromString(value);
}

public function sum(int a, int b) returns int {
    return a + b;
}

public class SetInt {
    map<boolean> set;

    public function init() {
        self.set = {};
    }

    public function add(int value) {
        self.set[self.toKey(value)] = true;
    }

    public function has(int value) returns boolean {
        boolean? hasValue = self.set[self.toKey(value)];
        return hasValue is () ? false : hasValue;
    }

    public function values() returns int[] {
        int[] accu = [];
        foreach var key in self.set.keys().sort() {
            accu.push(self.fromKey(key));
        }
        return accu;
    }

    private function toKey(int value) returns string {
        return value.toString();
    }

    private function fromKey(string value) returns int {
        return checkpanic 'int:fromString(value);
    }
}

public class SetInt2 {
    map<int> set;

    public function init() {
        self.set = {};
    }

    public function add(int value) {
        if self.has(value) {
            int currentValue = <int>self.set[self.toKey(value)];
            self.set[self.toKey(value)] = currentValue + 1;
        } else {
            self.set[self.toKey(value)] = 1;
        }
    }

    public function has(int value) returns boolean {
        int? currentValue = self.set[self.toKey(value)];
        return currentValue is () ? false : currentValue > 0 ? true : false;
    }

    public function remove(int value) {
        if self.has(value) {
            int currentValue = <int>self.set[self.toKey(value)];
            self.set[self.toKey(value)] = currentValue - 1;
        }
    }
    
    public function values() returns int[] {
        // "traditional" implementation

        // int[] accu = [];
        // foreach var key in self.set.keys().sort() {
        //     int currentValue = <int>self.set[key];
        //     if currentValue > 0 {
        //         accu.push(self.fromKey(key));
        //     }
        // }

        // query expression implementation
        int[] accu = 
            from var key in self.set.keys()
            let int value = <int>self.set[key]
            where value > 0
            order by key
            select self.fromKey(key)
            ;

        return accu;
    }

    private function toKey(int value) returns string {
        return value.toString();
    }

    private function fromKey(string value) returns int {
        return checkpanic 'int:fromString(value);
    }
}

public class SetString {
    map<boolean> set;

    public function init() {
        self.set = {};
    }

    public function add(string value) {
        self.set[value] = true;
    }

    public function has(string value) returns boolean {
        boolean? hasValue = self.set[value];
        return hasValue is () ? false : hasValue;
    }

    public function values() returns string[] {
        string[] accu = [];
        foreach var key in self.set.keys().sort() {
            accu.push(key);
        }
        return accu;
    }
}
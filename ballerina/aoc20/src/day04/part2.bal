import ballerina/lang.'int;

function isHgtValid(string value_) returns boolean {
    int & readonly len = value_.length();
    string & readonly typeStr = value_.substring(len-2);
    string & readonly heightStr = value_.substring(0, len-2);

    match typeStr {
        "in" => {
            int|error x = 'int:fromString(heightStr);
            if x is error {
                return false;
            } else {
                if x < 59 { return false; }
                if x > 76 { return false; }
            }
        }
        "cm" => {
            int|error x = 'int:fromString(heightStr);
            if x is error {
                return false;
            } else {
                if x < 150 { return false; }
                if x > 193 { return false; }
            }
        }
        _ => {
            return false;
        }
    }

    return true;
}

function isHclValid(string value_) returns boolean {
    if value_.length() != 7 { return false; }
    if value_[0] != "#" { return false; }
    foreach var i in 1 ... 6 {
        match value_[i] {
            "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" | "a" | "b" | "c" | "d" | "e" | "f" => {
                // ok
            }
            _ => {
                return false;
            }
        } 
    }

    return true;
}

function isEclValid(string value_) returns boolean {
    match value_ {
        "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth" => {
            return true;
        }
        _ => {
            return false;
        }
    }
}

function isPidValid(string value_) returns boolean {
    if value_.length() != 9 { return false; }
    foreach var i in 0 ... 8 {
        match value_[i] {
            "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" => {
                // ok
            }
            _ => {
                return false;
            }
        } 
    }

    return true;
}

function isValid2(string[] passport) returns boolean {
    foreach var item in passport {
        var [key_, value_] = split(item);
        match key_ {
            "byr" => {
                int|error x = 'int:fromString(value_);
                if x is error {
                    return false;
                } else {
                    if x < 1920 { return false; }
                    if x > 2002 { return false; }
                }
            } 
            "iyr" => {
                int|error x = 'int:fromString(value_);
                if x is error {
                    return false;
                } else {
                    if x < 2010 { return false; }
                    if x > 2020 { return false; }
                }
            } 
            "eyr" => {
                int|error x = 'int:fromString(value_);
                if x is error {
                    return false;
                } else {
                    if x < 2020 { return false; }
                    if x > 2030 { return false; }
                }
            } 
            "hgt" => {
                if !isHgtValid(value_) {
                    return false;
                }
            } 
            "hcl" => {
                if !isHclValid(value_) {
                    return false;
                }
            } 
            "ecl" => {
                if !isEclValid(value_) {
                    return false;
                }
            } 
            "pid" => {
                if !isPidValid(value_) {
                    return false;
                }
            }
            "cid" => {
                // ok
            }
        }
    }

    return true;
}

function part2(string[][] puzzleData) returns int {
    int validPassports = 0;
    
    foreach var passport in puzzleData {
        validPassports += isValid(passport) && isValid2(passport) ? 1 : 0;
    }
    
    return validPassports;
}
function split(string value, string separator = ":") returns ([string, string]) {
    int splitPoint = <int>value.indexOf(separator, 0);
    return [
        value.substring(0, splitPoint),
        value.substring(splitPoint + 1)
    ];
}

function isValid(string[] passport) returns boolean {
    int passportItems = 0;
    boolean cidMissing = true;

    foreach var item in passport {
        var [key_, _] = split(item);
        match key_ {
            "byr" | "iyr" | "eyr" | "hgt" | "hcl" | "ecl" | "pid" => {
                passportItems += 1;
            }
            "cid" => {
                passportItems += 1;
                cidMissing = false;
            }
        }
    }

    if passportItems == 8 { return true; }
    if passportItems == 7 && cidMissing { return true; }
    return false;
}

function part1(string[][] puzzleData) returns int {
    int validPassports = 0;

    foreach var passport in puzzleData {
        validPassports += isValid(passport) ? 1 : 0;
    }

    return validPassports;
}

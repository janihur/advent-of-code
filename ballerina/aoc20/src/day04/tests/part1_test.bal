import ballerina/test;
// import ballerina/io;
string[][] testData = [
    ["ecl:gry", "pid:860033327", "eyr:2020", "hcl:#fffffd"],
    ["byr:1937", "iyr:2017", "cid:147", "hgt:183cm"],
    [""],
    ["iyr:2013", "ecl:amb", "cid:350", "eyr:2023", "pid:028048884"],
    ["hcl:#cfa07d", "byr:1929"],
    [""],
    ["hcl:#ae17e1", "iyr:2013"],
    ["eyr:2024"],
    ["ecl:brn", "pid:760753108", "byr:1931"],
    ["hgt:179cm"],
    [""],
    ["hcl:#cfa07d", "eyr:2025", "pid:166559648"],
    ["iyr:2011", "ecl:brn", "hgt:59in"]
];

// @test:Config {}
function testNormalizeData() {
    string [][] normalizedData = normalize(testData);
    // io:println("testNormalizeData: ", normalizedData);
}

@test:Config {}
function testSplit() {
    var [a, b] = split("hcl:#ae17e1");
    test:assertEquals(a, "hcl");
    test:assertEquals(b, "#ae17e1");
}

@test:Config {}
function testIsValid() {
    boolean? a = ();
    a = isValid(["ecl:gry","pid:860033327","eyr:2020","hcl:#fffffd","byr:1937","iyr:2017","cid:147","hgt:183cm"]);
    test:assertEquals(a, true);
}

@test:Config {}
function test1() {
    string [][] normalizedData = normalize(testData);
    // io:println("test1: ", normalizedData);
    int a = part1(normalizedData);
    test:assertEquals(a, 2);
}

import ballerina/io;
import ballerina/test;

function normalize(string[][] data) returns string[][] {
    string[][] normalized = [];
    boolean firstRecord = true;
    int lastIndex = -1;

    foreach var line in data {
        if firstRecord {
            normalized.push(line);
            firstRecord = false;
            lastIndex += 1;
            continue;
        }
        
        if line[0].length() == 0 {
            firstRecord = true;
            continue;
        }

        foreach var item in line {
            normalized[lastIndex].push(item);
        }
        
    }

    return normalized;
}

public function main() {
    // read puzzle data -------------------------------------------------------
    io:ReadableByteChannel byteChannel = checkpanic io:openReadableFile("../../inputs/2020-04");
    io:ReadableCharacterChannel characterChannel = new (byteChannel, "UTF-8");
    io:ReadableTextRecordChannel delimitedRecordChannel =
        new (characterChannel, rs = "\\n", fs = " ");

    string[][] puzzleDataStr = [];
    while (delimitedRecordChannel.hasNext()) {
        string[] records = checkpanic delimitedRecordChannel.getNext();
        puzzleDataStr.push(records);
    }

    var puzzleData = normalize(puzzleDataStr);

    // part 1 -----------------------------------------------------------------
    int a1 = part1(puzzleData);
    io:println("part 1 answer: ", a1);
    test:assertEquals(a1, 237);

    // part 2 -----------------------------------------------------------------
    int a2 = part2(puzzleData);
    io:println("part 2 answer: ", a2);
    test:assertEquals(a2, 172);
}

function part1(string[] puzzleData) returns int {
    int total = 0;

    map<int> groupAnswers = {};

    foreach var line in puzzleData {
        if line.length() == 0 {
            total += count(groupAnswers);
            groupAnswers = {};
            continue;
        }

        foreach var letter in line {
            if groupAnswers.hasKey(letter) {
                var oldValue = groupAnswers[letter];
                groupAnswers[letter] = <int>oldValue + 1;
            } else {
                groupAnswers[letter] = 1;
            }
        }
    }
    
    // the final group
    total += count(groupAnswers);
    return total;
}

function count(map<int> groupAnswers) returns int {
    return groupAnswers.keys().length();
}
function part2(string[] puzzleData) returns int {
    int total = 0;

    int groupMemberCount = 0;
    map<int> groupAnswers = {};

    foreach var line in puzzleData {
        if line.length() == 0 {
            total += count2(groupMemberCount, groupAnswers);
            groupMemberCount = 0;
            groupAnswers = {};
            continue;
        }

        groupMemberCount += 1;

        foreach var letter in line {
            if groupAnswers.hasKey(letter) {
                var oldValue = groupAnswers[letter];
                groupAnswers[letter] = <int>oldValue + 1;
            } else {
                groupAnswers[letter] = 1;
            }
        }
    }
    
    // the final group
    total += count2(groupMemberCount, groupAnswers);
    return total;
}

function count2(int groupMemberCount, map<int> groupAnswers) returns int {
    return groupAnswers.keys().reduce(
        function (int accu, string curr) returns int {
            return groupAnswers[curr] == groupMemberCount ? accu + 1 : accu;
        }, 0
    );
}
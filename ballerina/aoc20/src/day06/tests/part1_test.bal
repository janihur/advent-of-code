import ballerina/test;

string[] testData = [
    "abc",
    "",
    "a",
    "b",
    "c",
    "",
    "ab",
    "ac",
    "",
    "a",
    "a",
    "a",
    "a",
    "",
    "b"
];

@test:Config {}
function test1() {
    int a = part1(testData);
    test:assertEquals(a, 11);
}

@test:Config {}
function test1Count() {
    test:assertEquals(count({a:1,b:2,c:3}), 3);
}
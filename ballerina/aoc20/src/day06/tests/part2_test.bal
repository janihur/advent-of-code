import ballerina/test;

@test:Config {}
function test2() {
    int a = part2(testData);
    test:assertEquals(a, 6);
}

@test:Config {}
function test2Count2() {
    test:assertEquals(count2(3, {a:2,b:2,c:1}), 0);
    test:assertEquals(count2(3, {a:3,b:2,c:1}), 1);
}
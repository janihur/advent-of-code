// import ballerina/io;

function part2(int[] numbers, int invalidNumber) returns int? {
    // io:println(numbers);
    int & readonly len = numbers.length();
    foreach var 'tuple1 in numbers.enumerate() {
        var [index1, number1] = 'tuple1;
        int sum = number1;
        foreach var 'tuple2 in numbers.slice(index1 + 1).enumerate() {
           var [index2, number2] = 'tuple2;
            sum += number2;
            if sum == invalidNumber {
                // io:println(index1, " ", index2);
                var [min, max] = minmax(numbers.slice(index1, index1 + index2 + 1 + 1));
                return min + max;
            }
            if sum > invalidNumber {
                continue;
            }
        }
    }
    return ();
}

function minmax(int[] arr) returns [int, int] {
    // io:println(arr);
    var x = arr.sort();
    return [x[0], x[x.length()-1]];
}
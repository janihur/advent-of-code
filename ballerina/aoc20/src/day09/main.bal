import ballerina/io;
import ballerina/lang.'int;
import ballerina/test;

public function main() {
    // read puzzle data -------------------------------------------------------
    io:ReadableByteChannel byteChannel = checkpanic io:openReadableFile("../../inputs/2020-09");
    io:ReadableCharacterChannel characterChannel = new (byteChannel, "UTF-8");
    io:ReadableTextRecordChannel delimitedRecordChannel =
        new (characterChannel, rs = "\\n", fs = "\\n");

    int[] puzzleData = [];
    while (delimitedRecordChannel.hasNext()) {
        string[] records = checkpanic delimitedRecordChannel.getNext();
        puzzleData.push(checkpanic 'int:fromString(records[0]));
    }

    // io:println(puzzleData);

    // part 1 -----------------------------------------------------------------
    int? a1 = part1(puzzleData, preambleSize = 25);
    io:println("part 1 answer: ", a1);
    test:assertEquals(a1, 27911108);

    // part 2 -----------------------------------------------------------------
    int? a2 = part2(puzzleData, invalidNumber = 27911108);
    io:println("part 2 answer: ", a2);
    test:assertEquals(a2, 4023754);
}

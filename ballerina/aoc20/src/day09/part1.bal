// import ballerina/io;
import jani/util;

function part1(int[] numbers, int preambleSize) returns int? {
    NumberVault nv = new(numbers.slice(0, preambleSize));
    // io:println("---");
    // io:println("preamble:    ", nv.preamble);
    // io:println("sums:        ", nv.allSums.values());
    // io:println("sums/number: ", nv.numberSums);
    foreach var number in numbers.slice(preambleSize) {
        boolean isValid = nv.add(number);
        // io:println("---");
        // io:println("number ", number, " is ", isValid);
        // io:println("preamble:    ", nv.preamble);
        // io:println("sums:        ", nv.allSums.values());
        // io:println("sums/number: ", nv.numberSums);
        if !isValid { return number; }
    }
    return ();
}

class NumberVault {
    int[] preamble = [];
    util:SetInt2 allSums = new();
    map<int[]> numberSums = {};
    
    function init(int[] preamble) {
        self.preamble = preamble.clone();
        foreach var 'tuple in preamble.enumerate() {
            var [index, number1] = 'tuple;
            foreach var number2 in preamble.slice(index + 1) {
                int sum = number1 + number2;
                self.allSums.add(sum);
                self.addNumberSum(number1, sum);
            }
        }
    }

    function add(int number) returns boolean {
        boolean & readonly isValid = self.allSums.has(number);

        // update preamble
        var oldNumber = self.preamble.shift();
        self.preamble.push(number);

        // remove old sums
        var oldSums = <int[]>self.numberSums[util:int2str(oldNumber)];
        foreach var sum in oldSums {
            self.allSums.remove(sum);
        }
        var x = self.numberSums.removeIfHasKey(util:int2str(oldNumber));

        // add new sums
        foreach var number1 in self.preamble.slice(0, self.preamble.length() - 1) {
            int newSum = number1 + number;
            self.allSums.add(newSum);
            self.addNumberSum(number1, newSum);
        }

        return isValid;
    }

    private function addNumberSum(int number, int sum) {
        if self.numberSums.hasKey(util:int2str(number)) {
            (<int[]>self.numberSums[util:int2str(number)]).push(sum);
        } else {
            self.numberSums[util:int2str(number)] = <int[]>[sum];
        }
    }
}

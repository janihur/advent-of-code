import ballerina/test;

int[] testData = [
    35,
    20,
    15,
    25,
    47, // preamble ends
    40,
    62,
    55,
    65,
    95,
    102,
    117,
    150,
    182,
    127,
    219,
    299,
    277,
    309,
    576
];

@test:Config {}
function test1_part1() {
    int? a = part1(testData, preambleSize = 5);
    test:assertEquals(a, 127);
}

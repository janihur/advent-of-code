import ballerina/test;

@test:Config {}
function test2_part2() {
    int? a = part2(testData, invalidNumber = 127);
    test:assertEquals(a, 62);
}
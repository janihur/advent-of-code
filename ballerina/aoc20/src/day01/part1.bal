function part1(int[] expenses) returns int? {
    return part1Imp4(expenses);
}

function myIndexOf(int[] arr, int value, int startIndex) returns int? {
    foreach var i in startIndex ... arr.length() - 1 {
        if arr[i] < value {
            return ();
        }
        if arr[i] == value {
            return i;
        }
    }
    return ();
}

function part1Imp4(int[] expenses) returns int? {
    int[] sorted = expenses.sort().reverse();

    foreach var pos in 0 ... sorted.length() - 1 {
        int actual = sorted[pos];
        int missing = 2020 - actual;
        int? x = myIndexOf(sorted, missing, pos + 1);
        if x is int {
            return actual * missing;
        }
    }

    return ();
}

function part1Imp3(int[] expenses) returns int? {
    int[] sorted = expenses.sort().reverse();

    foreach var pos in 0 ... sorted.length() - 1 {
        int actual = sorted[pos];
        int missing = 2020 - actual;
        int? x = sorted.indexOf(missing, pos + 1);
        if x is int {
            return actual * missing;
        }
    }

    return ();
}

function part1Imp2(int[] expenses) returns int? {
    int[] sorted = expenses.sort().reverse();

    foreach var expense in expenses {
        int[] x = sorted.filter(function(int i) returns boolean {
            return expense + i == 2020;
        });
        if x.length() > 0 {
            return expense * x[0];
        }
    }

    return ();
}

function part1Imp1(int[] expenses) returns int? {
    foreach var expense1 in expenses {
        foreach var expense2 in expenses {
            if expense1 + expense2 == 2020 {
                return expense1 * expense2;
            }
        }
    }

    return ();
}

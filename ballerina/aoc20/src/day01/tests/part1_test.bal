import ballerina/test;

int[] testData = [
    1721,
    979,
    366,
    299,
    675,
    1456
];

@test:Config {}
function testPart1Imp1() {
    int? a = part1Imp1(testData);
    test:assertEquals(a, 514579);
}

@test:Config {}
function testPart1Imp2() {
    int? a = part1Imp2(testData);
    test:assertEquals(a, 514579);
}

@test:Config {}
function testPart1Imp3() {
    int? a = part1Imp3(testData);
    test:assertEquals(a, 514579);
}

@test:Config {}
function testPart1Imp4() {
    int? a = part1Imp4(testData);
    test:assertEquals(a, 514579);
}

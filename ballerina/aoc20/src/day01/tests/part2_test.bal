import ballerina/test;

@test:Config {}
function testPart2Imp1() {
    int? a = part2Imp1(testData);
    test:assertEquals(a, 241861950);
}

@test:Config {}
function testPart2Imp2() {
    int? a = part2Imp2(testData);
    test:assertEquals(a, 241861950);
}
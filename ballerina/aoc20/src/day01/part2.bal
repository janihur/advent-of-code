function part2(int[] expenses) returns int? {
    return part2Imp1(expenses);
}

function helper1(int[] arr, int searchValue) returns int? {
    foreach var pos in 0 ... arr.length() - 1 {
        int actual = arr[pos];
        int missing = searchValue - actual;

        int? x = myIndexOf(arr, missing, pos + 1);
        if x is int {
            return actual * missing;
        }
    }

    return ();
}

function part2Imp2(int[] expenses) returns int? {
    int[] sorted = expenses.sort().reverse();

    foreach var pos in 0 ... sorted.length() - 1 {
        int actual = sorted[pos];
        int missing = 2020 - actual;

        int? x = helper1(sorted.slice(pos + 1), missing);
        if x is int {
            return actual * x;
        }
    }

    return ();
}

function part2Imp1(int[] expenses) returns int? {
    foreach var expense1 in expenses {
        foreach var expense2 in expenses {
            foreach var expense3 in expenses {
                if expense1 + expense2 + expense3 == 2020 {
                    return expense1 * expense2 * expense3;
                }
            }
        }
    }

    return ();
}

// import ballerina/io;
import jani/util;

function part2(map<BagRule[]> rules) returns int {
    int total = traverse(rules, "shiny gold", 1);
    return total - 1; // exclude "shiny gold" bag from total
}

function traverse(map<BagRule[]> rules, string color, int parentBagNumber) returns int {
    var subBags = rules.get(color);
    // io:println("---");
    // io:println(string`current bag: ${color} ${parentBagNumber}: `, subBags);

    if subBags.length() == 0 {
        // io:println(string`returning ${parentBagNumber}`);
        return parentBagNumber;
    }

    int[] subCounts = [];
    foreach var subBag in subBags {
        // io:println("subBag:", subBag);
        subCounts.push(traverse(rules, subBag.color, subBag.number));
    }
    int subCount = subCounts.reduce(util:sum, 0);

    // io:println("subCounts: ", subCounts, " = ", subCount);
    return (subCount * parentBagNumber) + parentBagNumber;
}
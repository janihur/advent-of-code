import ballerina/test;
// import ballerina/io;

string[] testData = [
    "light red bags contain 1 bright white bag, 2 muted yellow bags.",
    "dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
    "bright white bags contain 1 shiny gold bag.",
    "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
    "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
    "dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
    "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
    "faded blue bags contain no other bags.",
    "dotted black bags contain no other bags."
];

@test:Config {}
function test_getBagRules_1() {
    var rules = getBagRules(testData);
    // io:println(rules);
    test:assertEquals(
        rules["light red"],
        [{"color":"bright white","number":1},{"color":"muted yellow","number":2}]
    );
    test:assertEquals(
        rules["bright white"],
        [{"color":"shiny gold","number":1}]
    );
    test:assertEquals(
        rules["dotted black"],
        []
    );
}

@test:Config {}
function test_part1() {
    int a = part1(getBagRules(testData));
    test:assertEquals(a, 4);
}

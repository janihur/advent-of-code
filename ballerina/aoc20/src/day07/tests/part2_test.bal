import ballerina/test;

@test:Config {}
function test_part2() {
    int a = part2(getBagRules(testData));
    test:assertEquals(a, 32);
}

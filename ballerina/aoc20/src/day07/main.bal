import ballerina/io;
import ballerina/test;
import ballerina/lang.'int;
import jani/util;

type BagRule record {|
    string color;
    int number;
|};

function getBagRules(string[] puzzleData) returns map<BagRule[]> {
    map<BagRule[]> rules = {};
    foreach var line in puzzleData {
        var [color, contains] = util:cut(line, " bags contain ");
        rules[color] = [];

        var bags = util:split(contains);
        foreach var bag_ in bags {
            if bag_ == "no other bags." { continue; }
            var bag = bag_.trim();
            var parts = util:split(bag, " ");
            var number2 = checkpanic 'int:fromString(parts[0]);
            var color2 = string`${parts[1]} ${parts[2]}`;
            (<BagRule[]>rules[color]).push({color: color2, number: number2});
            
        }
    }
    return rules;
}

public function main() {
    // read puzzle data -------------------------------------------------------
    io:ReadableByteChannel byteChannel = checkpanic io:openReadableFile("../../inputs/2020-07");
    io:ReadableCharacterChannel characterChannel = new (byteChannel, "UTF-8");
    io:ReadableTextRecordChannel delimitedRecordChannel =
        new (characterChannel, rs = "\\n", fs = "\\n");

    string[] puzzleDataStr = [];
    while (delimitedRecordChannel.hasNext()) {
        string[] records = checkpanic delimitedRecordChannel.getNext();
        puzzleDataStr.push(records[0]);
    }

    // io:println(puzzleDataStr);

    map<BagRule[]> puzzleData = getBagRules(puzzleDataStr);

    // io:println(puzzleData);

    // part 1 -----------------------------------------------------------------
    int a1 = part1(puzzleData);
    io:println("part 1 answer: ", a1);
    test:assertEquals(a1, 226);

    // part 2 -----------------------------------------------------------------
    int a2 = part2(puzzleData);
    io:println("part 2 answer: ", a2);
    test:assertEquals(a2, 9569);
}

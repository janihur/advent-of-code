import jani/util;

function part1(map<BagRule[]> rules) returns int {
    map<boolean> result = {};
    string[] searchList = ["shiny gold"];
    util:SetString results = new();

    while (searchList.length() > 0) {
        map<BagRule[]> rules2 = findIncluding(rules, searchList.shift()); 
            foreach var color in rules2.keys() {
                searchList.push(color);
                results.add(color);
            }
    }

    return results.values().length();
}

function findIncluding(map<BagRule[]> rules, string color) returns map<BagRule[]> {
    return rules.filter(
        function (BagRule[] contains) returns boolean {
            return contains.reduce(
                function (boolean accu, BagRule item) returns boolean {
                    if accu { return accu; }
                    return item.color == color;
                },
                false
            );
        }
    );
}
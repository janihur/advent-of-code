import ballerina/test;

@test:Config {}
function test2_part2() {
    int? a = part2(testData);
    test:assertEquals(a, 8);
}
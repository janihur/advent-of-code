import ballerina/test;

Instruction[] testData = [
    {operation: "nop", argument: +0},
    {operation: "acc", argument: +1},
    {operation: "jmp", argument: +4},
    {operation: "acc", argument: +3},
    {operation: "jmp", argument: -3},
    {operation: "acc", argument: -99},
    {operation: "acc", argument: +1},
    {operation: "jmp", argument: -4},
    {operation: "acc", argument: +6}
];

@test:Config {}
function test1_part1() {
    int? a = part1(testData);
    test:assertEquals(a, 5);
}

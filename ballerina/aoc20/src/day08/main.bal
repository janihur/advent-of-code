import ballerina/io;
import ballerina/lang.'int;
import ballerina/test;

type Instruction record {|
    string operation;
    int argument;
|};

public function main() {
    // read puzzle data -------------------------------------------------------
    io:ReadableByteChannel byteChannel = checkpanic io:openReadableFile("../../inputs/2020-08");
    io:ReadableCharacterChannel characterChannel = new (byteChannel, "UTF-8");
    io:ReadableTextRecordChannel delimitedRecordChannel =
        new (characterChannel, rs = "\\n", fs = " ");

    string[][] puzzleDataStr = [];
    while (delimitedRecordChannel.hasNext()) {
        string[] records = checkpanic delimitedRecordChannel.getNext();
        puzzleDataStr.push(records);
    }

    // io:println(puzzleDataStr);

    var instructions = puzzleDataStr.map(function (string[] records) returns Instruction {
        return {
            operation: records[0],
            argument: checkpanic 'int:fromString(records[1])
        };
    });

    // io:println(instructions);

    // part 1 -----------------------------------------------------------------
    int? a1 = part1(instructions);
    io:println("part 1 answer: ", a1);
    test:assertEquals(a1, 1563);

    // part 2 -----------------------------------------------------------------
    int? a2 = part2(instructions);
    io:println("part 2 answer: ", a2);
    test:assertEquals(a2, 767);
}

import jani/util;

function part2(Instruction[] instructions) returns int? {
    int[] faultyInstructionIndexes = getFaultyInstructions(instructions);

    foreach var index in faultyInstructionIndexes {
        var modified = instructions.clone();
        swapInstruction(modified, index);
        var [finished, accu] = run(modified);
        if finished {
            return accu;
        }
    }

    return ();
}

function run(Instruction[] instructions) returns [boolean, int] {
    // boolean finished = false;
    int & readonly MAX_PC = instructions.length() - 1;
    int accu = 0;

    int pc = 0; // program counter
    util:SetInt seen = new ();
    
    while true {
        if pc > MAX_PC {
            return [true, accu];
        }
        if seen.has(pc) {
            return [false, accu];
        }
        seen.add(pc);
        match instructions[pc].operation {
            "nop" => {
                // no operation
                pc += 1;
            }
            "acc" => {
                accu += instructions[pc].argument;
                pc += 1;
            }
            "jmp" => {
                pc += instructions[pc].argument;
            }
        }
    }

    return [true, 0];
}

function swapInstruction(Instruction[] instructions, int index) {
    match instructions[index].operation {
        "nop" => { instructions[index].operation = "jmp"; }
        "jmp" => { instructions[index].operation = "nop"; }
    }
}

function getFaultyInstructions(Instruction[] instructions) returns int[] {
    int[] faultyInstructionIndexes = [];
    int index = 0;
    foreach var instruction in instructions {
        match instruction.operation {
            "nop" | "jmp" => {
                faultyInstructionIndexes.push(index);
            }
        }
        index += 1;
    }
    return faultyInstructionIndexes;
}
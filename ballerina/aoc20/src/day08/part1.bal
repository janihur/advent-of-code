import jani/util;

function part1(Instruction[] instructions) returns int? {
    int accu = 0;
    int pc = 0; // program counter
    util:SetInt seen = new ();
    
    while true {
        if seen.has(pc) {
            return accu;
        }
        seen.add(pc);
        match instructions[pc].operation {
            "nop" => {
                // no operation
                pc += 1;
            }
            "acc" => {
                accu += instructions[pc].argument;
                pc += 1;
            }
            "jmp" => {
                pc += instructions[pc].argument;
            }
        }
    }

    return ();
}

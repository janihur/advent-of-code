import ballerina/test;

@test:Config {}
function test2() {
    int a = part2(testData);
    test:assertEquals(a, 1);
}
import ballerina/test;

PasswordRequirement[] testData = [
    { password:"abcde", minOccurences:1, maxOccurences:3, requiredLetter:"a" },
    { password:"cdefg", minOccurences:1, maxOccurences:3, requiredLetter:"b" },
    { password:"ccccccccc", minOccurences:2, maxOccurences:9, requiredLetter:"c" }
];

@test:Config {}
function test1() {
    int a = part1(testData);
    test:assertEquals(a, 2);
}
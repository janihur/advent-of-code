import ballerina/io;
import ballerina/lang.'int;
import ballerina/test;

type PasswordRequirement record {|
    string password;
    int minOccurences;
    int maxOccurences;
    string requiredLetter;
|};

public function main() {
    // read puzzle data -------------------------------------------------------
    io:ReadableByteChannel byteChannel = checkpanic io:openReadableFile("../../inputs/2020-02");
    io:ReadableCharacterChannel characterChannel = new (byteChannel, "UTF-8");
    io:ReadableTextRecordChannel delimitedRecordChannel = 
        new (characterChannel, rs = "\\n", fs = " ");

    string[][] puzzleDataStr = [];
    while (delimitedRecordChannel.hasNext()) {
        string[] records = checkpanic delimitedRecordChannel.getNext();
        puzzleDataStr.push(records);
    }

    PasswordRequirement[] puzzleData = puzzleDataStr.map(function (string[] records) returns PasswordRequirement {
        int splitPoint = <int>records[0].indexOf("-", 0);
        return {
            password: records[2],
            minOccurences: checkpanic 'int:fromString(records[0].substring(0, splitPoint)),
            maxOccurences: checkpanic 'int:fromString(records[0].substring(splitPoint + 1)),
            requiredLetter: records[1].substring(0,1)
        };
    });

    // part 1 -----------------------------------------------------------------
    int a1 = part1(puzzleData);
    io:println("part 1 answer: ", a1);
    test:assertEquals(a1, 458);

    // part 2 -----------------------------------------------------------------
    int a2 = part2(puzzleData);
    io:println("part 2 answer: ", a2);
    test:assertEquals(a2, 342);
}

function part2(PasswordRequirement[] requirements) returns int {
    int validPasswords = 0;

    foreach var req in requirements {
        int requirementCount = 0;
        int & readonly pos1 = req.minOccurences - 1;
        int & readonly pos2 = req.maxOccurences - 1;

        requirementCount += req.password[pos1] == req.requiredLetter ? 1 : 0;
        requirementCount += req.password[pos2] == req.requiredLetter ? 1 : 0;

        if requirementCount == 1 {
            validPasswords += 1;
        }
    }

    return validPasswords;
}

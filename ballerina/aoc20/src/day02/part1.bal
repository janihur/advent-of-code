function part1(PasswordRequirement[] requirements) returns int {
    int validPasswords = 0;

    foreach var req in requirements {
        int letters = 0;
        foreach var letter in req.password {
            if letter == req.requiredLetter {
                letters += 1;
            }
        }
        if letters >= req.minOccurences &&
           letters <= req.maxOccurences {
            validPasswords += 1;
        }
    }
    
    return validPasswords;
}

import ballerina/io;
import ballerina/test;
import jani/util;

public function main() {
    // read puzzle data -------------------------------------------------------
    int[] puzzleData = util:readIntList("../../inputs/2020-10");
    // io:println(puzzleData);

    // part 1 -----------------------------------------------------------------
    int a1 = part1(puzzleData);
    io:println("part 1 answer: ", a1);
    test:assertEquals(a1, 1848);

    // part 2 -----------------------------------------------------------------
    int a2 = part2(puzzleData);
    io:println("part 2 answer: ", a2);
    test:assertEquals(a2, 8099130339328);
}

// import ballerina/io;
import ballerina/math;
import jani/util;

function part2(int[] jolts) returns int {
    int[] diffs = [];
    map<int> oneSequences = { "0": 0, "1": 0, "2": 0, "3": 0, "4": 0 };
    int[] sorted = jolts.clone().sort();
    sorted.unshift(0);
    sorted.push(sorted[sorted.length() - 1] + 3);
    // io:println("sorted: ", sorted);

    int onesLen = 0;
    foreach var index in 1 ... sorted.length() - 1 {
        int diff = sorted[index] - sorted[index - 1];
        diffs.push(diff);

        if diff == 1 {
            onesLen += 1;
        } else {
            var oldValue = <int>oneSequences[util:int2str(onesLen)];
            oneSequences[util:int2str(onesLen)] = oldValue + 1;
            onesLen = 0;
        }
    }

    // io:println("diffs: ", diffs);
    // io:println("oneSequences: ", oneSequences);

    var twos   = <float>oneSequences["2"];
    var threes = <float>oneSequences["3"];
    var fours  = <float>oneSequences["4"];

    // combinations (2, 4, 7) calculated manually with pen and paper
    //
    // a = number of [1,1] difference sequences, i.e. "twos"
    // b = number of [1,1,1] difference sequences, i.e. "threes"
    // c = number of [1,1,1,1] difference sequences, i.e. "fours"
    //
    // 2^a * 4^b * 7^c

    return 
          <int>math:pow(2, twos)
        * <int>math:pow(4, threes)
        * <int>math:pow(7, fours)
    ;
}
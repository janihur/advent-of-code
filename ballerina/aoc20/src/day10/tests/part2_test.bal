import ballerina/test;

@test:Config {}
function test2_part2_1() {
    int a = part2(testData1);
    test:assertEquals(a, 8);
}

@test:Config {}
function test2_part2_2() {
    int a = part2(testData2);
    test:assertEquals(a, 19208);
}
import jani/util;

function part1(int[] jolts) returns int {
    map<int> diffs = { "1": 0, "3": 0 };

    int[] sorted = jolts.clone().sort();
    sorted.unshift(0);
    sorted.push(sorted[sorted.length() - 1] + 3);

    foreach var index in 1 ... sorted.length() - 1 {
        int diff = sorted[index] - sorted[index - 1];
        diffs[util:int2str(diff)] = <int>diffs[util:int2str(diff)] + 1;
    }

    return <int>diffs["1"] * <int>diffs["3"];
}

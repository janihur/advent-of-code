import ballerina/io;

string & readonly EMPTY_SEAT = "L";
string & readonly OCCUPIED_SEAT = "#";

int xMax = 0;
int yMax = 0;

string[][] currentSeatMap = [];
string[][] nextSeatMap = [];

function part1(string[][] seatMap) returns int {
    xMax = seatMap[0].length() - 1;
    yMax = seatMap.length() - 1;

    currentSeatMap = seatMap.clone();
    nextSeatMap = seatMap.clone();

    io:println(string`(xMax = ${xMax})(yMax = ${yMax})`);

    foreach var index in 1 ... 6 {
        io:println(string`${index} ---`);
        print(currentSeatMap);
        
        foreach var 'tuple1 in currentSeatMap.enumerate() {
            var [y, line] = 'tuple1;
            foreach var 'tuple2 in line.enumerate() {
                var [x, seat] = 'tuple2;
                // io:print(string`((${x},${y}): ${seat})`);
                update(x, y, seat);
            }
            io:println();
        }

        currentSeatMap = nextSeatMap.clone();
 
        // io:println("---");
        // print(currentSeatMap);
    }

    io:println(string`final ---`);
    print(currentSeatMap);

    return 0;
}

function update(int x, int y, string seat) {
    match seat {
        EMPTY_SEAT => {
            if countAdjacentSeats(x, y, OCCUPIED_SEAT) == 0 {
                nextSeatMap[y][x] = OCCUPIED_SEAT;
            }
        }
        OCCUPIED_SEAT => {
            if countAdjacentSeats(x, y, OCCUPIED_SEAT) >= 4 {
                nextSeatMap[y][x] = EMPTY_SEAT;
            }
        }
    }
}

function countAdjacentSeats(int x, int y, string seatType) returns int {
    int count = 0;
    // up left
    if (x-1 >= 0) && (y-1 >= 0) {
        if currentSeatMap[y-1][x-1] == seatType {
            count += 1;
        }
    }
    // up
    if (y-1 >= 0) {
        if currentSeatMap[y-1][x] == seatType {
            count += 1;
        }
    }
    // up right
    if (x+1 <= xMax) && (y-1 >= 0) {
        if currentSeatMap[y-1][x+1] == seatType {
            count += 1;
        }
    }
    // left
    if (x-1 >= 0) {
        if currentSeatMap[y][x-1] == seatType {
            count += 1;
        }
    }
    // right
    if (x+1 <= xMax) {
        if currentSeatMap[y][x+1] == seatType {
            count += 1;
        }
    }
    // down left
    if (x-1 >= 0) && (y+1 <= yMax) {
        if currentSeatMap[y+1][x-1] == seatType {
            count += 1;
        }
    }
    // down 
    if (y+1 <= yMax) {
        if currentSeatMap[y+1][x] == seatType {
            count += 1;
        }
    }
    // down right
    if (x+1 <= xMax) && (y+1 <= yMax) {
        if currentSeatMap[y+1][x+1] == seatType {
            count += 1;
        }
    }

    io:print(string`(x: ${x} y: ${y} count: ${count})`);
    return count;
}

function print(string[][] seatMap) {
    foreach var line in seatMap {
        foreach var seat in line {
            io:print(seat);
        }
        io:println();
    }
}
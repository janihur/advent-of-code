import ballerina/test;

string[][] testData = [
    ["L",".","L","L",".","L","L",".","L","L"],
    ["L","L","L","L","L","L","L",".","L","L"],
    ["L",".","L",".","L",".",".","L",".","."],
    ["L","L","L","L",".","L","L",".","L","L"]
    // ["L",".","L","L",".","L","L",".","L","L"]
    // ["L",".","L","L","L","L","L",".","L","L"]
    // [".",".","L",".","L",".",".",".",".","."],
    // ["L","L","L","L","L","L","L","L","L","L"],
    // ["L",".","L","L","L","L","L","L",".","L"],
    // ["L",".","L","L","L","L","L",".","L","L"]
];

@test:Config {}
function test1_part1() {
    int a = part1(testData);
    test:assertEquals(a, 0);
}

@test:Config {}
function test1_countAdjacentSeats() {
    var seatMap = [
        ["#",".","#","#",".","#","#",".","#","#"],
        ["#","#","#","#","#","#","#",".","#","#"],
        ["#",".","#",".","#",".",".","#",".","."],
        ["#","#","#","#",".","#","#",".","#","#"],
        ["#",".","#","#",".","#","#",".","#","#"],
        ["#",".","#","#","#","#","#",".","#","#"],
        [".",".","#",".","#",".",".",".",".","."],
        ["#","#","#","#","#","#","#","#","#","#"],
        ["#",".","#","#","#","#","#","#",".","#"],
        ["#",".","#","#","#","#","#",".","#","#"]
    ];

    test:assertEquals(countAdjacentSeats(seatMap, 0, 0, "#"), 2);
 // test:assertEquals(countAdjacentSeats(seatMap, 1, 0, "#"), );
    test:assertEquals(countAdjacentSeats(seatMap, 2, 0, "#"), 4);
    test:assertEquals(countAdjacentSeats(seatMap, 3, 0, "#"), 4);
 // test:assertEquals(countAdjacentSeats(seatMap, 4, 0, "#"), );
    test:assertEquals(countAdjacentSeats(seatMap, 5, 0, "#"), 4);
    test:assertEquals(countAdjacentSeats(seatMap, 6, 0, "#"), 3);
 // test:assertEquals(countAdjacentSeats(seatMap, 7, 0, "#"), );
    test:assertEquals(countAdjacentSeats(seatMap, 8, 0, "#"), 3);
    test:assertEquals(countAdjacentSeats(seatMap, 9, 0, "#"), 3);

    test:assertEquals(countAdjacentSeats(seatMap, 0, 1, "#"), 3);
    test:assertEquals(countAdjacentSeats(seatMap, 1, 1, "#"), 6);
    test:assertEquals(countAdjacentSeats(seatMap, 2, 1, "#"), 5);
    test:assertEquals(countAdjacentSeats(seatMap, 3, 1, "#"), 6);
    test:assertEquals(countAdjacentSeats(seatMap, 4, 1, "#"), 5);
    test:assertEquals(countAdjacentSeats(seatMap, 5, 1, "#"), 5);
    test:assertEquals(countAdjacentSeats(seatMap, 6, 1, "#"), 4);
 // test:assertEquals(countAdjacentSeats(seatMap, 7, 1, "#"), );
    test:assertEquals(countAdjacentSeats(seatMap, 8, 1, "#"), 4);
    test:assertEquals(countAdjacentSeats(seatMap, 9, 1, "#"), 3);
}

import ballerina/io;
// import ballerina/test;

public function main() {
    // read puzzle data -------------------------------------------------------
    io:ReadableByteChannel byteChannel = checkpanic io:openReadableFile("../../inputs/2020-11");
    io:ReadableCharacterChannel characterChannel = new (byteChannel, "UTF-8");
    io:ReadableTextRecordChannel delimitedRecordChannel =
        new (characterChannel, rs = "\\n", fs = "");

    string[][] puzzleDataStr = [];
    while (delimitedRecordChannel.hasNext()) {
        string[] records = checkpanic delimitedRecordChannel.getNext();
        puzzleDataStr.push(records);
    }

    // io:println(puzzleDataStr);

    // var puzzleData = puzzleDataStr.map(function (string[] records) returns TODO {
    //     return TODO;
    // }

    // io:println(puzzleData);

    // part 1 -----------------------------------------------------------------
    // int a1 = part1(puzzleData);
    // io:println("part 1 answer: ", a1);
    // test:assertEquals(a1, 0);

    // part 2 -----------------------------------------------------------------
    // int a2 = part2(puzzleData);
    // io:println("part 2 answer: ", a2);
    // test:assertEquals(a2, 0);
}

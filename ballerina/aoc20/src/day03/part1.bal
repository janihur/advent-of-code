// import ballerina/io;
function toStr(boolean? b) returns string {
    if b is () {
        return "()";
    } else {
        return b ? "true" : "false";
    }
}

function nextPos(int x, int y) returns [int, int] {
    int nextX = x + 3;
    int nextY = y + 1;
    return [nextX, nextY];
}

function part1(Area area) returns int {
    int & readonly areaXOffset = area.width;

    int currentX = 0;
    int currentY = 0;

    int trees = 0;
    while (currentY < area.height) {
        [currentX, currentY] = nextPos(currentX, currentY);
        var hasTree = area.treeLocation[toKey(currentX, currentY)];

        // io:println("---");
        // io:println(string`currentX: ${currentX} currentY: ${currentY} hasTree: ${toStr(hasTree)}`);

        if hasTree is () {
            // io:println("has to adjust coordinates first");
            currentX -= areaXOffset;
            hasTree = area.treeLocation[toKey(currentX, currentY)];
            // io:println(string`currentX: ${currentX} currentY: ${currentY} hasTree: ${toStr(hasTree)}`);
            if hasTree is () {
                // nothing to do
            } else {
                trees += hasTree ? 1 : 0;
            }
        } else {
            trees += hasTree ? 1 : 0;
        }
    }

    return trees;
}
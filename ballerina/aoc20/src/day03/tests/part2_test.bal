import ballerina/test;

@test:Config {}
function testCountTrees() {
    var area = getArea(testDataStr);

    int a = countTrees(area, [1, 1]);
    test:assertEquals(a, 2);
    int b = countTrees(area, [3, 1]);
    test:assertEquals(b, 7);
    int c = countTrees(area, [5, 1]);
    test:assertEquals(c, 3);
    int d = countTrees(area, [7, 1]);
    test:assertEquals(d, 4);
    int e = countTrees(area, [1, 2]);
    test:assertEquals(e, 2);
}

@test:Config {}
function testPart2() {
   var area = getArea(testDataStr);
   int a = part2(area);
   test:assertEquals(a, 336);
}

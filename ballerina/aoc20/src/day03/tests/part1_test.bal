import ballerina/test;

string[][] testDataStr = [
    [".",".","#","#",".",".",".",".",".",".","."],
    ["#",".",".",".","#",".",".",".","#",".","."],
    [".","#",".",".",".",".","#",".",".","#","."],
    [".",".","#",".","#",".",".",".","#",".","#"],
    [".","#",".",".",".","#","#",".",".","#","."],
    [".",".","#",".","#","#",".",".",".",".","."],
    [".","#",".","#",".","#",".",".",".",".","#"],
    [".","#",".",".",".",".",".",".",".",".","#"],
    ["#",".","#","#",".",".",".","#",".",".","."],
    ["#",".",".",".","#","#",".",".",".",".","#"],
    [".","#",".",".","#",".",".",".","#",".","#"]
];

@test:Config {}
function testTestData() {
    test:assertEquals(testDataStr[0][1], ".");
    test:assertEquals(testDataStr[1][0], "#");
    test:assertEquals(testDataStr[2][6], "#");
    test:assertEquals(testDataStr[10][9], ".");
    test:assertEquals(testDataStr[9][10], "#");
}

@test:Config {}
function testKeyFunctions() {
    var s = toKey(12, 97);
    test:assertEquals(s, "12,97");

    var [x, y] = fromKey(s);
    test:assertEquals(x, 12);
    test:assertEquals(y, 97);
}

@test:Config {}
function testGetArea() {
    var area = getArea(testDataStr);
    // printArea(testDataStr);
    test:assertEquals(area.width, 11);
    test:assertEquals(area.height, 11);
    test:assertEquals(area.treeLocation[toKey(0, 1)], true);
    test:assertEquals(area.treeLocation[toKey(1, 0)], false);
    test:assertEquals(area.treeLocation[toKey(6, 2)], true);
    test:assertEquals(area.treeLocation[toKey(9, 10)], false);
    test:assertEquals(area.treeLocation[toKey(10, 9)], true);
}

@test:Config {}
function testPart1() {
    int a = part1(getArea(testDataStr));
    test:assertEquals(a, 7);
}
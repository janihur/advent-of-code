function part2(Area area) returns int {
    [int, int][] & readonly slopes = [
        [1, 1],
        [3, 1],
        [5, 1],
        [7, 1],
        [1, 2]
    ];
    int product = 1;

    foreach var slope in slopes {
        product *= countTrees(area, slope);
    }

    return product;
}

function countTrees(Area area, [int, int] slope) returns int {
    int & readonly areaXOffset = area.width;
    var [xSlope, ySlope] = slope;

    int currentX = 0;
    int currentY = 0;

    int trees = 0;

    while (currentY < area.height) {
        currentX += xSlope;
        currentY += ySlope;

        var hasTree = area.treeLocation[toKey(currentX, currentY)];

        if hasTree is () {
            currentX -= areaXOffset;
            hasTree = area.treeLocation[toKey(currentX, currentY)];
            if hasTree is () {
                // nothing
            } else {
                trees += hasTree ? 1 : 0;
            }
        } else {
            trees += hasTree ? 1 : 0;
        }
    }

    return trees;
}

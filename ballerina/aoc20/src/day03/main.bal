import ballerina/io;
import ballerina/lang.'int;
import ballerina/test;

type Area record {|
    int width;
    int height;
    map<boolean> treeLocation;
|};

function toKey(int x, int y) returns string {
    return string `${x},${y}`;
}

function fromKey(string 'key) returns ([int, int]) {
    int splitPoint = <int>'key.indexOf(",", 0);
    int x = checkpanic 'int:fromString('key.substring(0, splitPoint));
    int y = checkpanic 'int:fromString('key.substring(splitPoint + 1));
    return [x, y];
}

function getArea(string[][] puzzleDataStr) returns Area {
    Area area = {
        width: puzzleDataStr[0].length(),
        height: puzzleDataStr.length(),
        treeLocation: {}
    };

    foreach var y in 0 ... area.height - 1 {
        foreach var x in 0 ... area.width - 1 {
            area.treeLocation[toKey(x, y)] = puzzleDataStr[y][x] == "#" ? true : false;
        }
    }

    return area;
}

function printArea(string[][] puzzleDataStr) {
    foreach var y in 0 ... puzzleDataStr.length() - 1 {
        foreach var x in 0 ... puzzleDataStr[y].length() - 1 {
            io:print(puzzleDataStr[y][x]);
        }
        io:println();
    }
}

public function main() {
    // read puzzle data -------------------------------------------------------
    io:ReadableByteChannel byteChannel = checkpanic io:openReadableFile("../../inputs/2020-03");
    io:ReadableCharacterChannel characterChannel = new (byteChannel, "UTF-8");
    io:ReadableTextRecordChannel delimitedRecordChannel = 
        new (characterChannel, rs = "\\n", fs = "");

    string[][] puzzleDataStr = [];
    while (delimitedRecordChannel.hasNext()) {
        string[] records = checkpanic delimitedRecordChannel.getNext();
        puzzleDataStr.push(records);
    }
   
    Area area = getArea(puzzleDataStr);
    
    // part 1 -----------------------------------------------------------------
    int a1 = part1(area);
    io:println("part 1 answer: ", a1);
    test:assertEquals(a1, 289);

    // part 2 -----------------------------------------------------------------
    int a2 = part2(area);
    io:println("part 2 answer: ", a2);
    test:assertEquals(a2, 5522401584);
}

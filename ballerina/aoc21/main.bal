import aoc21.day01;
import aoc21.day02;
import aoc21.day03;
import aoc21.day04;
import aoc21.day05;
import aoc21.day06;
import aoc21.day07;
import aoc21.day08;
import aoc21.day09;
import aoc21.day10;
import ballerina/io;
import ballerina/test;

public type InputPuzzleId record {
    int day;
    int? part;
};
public function main(*InputPuzzleId inputPuzzle) returns error? {
    check validate(inputPuzzle);
    final PuzzleId puzzleId = toPuzzleId(inputPuzzle);
       
    match puzzleId.day {
         1 => { puzzleRun(puzzleId, expected = [1583, 1627],            runner = day01:run); }
         2 => { puzzleRun(puzzleId, expected = [1488669, 1176514794],   runner = day02:run); }
         3 => { puzzleRun(puzzleId, expected = [2003336, 1877139],      runner = day03:run); }
         4 => { puzzleRun(puzzleId, expected = [44088, 23670],          runner = day04:run); }
         5 => { puzzleRun(puzzleId, expected = [7297, 21038],           runner = day05:run); }
         6 => { puzzleRun(puzzleId, expected = [350605, 1592778185024], runner = day06:run); }
         7 => { puzzleRun(puzzleId, expected = [336120, 96864235],      runner = day07:run); }
         8 => { puzzleRun(puzzleId, expected = [239, 946346],           runner = day08:run); }
         9 => { puzzleRun(puzzleId, expected = [532, 1110780],          runner = day09:run); }
        10 => { puzzleRun(puzzleId, expected = [392043, 1605968119],    runner = day10:run); }
         _ => {
            return error(string`not implemented: ${toDayString(inputPuzzle)}`);
        }
    }
}

type PuzzleId record { int day; int[] parts; };

function toDayString(InputPuzzleId puzzle) returns string {
    return string`(day ${puzzle.day})`;
}

function toPartString(InputPuzzleId puzzle) returns string {
    if puzzle.part is int {
        return string`(part ${<int>puzzle.part})`;
    } else {
        return string`(part nil)`;
    }
}

function isValid(InputPuzzleId puzzle) returns boolean {
    if puzzle.day < 1 || puzzle.day > 25 { return false; }
    if puzzle.part is int && puzzle.part != 1 && puzzle.part != 2 { return false; }
    return true;
}

function validate(InputPuzzleId puzzle) returns error? {
    final string dayStr = toDayString(puzzle);
    final string partStr = toPartString(puzzle);
    final string puzzleStr = string`${dayStr}${partStr}`;

    if !isValid(puzzle) {
        return error(string`no such puzzle: ${puzzleStr}`);
    }
}

function toPuzzleId(InputPuzzleId input) returns PuzzleId {
    int[] parts = [];
    
    if input.part is int {
        parts.push(<int>input.part);
    } else {
        parts.push(1);
        parts.push(2);
    }

    return {
        day: input.day,
        parts: parts
    };
}

function puzzleRun(
     PuzzleId puzzleId
    ,int[] expected
    ,function(int[]) returns record { int? part1; int? part2; } runner
) {
    var answers = runner(puzzleId.parts);

    if answers.part1 is int {
        io:println(string`(day ${puzzleId.day})(part 1)(answer ${<int>answers.part1})`);
        test:assertEquals(answers.part1, expected[0], string`Wrong answer to part 1`);
    }

    if answers.part2 is int {
        io:println(string`(day ${puzzleId.day})(part 2)(answer ${<int>answers.part2})`);
        test:assertEquals(answers.part2, expected[1], string`Wrong answer to part 2`);
    }
}
import ballerina/test;

final string[] DIAGNOSTIC_REPORT_1 = [
    "00100",
    "11110",
    "10110",
    "10111",
    "10101",
    "01111",
    "00111",
    "11100",
    "10000",
    "11001",
    "00010",
    "01010"
];

@test:Config {}
function test_countZeroBits_1() {
    final int[] zeroBits = countZeroBits(DIAGNOSTIC_REPORT_1);
    final int[] expected = [5, 7, 4, 5, 7];
    test:assertEquals(zeroBits, expected);
}

@test:Config {}
function test_bitCriteriaBuilderFactory_1() {
    final int reportLength = DIAGNOSTIC_REPORT_1.length();
    final int[] zeroBits = [5, 7, 4, 5, 7];

    final string[] bitCriteria = zeroBits.map(
        bitCriteriaBuilderFactory(["1", "0", "1"], reportLength / 2)
    );
    test:assertEquals(bitCriteria, ["1", "0", "1", "1", "0"]);  
}

@test:Config {}
function test_bitCriteriaBuilderFactory_2() {
    final int reportLength = DIAGNOSTIC_REPORT_1.length();
    final int[] zeroBits = [5, 7, 4, 5, 7];

    final string[] bitCriteria = zeroBits.map(
        bitCriteriaBuilderFactory(["0", "1", "0"], reportLength / 2)
    );
    test:assertEquals(bitCriteria, ["0", "1", "0", "0", "1"]);  
}

import aoc21.util;
import ballerina/io;
public function run(int[] parts) returns record { int? part1; int? part2; } {
    // read puzzle data -------------------------------------------------------
    string[] puzzleData = checkpanic io:fileReadLines("../../inputs/2021-03");
    // run puzzle solvers -----------------------------------------------------
    final record { int? part1; int? part2; } answers = { part1: (), part2: () };
    foreach int part in parts {
        match part {
            1 => { answers.part1 = part1(puzzleData); }
            2 => { answers.part2 = part2(puzzleData); }
        }
    }
    return answers;
}

function part1(string[] diagnosticReport) returns int {
    final int reportLength = diagnosticReport.length();
    final int[] zeroBits = countZeroBits(diagnosticReport);

    string gammaRate = "";
    string epsilonRate = "";

    foreach var count in zeroBits {
        if count > reportLength / 2 {
            gammaRate += "0";
            epsilonRate += "1";
        } else {
            gammaRate += "1";
            epsilonRate += "0";
        }
    }

    // io:println("gammaRate: " + gammaRate);
    // io:println("epsilonRate: " + epsilonRate);

    return util:stringBinaryNumberToInt(gammaRate) * util:stringBinaryNumberToInt(epsilonRate);
}

function part2(string[] diagnosticReport) returns int {
    string oxygenGeneratorRating = "";
    {
        string[] oxygenGeneratorRatingCandicates = diagnosticReport.clone();
        
        int criteriaBitIndex = 0;
        while oxygenGeneratorRatingCandicates.length() > 1 {
            final int reportLength = oxygenGeneratorRatingCandicates.length();
            final var bitCriteriaFn = bitCriteriaBuilderFactory(["1","0","1"], reportLength / 2);
            final int[] zeroBits = countZeroBits(oxygenGeneratorRatingCandicates);
            final string[] oxygenGeneratorBitCriteria = zeroBits.map(bitCriteriaFn);

            final string[] filtered = oxygenGeneratorRatingCandicates.filter(function (string value) returns boolean {
                return value[criteriaBitIndex] == oxygenGeneratorBitCriteria[criteriaBitIndex];
            });

            oxygenGeneratorRatingCandicates = filtered.clone();
            criteriaBitIndex += 1;
        }

        oxygenGeneratorRating = oxygenGeneratorRatingCandicates[0];
    }
    // io:println("oxygenGeneratorRating: " + oxygenGeneratorRating);

    // ------------------------------------------------------------------------

    string co2ScrubberRating = "";
    {
        string[] co2ScrubberRatingCandicates = diagnosticReport.clone();
        
        int criteriaBitIndex = 0;
        while co2ScrubberRatingCandicates.length() > 1 {
            final int reportLength = co2ScrubberRatingCandicates.length();
            final var bitCriteriaFn = bitCriteriaBuilderFactory(["0","1","0"], reportLength / 2);
            final int[] zeroBits = countZeroBits(co2ScrubberRatingCandicates);
            final string[] co2ScrubberBitCriteria = zeroBits.map(bitCriteriaFn);

            final string[] filtered = co2ScrubberRatingCandicates.filter(function (string value) returns boolean {
                return value[criteriaBitIndex] == co2ScrubberBitCriteria[criteriaBitIndex];
            });

            co2ScrubberRatingCandicates = filtered.clone();
            criteriaBitIndex += 1;

        }

        co2ScrubberRating = co2ScrubberRatingCandicates[0];
    }
    // io:println("co2ScrubberRating: " + co2ScrubberRating);

    return util:stringBinaryNumberToInt(oxygenGeneratorRating) * util:stringBinaryNumberToInt(co2ScrubberRating);
}

function countZeroBits(string[] diagnosticReport) returns int[] {
    final int bitLength = diagnosticReport[0].length();
    
    int[] zeroBits =[];
    zeroBits.setLength(bitLength);

    foreach var binaryNumber in diagnosticReport {
        foreach var bitIndex in 0 ..< bitLength {
            if binaryNumber[bitIndex] == "0" {
                zeroBits[bitIndex] += 1;
            }
        }
    }
    return zeroBits;   
}

function bitCriteriaBuilderFactory(string:Char[] values, int 'limit) returns function(int) returns string {
    return function (int value) returns string {
        if value == 'limit {
            return values[0];
        }
        else if value > 'limit {
            return values[1];
        } else {
            return values[2];
        }
    };
}
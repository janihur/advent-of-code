import ballerina/io;
public function run(int[] parts) returns record { int? part1; int? part2; } {
    // read puzzle data -------------------------------------------------------
    string[] puzzleData = checkpanic io:fileReadLines("../../inputs/2021-10");
    // run puzzle solvers -----------------------------------------------------
    final record { int? part1; int? part2; } answers = { part1: (), part2: () };
    foreach int part in parts {
        match part {
            1 => { answers.part1 = part1(puzzleData); }
            2 => { answers.part2 = part2(puzzleData); }
        }
    }
    return answers;
}

// Opens and Closes are the same language construct with different syntax

const OPEN1 = "(";
const OPEN2 = "[";
const OPEN3 = "{";
const OPEN4 = "<";

type Opens OPEN1|OPEN2|OPEN3|OPEN4;

enum Closes {
    CLOSE1 = ")",
    CLOSE2 = "]",
    CLOSE3 = "}",
    CLOSE4 = ">"
}

// type Closes CLOSE1|CLOSE2|CLOSE3|CLOSE4;

function part1(string[] lines) returns int {
    map<int> illegalChars = { ")": 0, "]": 0, "}": 0, ">": 0 };
    string:Char[] stack = [];
    foreach var line in lines {
        foreach var char in line {
            if char is Opens {
                stack.push(char);
            } else {
                final string:Char closeChar = char;
                final string:Char openChar = stack.pop();
                match openChar {
                    OPEN1 => {
                        if closeChar != CLOSE1 {
                            illegalChars[closeChar] = illegalChars.get(closeChar) + 1;
                            break;
                        }
                    }
                    OPEN2 => {
                        if closeChar != CLOSE2 {
                            illegalChars[closeChar] = illegalChars.get(closeChar) + 1;
                            break;
                        }
                    }
                    OPEN3 => {
                        if closeChar != CLOSE3 {
                            illegalChars[closeChar] = illegalChars.get(closeChar) + 1;
                            break;
                        }
                    }
                    OPEN4 => {
                        if closeChar != CLOSE4 {
                            illegalChars[closeChar] = illegalChars.get(closeChar) + 1;
                            break;
                        }
                    }
                }
            }
        }
    }
    return 
        illegalChars.get(CLOSE1) * 3 + 
        illegalChars.get(CLOSE2) * 57 + 
        illegalChars.get(CLOSE3) * 1197 +
        illegalChars.get(CLOSE4) * 25137
    ;
}

function corruptedChar(string line) returns string:Char? {
    string:Char[] stack = [];
    foreach var char in line {
        if char is Opens {
            stack.push(char);
        } else {
            final string:Char closeChar = char;
            final string:Char openChar = stack.pop();
            match openChar {
                OPEN1 => {
                    if closeChar != CLOSE1 {
                        return closeChar;
                    }
                }
                OPEN2 => {
                    if closeChar != CLOSE2 {
                        return closeChar;
                    }
                }
                OPEN3 => {
                    if closeChar != CLOSE3 {
                        return closeChar;
                    }
                }
                OPEN4 => {
                    if closeChar != CLOSE4 {
                        return closeChar;
                    }
                }
            }
        }
    }
    return ();
}

function part2(string[] lines) returns int {
    int[] scores = [];

    foreach var line in lines {
        if corruptedChar(line) is string:Char {
            continue; // discard corrupted lines
        }
        // line is incomplete
        string:Char[] stack = [];
        foreach var char in line {
            if char is Opens {
                stack.push(char);
            } else {
                _ = stack.pop();
            }
        }
        // io:println(stack);
        // complete the line and score it
        int score = 0;
        foreach var char in stack.reverse() {
            score *= 5;
            match char {
                OPEN1 => { score += 1; }
                OPEN2 => { score += 2; }
                OPEN3 => { score += 3; }
                OPEN4 => { score += 4; }
            }
            
        }
        scores.push(score);
    }
    // io:println(scores);
    {
        final int index = scores.length() / 2;
        return scores.sort()[index];
    }
}
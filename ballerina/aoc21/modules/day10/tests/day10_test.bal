import ballerina/test;

final string[] PUZZLE_DATA = [
    "[({(<(())[]>[[{[]{<()<>>",
    "[(()[<>])]({[<{<<[]>>(",
    "{([(<{}[<>[]}>{[]{[(<()>",
    "(((({<>}<{<{<>}{[]{[]{}",
    "[[<[([]))<([[{}[[()]]]",
    "[{[{({}]{}}([{[{{{}}([]",
    "{<[[]]>}<{[{[{[]{()[[[]",
    "[<(<(<(<{}))><([]([]()",
    "<{([([[(<>()){}]>(<<{{",
    "<{([{{}}[<[[[<>{}]]]>[]]"
];

@test:Config {}
function test_part_1() {
    test:assertEquals(part1(PUZZLE_DATA), 26397);
}

@test:Config {}
function test_part_2() {
    test:assertEquals(part2(PUZZLE_DATA), 288957);
}

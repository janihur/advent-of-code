import aoc21.util;
import ballerina/io;
public function run(int[] parts) returns record { int? part1; int? part2; } {
    // read puzzle data -------------------------------------------------------
    final string[] stringData = checkpanic io:fileReadLines("../../inputs/2021-06");
    int[] puzzleData = util:split(stringData[0]).map(function (string str) returns int {
        return util:str2int(str);
    });
    // run puzzle solvers -----------------------------------------------------
    final record { int? part1; int? part2; } answers = { part1: (), part2: () };
    foreach int part in parts {
        match part {
            1 => { answers.part1 = part1(puzzleData); }
            2 => { answers.part2 = part2(puzzleData); }
        }
    }
    return answers;
}

function part1Impl1(int[] lanternFishes) returns int {
    foreach var day in 1 ... 80 {
        int newFishes = 0;
        foreach var i in 0 ..< lanternFishes.length() {
            lanternFishes[i] -= 1;
            if lanternFishes[i] == -1 {
                newFishes += 1;
                lanternFishes[i] = 6;
            }
        }
        foreach var _ in 0 ..< newFishes {
            lanternFishes.push(8);
        }
    }
    return lanternFishes.length();
}

function part1(int[] lanternFishes_) returns int {
    int[][] lanternFishes = util:partition(lanternFishes_.sort());
    foreach var day in 1 ... 80 {
        int newFishes = 0;
        foreach var i in 0 ..< lanternFishes.length() {
            lanternFishes[i][0] -= 1;
            if lanternFishes[i][0] == -1 {
                newFishes += lanternFishes[i][1];
                lanternFishes[i][0] = 6;
            }
        }
        if newFishes > 0 {
            lanternFishes.push([8, newFishes]);
        }
        // TODO compact but was fast enough without
    }

    return lanternFishes.reduce(function (int total, int[] n) returns int { return total + n[1]; }, 0);
}

function part2(int[] lanternFishes_) returns int {
    int[][] lanternFishes = util:partition(lanternFishes_.sort());
    foreach var day in 1 ... 256 {
        int newFishes = 0;
        foreach var i in 0 ..< lanternFishes.length() {
            lanternFishes[i][0] -= 1;
            if lanternFishes[i][0] == -1 {
                newFishes += lanternFishes[i][1];
                lanternFishes[i][0] = 6;
            }
        }
        if newFishes > 0 {
            lanternFishes.push([8, newFishes]);
        }
        // TODO compact but was fast enough without
    }

    return lanternFishes.reduce(function (int total, int[] n) returns int { return total + n[1]; }, 0);
}
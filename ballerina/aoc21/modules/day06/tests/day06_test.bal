import ballerina/test;

final int[] LANTERN_FISHES = [3,4,3,1,2];

@test:Config {}
function test_part_1() {
    // test:assertEquals(part1(LANTERN_FISHES), 26); // 18 iterations
    test:assertEquals(part1(LANTERN_FISHES), 5934); // 80 iterations
}

@test:Config {}
function test_part_2() {
    test:assertEquals(part2(LANTERN_FISHES), 26984457539); // 256 iterations
}

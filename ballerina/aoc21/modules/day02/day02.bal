import ballerina/io;
public function run(int[] parts) returns record { int? part1; int? part2; } {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2021-02");
    Command[] puzzleData = stringData.map(function (string line) returns Command {
        int splitPos = <int>line.indexOf(" ");
        return {
            movement: line.substring(0, splitPos),
            amount: checkpanic 'int:fromString(line.substring(splitPos + 1))
        };
    });
    // run puzzle solvers -----------------------------------------------------
    final record { int? part1; int? part2; } answers = { part1: (), part2: () };
    foreach int part in parts {
        match part {
            1 => { answers.part1 = part1Impl3(puzzleData); }
            2 => { answers.part2 = part2(puzzleData); }
        }
    }
    return answers;
}

type Command record {|
    string movement;
    int amount;
|};

// implementation #1 - iterate with foreach-loop
function part1Impl1(Command[] commands) returns int {
    int horizontalPosition = 0;
    int depth = 0;

    foreach Command c in commands {
        match c.movement {
            "forward" => { horizontalPosition += c.amount; }
            "down" => { depth += c.amount; }
            "up" => { depth -= c.amount; }
        }
    }

    return horizontalPosition * depth;
}

// implementation #2 - iterate with forEach-function
function part1Impl2(Command[] commands) returns int {
    int horizontalPosition = 0;
    int depth = 0;

    commands.forEach(function (Command c) {
        match c.movement {
            "forward" => { horizontalPosition += c.amount; }
            "down" => { depth += c.amount; }
            "up" => { depth -= c.amount; }
        }
    });

    return horizontalPosition * depth;
}

// implementation #3 - iterate with reduce-function
type FinalPosition record {|
    int horizontalPosition;
    int depth;
|};

function part1Impl3(Command[] commands) returns int {
    final FinalPosition fp = commands.reduce(
        function(FinalPosition accu, Command c) returns FinalPosition {
            match c.movement {
                "forward" => { accu.horizontalPosition += c.amount; }
                "down" => { accu.depth += c.amount; }
                "up" => { accu.depth -= c.amount; }
            }
            return accu;
        }
        , // initial value for accumulator
        {
            horizontalPosition: 0,
            depth: 0
        }
    );

    return fp.horizontalPosition * fp.depth;
}

function part2(Command[] commands) returns int {
    int horizontalPosition = 0;
    int depth = 0;
    int aim = 0;

    foreach Command c in commands {
        match c.movement {
            "forward" => { 
                horizontalPosition += c.amount; 
                depth += (aim* c.amount);
            }
            "down" => { aim += c.amount; }
            "up" => { aim -= c.amount; }
        }
    }

    return horizontalPosition * depth;
}
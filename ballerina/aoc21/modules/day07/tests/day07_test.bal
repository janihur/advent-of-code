import ballerina/test;

final int[] HORIZONTAL_POSITIONS = [16,1,2,0,4,2,7,1,2,14];

@test:Config {}
function test_print() {
    printFuelConsumptionForAllPositions(HORIZONTAL_POSITIONS, minPos = 0, maxPos = 16);
}

@test:Config {}
function test_part_1() {
    test:assertEquals(part1(HORIZONTAL_POSITIONS), 37);
}

@test:Config {}
function test_part_2() {
    test:assertEquals(part2(HORIZONTAL_POSITIONS, 2), 168);
}

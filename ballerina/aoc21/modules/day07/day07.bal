import aoc21.util;
import ballerina/io;
public function run(int[] parts) returns record { int? part1; int? part2; } {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2021-07");
    int[] puzzleData = util:split(stringData[0]).map(function (string str) returns int {
        return util:str2int(str);
    });
    // run puzzle solvers -----------------------------------------------------
    final record { int? part1; int? part2; } answers = { part1: (), part2: () };
    foreach int part in parts {
        match part {
            1 => { answers.part1 = part1(puzzleData); }
            2 => { answers.part2 = part2(puzzleData, 309); }
        }
    }
    return answers;
}

function part1(int[] horizontalPositions) returns int {
    final var sorted = horizontalPositions.sort();
    // returns 2 indexes if array length is even
    final var indexes = util:indexOfMedian(sorted);
    // unique indexes
    util:IntSet candidates = new;
    candidates.add(sorted[indexes.a]);
    if indexes.b is int {
        candidates.add(sorted[<int>indexes.b]);
    }

    int fuelConsumption = -1;
    foreach var pos in candidates.values() {
        int x = sorted.reduce(function (int total, int crab) returns int {
            return total + int:abs(crab - pos);
        }, 0);
        fuelConsumption = x > fuelConsumption ? x : fuelConsumption;
    }

    return fuelConsumption;
}

function part2(int[] horizontalPositions, int medianPosition) returns int {
    final int[] sorted = horizontalPositions.sort();
    final int maxPos = sorted[sorted.length()-1];
    // io:println(minPos); // 0
    // io:println(maxPos); // 1919
    int min = int:MAX_VALUE;
    // checked "manually" this is the correct direction ...
    foreach var pos in medianPosition ... maxPos {
        int x = sorted.reduce(function (int total, int crab) returns int {
            return total + fuelConsumption(int:abs(crab - pos));
        }, 0);
        min = x < min ? x : min;
        // io:println(pos.toString() + ": " + x.toString());
        if x > min { break; }
    }
    return min;
}

// TODO: from internet: 1+2+3+...+N = N*(N+1)/2
function fuelConsumption(int distance) returns int {
    int consumption = 0;
    foreach var i in 1 ... distance {
        consumption += i;
    }
    return consumption;
}

function printFuelConsumptionForAllPositions(int[] horizontalPositions, int minPos, int maxPos) {
    foreach var pos in minPos ... maxPos {
        final int fuel1 = horizontalPositions.reduce(function (int total, int crab) returns int {
            return total + int:abs(crab - pos);
        }, 0);
        final int fuel2 = horizontalPositions.reduce(function (int total, int crab) returns int {
            return total + fuelConsumption(int:abs(crab - pos));
        }, 0);
        io:println(string`${pos}: ${fuel1} : ${fuel2}`);
    }
}
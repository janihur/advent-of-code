import aoc21.util;
import ballerina/io;
public function run(int[] parts) returns record { int? part1; int? part2; } {
    // read puzzle data -------------------------------------------------------
    final string[] stringData = checkpanic io:fileReadLines("../../inputs/2021-05");
    Line[] puzzleData = stringData.map(function (string line) returns Line {
        final string[] parts_ = util:split(line, " -> ");
        final string[] one = util:split(parts_[0]);
        final string[] two = util:split(parts_[1]);
        return {
            x1: util:str2int(one[0]),
            y1: util:str2int(one[1]),
            x2: util:str2int(two[0]),
            y2: util:str2int(two[1])
        };
    });

    // run puzzle solvers -----------------------------------------------------
    final record { int? part1; int? part2; } answers = { part1: (), part2: () };
    foreach int part in parts {
        match part {
            1 => { answers.part1 = part1(puzzleData); }
            2 => { answers.part2 = part2(puzzleData); }
        }
    }
    return answers;
}

type Line record {
    int x1;
    int y1;
    int x2;
    int y2;
};

function part1(Line[] lines_) returns int {
    final int CANVAS_SIZE = findCanvasSize(lines_);
    int[][] canvas = initCanvas(CANVAS_SIZE);
    final Line[] lines = lines_.filter(function (Line line) returns boolean {return line.x1 == line.x2 || line.y1 == line.y2 ;});
    lines.forEach(function (Line line) {
        final int[] xs = util:range(line.x1, line.x2);
        final int[] ys = util:range(line.y1, line.y2);
        final int[][] points = util:zip(xs, ys);
        plot(canvas, points);
    });
    int overlaps = 0;
    foreach var row in canvas {
        foreach var col in row {
            if col > 1 { overlaps += 1; }
        }
    }

    return overlaps;
}

function part2(Line[] lines) returns int {
    final int CANVAS_SIZE = findCanvasSize(lines);
    int[][] canvas = initCanvas(CANVAS_SIZE);
    lines.forEach(function (Line line) {
        final int[] xs = util:range(line.x1, line.x2);
        final int[] ys = util:range(line.y1, line.y2);
        final int[][] points = util:zip(xs, ys);
        plot(canvas, points);
    });
    int overlaps = 0;
    foreach var row in canvas {
        foreach var col in row {
            if col > 1 { overlaps += 1; }
        }
    }

    return overlaps;
}

function plot(int[][] canvas, int[][] points) {
    foreach var point in points {
        canvas[point[1]][point[0]] += 1;
    }
}

function initCanvas(int size) returns int[][] {
    int[] row = [];

    foreach var i in 0 ..< size {
        row.push(0);
    }

    int[][] canvas = [];

    foreach var i in 0 ..< size {
        canvas.push(row.clone());
    }

    return canvas;
}

function findCanvasSize(Line[] lines) returns int {
    final Line max = { x1: 0, y1: 0, x2: 0, y2: 0 };

    foreach var line in lines {
        if line.x1 > max.x1 { max.x1 = line.x1; }
        if line.y1 > max.y1 { max.y1 = line.y1; }
        if line.x2 > max.x2 { max.x2 = line.x2; }
        if line.y2 > max.y2 { max.y2 = line.y2; }
    }

    return int:max(max.x1, max.y1, max.x2, max.y2) + 1;
}
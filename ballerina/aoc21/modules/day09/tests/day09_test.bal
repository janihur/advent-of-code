import ballerina/test;

final int[][] PUZZLE_DATA = [
    [2, 1, 9, 9, 9, 4, 3, 2, 1, 0],
    [3, 9, 8, 7, 8, 9, 4, 9, 2, 1],
    [9, 8, 5, 6, 7, 8, 9, 8, 9, 2],
    [8, 7, 6, 7, 8, 9, 6, 7, 8, 9],
    [9, 8, 9, 9, 9, 6, 5, 6, 7, 8]
];
@test:Config {}
function test_part_1() {
    test:assertEquals(part1(PUZZLE_DATA), 15);
}

@test:Config {}
function test_part_2() {
    test:assertEquals(part2(PUZZLE_DATA), 1134);
}

import aoc21.util;
import ballerina/lang.array;
import ballerina/io;
public function run(int[] parts) returns record { int? part1; int? part2; } {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/2021-09");
    int[][] puzzleData = stringData.map(function (string line) returns int[] {
        int[] ints = [];
        foreach var char in line {
            ints.push(util:str2int(char));
        }
        return ints;
    });
    // run puzzle solvers -----------------------------------------------------
    final record { int? part1; int? part2; } answers = { part1: (), part2: () };
    foreach int part in parts {
        match part {
            1 => { answers.part1 = part1(puzzleData); }
            2 => { answers.part2 = part2(puzzleData); }
        }
    }
    return answers;
}

type Coordinate record {
    int x;
    int y;
};
type Coordinates Coordinate[];
type NeighbourCoordinates Coordinates[][];

function initHeightMapNeighbours(int maxX, int maxY) returns NeighbourCoordinates {
    final Coordinates singlePointNeighbours = [];
    final Coordinates[] rowNeighbours = [];
    foreach var x in 0 ... maxX {
        rowNeighbours.push(singlePointNeighbours.clone());
    }

    final NeighbourCoordinates allNeighbours = [];
    foreach var y in 0 ... maxY {
        allNeighbours.push(rowNeighbours.clone());
        
    }

    return allNeighbours;
}

function createBoolean2dArray(int maxX, int maxY, boolean initValue) returns boolean[][] {
    final boolean[][] arr2d = [];
    arr2d.setLength(maxY + 1);
    final boolean[] row = [];
    row.setLength(maxX + 1);
    foreach var x in 0 ... maxX {
        row[x] = initValue;
    }
    foreach var y in 0 ... maxY {
        arr2d[y] = row.clone();
    }
    return arr2d;
}

function getNeighbourCoordinates(int minX, int maxX, int minY, int maxY) returns NeighbourCoordinates {
    final NeighbourCoordinates heightMapNeighbours = initHeightMapNeighbours(maxX, maxY);
    foreach var y in minY ... maxY {
        foreach var x in minX ... maxX {
            final Coordinates singlePointNeighbours = [];
            if y - 1 >= minY { // above
                singlePointNeighbours.push({x: x, y: y - 1});
            }
            if x + 1 <= maxX { // right
                singlePointNeighbours.push({x: x + 1, y: y});
            }
            if y + 1 <= maxY { // below
                singlePointNeighbours.push({x: x, y: y + 1});
            }
            if x - 1 >= minX { // left
                singlePointNeighbours.push({x: x - 1, y: y});
            }
            heightMapNeighbours[y][x] = singlePointNeighbours;
        }
    }
    return heightMapNeighbours;
}

function part1(int[][] heightmap) returns int {
    final int minX = 0;
    final int minY = 0;
    final int maxX = heightmap[0].length() - 1;
    final int maxY = heightmap.length() - 1;

    // find neighbours
    final NeighbourCoordinates heightMapNeighbours = getNeighbourCoordinates(minX, maxX, minY, maxY);

    // check neighbours of all points in heightmap
    int numberOfLowPoints = 0;
    int sumOfLowPoints = 0;
    foreach var y in minY ... maxY {
        foreach var x in minX ... maxX {
            final Coordinates neighboursOfThisPoint = heightMapNeighbours[y][x];
            final int currentHeight = heightmap[y][x];
            int minNeighbourHeight = int:MAX_VALUE;
            {
                final int[] neighbourHeights = neighboursOfThisPoint.map(function (Coordinate neighbour) returns int {
                    return heightmap[neighbour.y][neighbour.x];
                });
                minNeighbourHeight = util:min(neighbourHeights);
            }
            if currentHeight < minNeighbourHeight {
                numberOfLowPoints += 1;
                sumOfLowPoints += currentHeight;
            }
        }
    }

    return numberOfLowPoints + sumOfLowPoints;
}

function part2(int[][] heightmap) returns int {
    final int minX = 0;
    final int minY = 0;
    final int maxX = heightmap[0].length() - 1;
    final int maxY = heightmap.length() - 1;

    // find neighbours
    final NeighbourCoordinates heightMapNeighbours = getNeighbourCoordinates(minX, maxX, minY, maxY);

    // find low points
    final Coordinates lowPoints = [];
    foreach var y in minY ... maxY {
        foreach var x in minX ... maxX {
            final Coordinates neighboursOfThisPoint = heightMapNeighbours[y][x];
            final int currentHeight = heightmap[y][x];
            int minNeighbourHeight = int:MAX_VALUE;
            {
                final int[] neighbourHeights = neighboursOfThisPoint.map(function (Coordinate neighbour) returns int {
                    return heightmap[neighbour.y][neighbour.x];
                });
                minNeighbourHeight = util:min(neighbourHeights);
            }
            if currentHeight < minNeighbourHeight {
                // io:println(string`[${x},${y}] ${currentHeight} is a low point`);
                lowPoints.push({ x: x, y: y });
            }
        }
    }
    // io:println(lowPoints);

    final int[] basinSizes = [];
    boolean[][] visited = createBoolean2dArray(maxX, maxY, false);
    // TODO: traverse basin starting from the low points
    foreach var point in lowPoints {
        // can't introduce recursive function here :(
        // var traverse = function(int total, Coordinate point) returns int { traverse() };
        // io:println(string`--- lowPoint: ${point.toString()}`);
        visited[point.y][point.x] = true;

        final Coordinates neighboursOfThisPoint = heightMapNeighbours[point.y][point.x];
        int currentBasinSize = 1;
        foreach var neighbour in neighboursOfThisPoint {
            int count = 0;
            if (visited[neighbour.y][neighbour.x] != true) && 
                (heightmap[neighbour.y][neighbour.x] > heightmap[point.y][point.x]) {
                visited[neighbour.y][neighbour.x] = true;
                if heightmap[neighbour.y][neighbour.x] < 9 {
                    count = traverse(heightmap, heightMapNeighbours, visited, 0, neighbour, point);
                }
            }
            
            // io:println(string`(count ${count})`);
            currentBasinSize += count;
        }
        // io:println(string`(currentBasinSize ${currentBasinSize})`);
        basinSizes.push(currentBasinSize);
    }

    {
        var sorted = basinSizes.sort(array:DESCENDING);
        return sorted[0] * sorted[1] * sorted[2];
    }
}

function traverse(int[][] heightmap, NeighbourCoordinates heightMapNeighbours, boolean[][] visited, int count, Coordinate point, Coordinate fromPoint) returns int {
    int localCount = count;
    localCount += 1; // visiting a new location !
    final Coordinates neighboursOfThisPoint = heightMapNeighbours[point.y][point.x];

    // remove fromPoint
    {
        int? index = neighboursOfThisPoint.indexOf(fromPoint);
        if index is int {
            _ = neighboursOfThisPoint.remove(index);
        }
    }

    foreach var neighbour in neighboursOfThisPoint {
        if (visited[neighbour.y][neighbour.x] != true) && 
           (heightmap[neighbour.y][neighbour.x] > heightmap[point.y][point.x]) {
            visited[neighbour.y][neighbour.x] = true;
            if heightmap[neighbour.y][neighbour.x] < 9 {
                localCount = traverse(heightmap, heightMapNeighbours, visited, localCount, neighbour, point);
            }
        }
    }

    return localCount;
}
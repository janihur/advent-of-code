import ballerina/io;

public function run(
    record {| boolean part1; boolean part2; |} parts
) returns record {| int? part1; int? part2; |} {
    // read puzzle data -------------------------------------------------------
    string[] stringData = checkpanic io:fileReadLines("../../inputs/20<YEAR>-<DAY>");
    int[] puzzleData = stringData.map(function (string line) returns int {
        return 0;
    });
    // run puzzle solvers -----------------------------------------------------
    final record {| int? part1; int? part2; |} answers = { part1: (), part2: () };
    if parts.part1 {
        answers.part1 = part1(puzzleData);
    }
    if parts.part2 {
        answers.part2 = part2(puzzleData);
    }
    return answers;
}

function part1(int[] x) returns int {
    return 0;
}

function part2(int[] x) returns int {
    return 0;
}
import ballerina/test;

final int[] PUZZLE_DATA = [0, 1, 2];

@test:Config {}
function test_part_1() {
    test:assertEquals(part1(PUZZLE_DATA), 0);
}

@test:Config {}
function test_part_2() {
    test:assertEquals(part2(PUZZLE_DATA), 0);
}

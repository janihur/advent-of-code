# Advent of Code - Ballerina

Note that Ballerina has been evolving a lot since the initial release and the old code won't compile with the new compiler versions:

* `aoc23` is developed with Ballerina [Swan Lake 2201.8](https://blog.ballerina.io/posts/2023-09-22-announcing-ballerina-2201.8.0-swan-lake-update-8/) (Java 17).
* `aoc22` compiles with Ballerina [Swan Lake 2201.3](https://blog.ballerina.io/posts/2022-12-05-announcing-ballerina-2201.3.0-swan-lake-update-3/) (Java 11). Originally developed with Swan Lake Update 2.
* `aoc21` compiles with Ballerina [Swan Lake 2201.0.0](https://blog.ballerina.io/posts/2022-02-01-announcing-ballerina-2201.0.0-swan-lake/) (Java 11). Originally developed with Swan Lake Beta 4.
* `aoc20` compiles with Ballerina Swan Lake Preview 5 (Java 11).
* `aoc19` compiles with Ballerina 1.2.6 (Java 8).

## aoc23

Developed with Ballerina [Swan Lake 2201.8](https://blog.ballerina.io/posts/2023-09-22-announcing-ballerina-2201.8.0-swan-lake-update-8/).

The code structure and build commands are identical to `aoc22`.

New build feature `bal format` command that enforces the canonical code formatting rules is used first time.

## aoc22

Originally developed with Swan Lake Update 2 but the code compiles without changes with [Swan Lake 2201.3](https://blog.ballerina.io/posts/2022-12-05-announcing-ballerina-2201.3.0-swan-lake-update-3/) too.

The code has been structured as one Ballerina package and modules:

* package `aoc22` default module contains the main program code
* each day is a Ballerina module named as `day<NN>`
* a few utilities are available in `util` module

Bootstrapping:
```
# package
bal new aoc22

# daily modules
./add-module 22 <DAY>
```

Build (without generating a jar) and run:
```
bal run --offline -- --day <DAY> --part <PART>
```

where:

* `--offline` is command line parameter for `bal` command (don't call to Ballerina Central).
* `--` separates `bal` command and program command line parameters.
* `--day <DAY>` is the mandatory command line parameter. Valid values are from 1 to 25.
* `--part <PART>` is optional. Valid values are 1 and 2. If not given both parts of the day are run.

Clean build (generates a jar):
```
bal clean && bal build --offline
```

The generated executable jar is located in `target/bin`.

Run the executable jar:
```
bal run target/bin/<JAR> --day <DAY> --part <PART>
```
or
```
java -jar target/bin/<JAR> --day <DAY> --part <PART>
```

Run tests:
```
# all tests
bal test --offline
# only tests from a <MODULE>
bal test --offline --tests <PACKAGE>.<MODULE>:*
```

## aoc21

Originally developed with Swan Lake Beta 4 and later modified to run with [Swan Lake 2201.0.0](https://blog.ballerina.io/posts/2022-02-01-announcing-ballerina-2201.0.0-swan-lake/). There was one bug still, see [ballerina-lang/issues/34949](https://github.com/ballerina-platform/ballerina-lang/issues/34949), but luckily there was simple workaround.

The code has been structured as Ballerina packages and modules:

* each year is a Ballerina package named as `aocNN`
* each day is a Ballerina module named as `dayNN`
* there might be also other modules providing common functionality

Build (without generating a jar) and run:
```
bal run --offline -- --day <DAY> --part <PART>
```

where:

* `--offline` is command line parameter for `bal` command (don't call to Ballerina Central).
* `--` separates `bal` command and program command line parameters.
* `--day <DAY>` is the mandatory command line parameter. Valid values are from 1 to 25.
* `--part <PART>` is optional. Valid values are 1 and 2. If not given both parts of the day are run.

Clean build (generates a jar):
```
bal clean && bal build --offline
```

The generated executable jar is located in `target/bin`.

Run the executable jar:
```
bal run target/bin/<JAR> --day <DAY> --part <PART>
```
or
```
java -jar target/bin/<JAR> --day <DAY> --part <PART>
```

Run tests:
```
# all tests
bal test --offline
# only tests from a <MODULE>
bal test --offline --tests <PACKAGE>.<MODULE>:*
```

## aoc19 and aoc20

The code has been structured as Ballerina projects and modules:

* each year is a Ballerina project named as `aocNN`
* each day is a Ballerina module named as `dayNN`
* there might be also other modules providing common functionality

All `dayNN` modules have `public function main` that is considered an entry point to a Ballerina program. So all `dayNN` modules will generate an executable Java JAR file to `aocNN/target/bin/dayNN.jar`.

Projects:

* `aoc19` compiles with Ballerina 1.2.6 (Java 8).
* `aoc20` compiles with Ballerina Swan Lake Preview 5 (Java 11).

Compile the project from scratch without running unit tests:

```
ballerina clean && ballerina build --all --skip-tests
```

Compile and run unit tests for a single module:

```
ballerina build dayNN
```

Run a single module:

```
# with ballerina tool:
ballerina run dayNN
# or run the fat jar:
java -jar target/bin/dayNN.jar
```

Note that input file locations are hardcoded as relative paths so you have to run the code in the project directory.

Compile the helper library (`util`):

```
ballerina build --compile --code-coverage util
```

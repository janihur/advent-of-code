import ballerina/io;
import ballerina/stringutils;
import ballerina/lang.'int;
import ballerina/test;

function isValid1(int password) returns boolean {
    final string s = password.toString();
    boolean isValid = false;
    foreach int i in 0 ... 4 {
        if (s[i] == s[i+1]) {
            isValid = true;
            break;
        }
    }
    if (isValid) {
        foreach int i in 0 ... 4 {
            int a = checkpanic 'int:fromString(s[i]);
            int b = checkpanic 'int:fromString(s[i+1]);
            if (a > b) {
                isValid = false;
                break;
            }
        }
    }
    return isValid;
}

function group_(string s) returns string[] {
    string[] arr = [];
    int i = 0;
    while (i < 6) {
        int j = i + 1;
        while (j < 6) {
            if (s[i] != s[j]) { break; }
            j += 1;
        }
        arr.push(s.substring(i, j));
        i = j;
    }
    return arr;
}

function isValid2(int password) returns boolean {
    final string s = password.toString();
    boolean isValid = false;
    string[] groups = group_(s);
    foreach var group_ in groups {
        if (group_.length() == 2) {
            isValid = true;
            break;
        }
    }
    if (isValid) {
        foreach int j in 0 ... 4 {
            int a = checkpanic 'int:fromString(s[j]);
            int b = checkpanic 'int:fromString(s[j+1]);
            if (a > b) {
                isValid = false;
                break;
            }
        }
    }
    return isValid;
}

function part1(int min, int max) returns int {
    int valids = 0;
    foreach int x in min ... max {
        if (isValid1(x)) {
            valids += 1;
        }
    }
    return valids;
}

function part2(int min, int max) returns int {
    int valids = 0;
    foreach int x in min ... max {
        if (isValid2(x)) {
            valids += 1;
        }
    }
    return valids;
}

public function main() {
    // puzzle data ------------------------------------------------------------
    io:ReadableByteChannel byteChannel = checkpanic io:openReadableFile("../../inputs/2019-04");
    io:ReadableCharacterChannel charChannel = new(byteChannel, "UTF-8");
    string puzzleStr = checkpanic charChannel.read(10000);
    string[] puzzleData = stringutils:split(puzzleStr.trim(), "-");
    int min = checkpanic 'int:fromString(puzzleData[0]);
    int max = checkpanic 'int:fromString(puzzleData[1]);

    // part 1 -----------------------------------------------------------------
    int a1 = part1(min, max);
    io:println("part 1 answer: ", a1);
    test:assertEquals(a1, 1919);

    // part 2 -----------------------------------------------------------------
    int a2 = part2(min, max);
    io:println("part 2 answer: ", a2);
    test:assertEquals(a2, 1291);
}
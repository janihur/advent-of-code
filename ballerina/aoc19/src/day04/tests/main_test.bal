import ballerina/io;
import ballerina/test;

@test:Config {}
function testGroup_() {
    io:println(group_("111111"));
    test:assertEquals(group_("111111"), <string[]>["111111"]);
    test:assertEquals(group_("122345"), <string[]>["1","22","3","4","5"]);
    test:assertEquals(group_("111122"), <string[]>["1111","22"]);
    test:assertEquals(group_("111123"), <string[]>["1111","23"]);
    test:assertEquals(group_("112233"), <string[]>["11","22","33"]);
    test:assertEquals(group_("111222"), <string[]>["111","222"]);
    test:assertEquals(group_("122223"), <string[]>["1","2222","3"]);
    test:assertEquals(group_("122334"), <string[]>["1","22","33","4"]);
    test:assertEquals(group_("123456"), <string[]>["1","2","3","4","5","6"]);
}

@test:Config {}
function testIsValid1() {
    test:assertEquals(isValid1(111111), true);
    test:assertEquals(isValid1(122345), true);
    test:assertEquals(isValid1(111122), true, "111122");
    test:assertEquals(isValid1(111123), true);
    
    test:assertEquals(isValid1(135679), false);
    test:assertEquals(isValid1(223450), false);
    test:assertEquals(isValid1(123789), false);
    test:assertEquals(isValid1(136818), false);
    test:assertEquals(isValid1(685979), false);

    test:assertEquals(isValid1(112233), true);
    test:assertEquals(isValid1(123444), true, "123444");
    test:assertEquals(isValid1(111122), true);
}

@test:Config {}
function testIsValid2() {
    test:assertEquals(isValid2(111111), false, "111111");
    test:assertEquals(isValid2(122345), true);
    test:assertEquals(isValid2(111122), true, "111122");
    test:assertEquals(isValid2(111123), false, "111123");
    
    test:assertEquals(isValid2(135679), false);
    test:assertEquals(isValid2(223450), false);
    test:assertEquals(isValid2(123789), false);
    test:assertEquals(isValid2(136818), false);
    test:assertEquals(isValid2(685979), false);

    test:assertEquals(isValid2(112233), true);
    test:assertEquals(isValid2(123444), false, "123444");
    test:assertEquals(isValid2(111122), true);
}
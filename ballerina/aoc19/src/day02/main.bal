import ballerina/io;
import ballerina/lang.'int;
import ballerina/stringutils;
import ballerina/test;

import jani/intcode19 as ic;

function copyProgram(int[] program, int noun = 0, int verb = 0) returns int[] {
    var copy = program.clone();
    copy[1] = noun;
    copy[2] = verb;
    return copy;
}

function run2(int[] inputProgram, int expectedOutput) returns [int, int] {
    foreach var noun in 0 ... 99 {
        foreach var verb in 0 ... 99 {
            var program = copyProgram(inputProgram, noun, verb);
            ic:IntCode19 computer = checkpanic new(program);
            computer.run();
            if (computer.memory[0] == expectedOutput) {
                return [noun, verb];
            }
        }
    }
    panic error("ANSWER NOT FOUND", msg="(expectedOutput = "+ expectedOutput.toString() + ")");
}

function part1(int[] program) returns int {
    ic:IntCode19 computer = checkpanic new(program);
    computer.run();
    return computer.memory[0];
}

function part2(int[] inputProgram, int expectedOutput) returns int {
    var [noun, verb] = run2(inputProgram, expectedOutput);
    return (100 * noun) + verb;
}

public function main() {
    // puzzle data ------------------------------------------------------------
    io:ReadableByteChannel byteChannel = checkpanic io:openReadableFile("../../inputs/2019-02");
    io:ReadableCharacterChannel charChannel = new(byteChannel, "UTF-8");
    var puzzleStr = checkpanic charChannel.read(1000);
    var puzzleStrA = stringutils:split(puzzleStr, ",");
    int[] puzzleData = [];
    foreach var item in puzzleStrA {
        puzzleData.push(checkpanic 'int:fromString(stringutils:replace(item, "\n", "")));
    }

    // part 1 -----------------------------------------------------------------
    var program = copyProgram(puzzleData, 12, 2);
    int a1 = part1(program = program);
    io:println("part 1 answer: ", a1);
    test:assertEquals(a1, 4570637);

    // part 2 -----------------------------------------------------------------
    int a2 = part2(inputProgram = puzzleData, expectedOutput = 19690720);
    io:println("part 2 answer: ", a2);
    test:assertEquals(a2, 5485);
}
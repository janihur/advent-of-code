// import ballerina/io;
import ballerina/test;

@test:Config {}
function testKeyConversion() {
    test:assertEquals(toKey(<Point>{x:1,y:2}), "{x:1, y:2}");
    test:assertEquals(toKey(<Point>{x:-123,y:2345}), "{x:-123, y:2345}");

    test:assertEquals(fromKey("{x:1, y:2}"), <Point>{x:1,y:2});
    test:assertEquals(fromKey("{x:-123, y:2345}"), <Point>{x:-123,y:2345});
}

@test:Config {}
function testManhattanDistance() {
    test:assertEquals(manhattanDistance(<Point>{x: 1, y: 1}, <Point>{x: 8, y: 3}), 9);
    test:assertEquals(manhattanDistance(<Point>{x: 8, y: 9}, <Point>{x: 8, y: 3}), 6);
    test:assertEquals(manhattanDistance(<Point>{x: 8, y: 3}, <Point>{x: 8, y: 3}), 0);
    test:assertEquals(manhattanDistance(<Point>{x: 0, y: 0}, <Point>{x: 3, y: 3}), 6);
    test:assertEquals(manhattanDistance(<Point>{x: 0, y: 0}, <Point>{x: -3, y: -3}), 6);
}

@test:Config {}
function testGetLine() {
    // map<Point> line1 = getLine("R8,U5,L5,D3");
    // Point p1 = line1.get("{x:3, y:2}");
    // test:assertEquals(p1, <Point>{x:3,y:2,steps:21});

    map<Point> line = getLine("R3,U1,L1,D2");
    test:assertEquals(line.get("{x:2, y:0}"), <Point>{x:2,y:0,steps:2});
    test:assertEquals(line.get("{x:2, y:-1}"), <Point>{x:2,y:-1,steps:7});
    // foreach var item in line2 {
    //     io:println(item);
    // }
}

@test:Config {}
function test1() {
    map<Point> line1 = getLine("R8,U5,L5,D3");
    // io:println("(line1 = ", line1, ")");

    map<Point> line2 = getLine("U7,R6,D4,L4");
    // io:println("(line2 = ", line2, ")");

    Point[] intersections = findIntersections(line1, line2);
    // io:println("(intersections = ", intersections, ")");
    test:assertEquals(intersections[0], <Point>{x:0,y:0,steps:0});
    test:assertEquals(intersections[1], <Point>{x:6,y:5,steps:30});
    test:assertEquals(intersections[2], <Point>{x:3,y:3,steps:40});

    var shortest = shortestDistance(intersections);
    // io:println("(shortest = ", shortest, ")");
    test:assertEquals(shortest, 6);
    test:assertEquals(fewestSteps(intersections), 30);
}

@test:Config {}
function test2() {
    map<Point> line1 = getLine("R75,D30,R83,U83,L12,D49,R71,U7,L72");
    map<Point> line2 = getLine("U62,R66,U55,R34,D71,R55,D58,R83");

    Point[] intersections = findIntersections(line1, line2);

    test:assertEquals(shortestDistance(intersections), 159);
    test:assertEquals(fewestSteps(intersections), 610);
}

@test:Config {}
function test3() {
    map<Point> line1 = getLine("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51");
    map<Point> line2 = getLine("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7");

    Point[] intersections = findIntersections(line1, line2);

    test:assertEquals(shortestDistance(intersections), 135);
    test:assertEquals(fewestSteps(intersections), 410);
}
import ballerina/io;
import ballerina/stringutils;
import ballerina/lang.'int;
import ballerina/lang.'string;

function toKey(Point point) returns string {
    return string `{x:${point.x}, y:${point.y}}`;
}

function fromKey(string key) returns Point {
    var parts = stringutils:split(key, ", ");
    int x = checkpanic 'int:fromString('string:substring(parts[0], 3));
    int y = checkpanic 'int:fromString('string:substring(parts[1], 2, parts[1].length() - 1));
    return <Point>{x: x, y: y};
}

type Point record {
    int x;
    int y;
    int steps?;
};

function getLineSegment(map<Point> canvas, [int, int] from_, [int, int] to, int steps) returns int {
    final var [fromX, fromY] = from_;
    final var [toX, toY] = to;

    int localSteps = steps;

    if (fromX < toX) {
        boolean isFirst = true;
        foreach int x in fromX ... toX {
            Point point = {x: x, y: fromY, steps: localSteps};
            // io:print("getLineSegment: (point = ", point, ")");
            var key = toKey(point);
            if (canvas.hasKey(key) && isFirst) {
                // io:println(" skipped because first");
            } else if (canvas.hasKey(key)) {
                // io:println(" skipped because already visited");
                localSteps += 1;
            } else {
                canvas[toKey(point)] = point;
                localSteps += 1;
                // io:println();
            }
        }
    }

    if (fromX > toX) {
        boolean isFirst = true;
        int x = fromX;
        while (x >= toX) {
            Point point = {x: x, y: fromY, steps: localSteps};
            // io:print("getLineSegment: (point = ", point, ")");
            var key = toKey(point);
            if (canvas.hasKey(key) && isFirst) {
                // io:println(" skipped because first");
            } else if (canvas.hasKey(key)) {
                // io:println(" skipped because already visited");
                localSteps += 1;
            } else {
                canvas[toKey(point)] = point;
                localSteps += 1;
                // io:println();
            }
            x -= 1;
        }
    }

    if (fromY < toY) {
        boolean isFirst = true;
        foreach int y in fromY ... toY {
            Point point = {x: fromX, y: y, steps: localSteps};
            // io:print("getLineSegment: (point = ", point, ")");
            var key = toKey(point);
            if (canvas.hasKey(key) && isFirst) {
                // io:println(" skipped because first");
            } else if (canvas.hasKey(key)) {
                // io:println(" skipped because already visited");
                localSteps += 1;
            } else {
                canvas[toKey(point)] = point;
                localSteps += 1;
                // io:println();
            }
        }
    }

    if (fromY > toY) {
        boolean isFirst = true;
        int y = fromY;
        while (y >= toY) {
            Point point = {x: fromX, y: y, steps: localSteps};
            // io:print("getLineSegment: (point = ", point, ")");
            var key = toKey(point);
            if (canvas.hasKey(key) && isFirst) {
                // io:println(" skipped because first");
                isFirst = false;
            } else if (canvas.hasKey(key)) {
                // io:println(" skipped because already visited");
                localSteps += 1;
            } else {
                canvas[toKey(point)] = point;
                localSteps += 1;
                // io:println();
            }
            y -= 1;
        }
    }

    // final var xMax = 'int:max(fromX, toX);
    // final var xMin = 'int:min(fromX, toX);

    // final var yMax = 'int:max(fromY, toY);
    // final var yMin = 'int:min(fromY, toY);

    // int localSteps = steps;

    // foreach int x in xMin ... xMax {
    //     foreach int y in yMin ... yMax {
    //         Point point = {x: x, y: y, steps: localSteps};
    //         io:print("getLineSegment: (point = ", point, ")");
    //         var key = toKey(point);
    //         if (canvas.hasKey(key)) {
    //             io:println(" skipped");
    //         } else {
    //             canvas[toKey(point)] = point;
    //             localSteps += 1;
    //             io:println();
    //         }
    //     }
    // }
    // foreach int x in fromX ... toX {
    //     foreach int y in fromY ... toY {
    //         Point point = {x: x, y: y, steps: localSteps};
    //         // final string key = string `{x:${x}, y:${y}}`;

    //         canvas[toKey(point)] = point;
    //         // io:println("(key = ", key, ")");
    //         localSteps += 1;
    //     }
    // }
    // foreach int x in toX ... fromX  {
    //     foreach int y in toY ... fromY {
    //         Point point = {x: x, y: y, steps: localSteps};
    //         // final string key = string `{x:${x}, y:${y}}`;
    //         canvas[toKey(point)] = point;
    //         // io:println("(key = ", key, ")");
    //         localSteps += 1;
    //     }
    // }
    return localSteps;
}

function getLine(string wirePath) returns map<Point> {
    final map<Point> line = {};

    int x = 0;
    int y = 0;
    int steps = 0;

    string[] parts = stringutils:split(wirePath, ",");

    foreach string part in parts {
        final string direction = 'string:substring(part, 0, 1);
        final int length = checkpanic 'int:fromString('string:substring(part, 1));
        // io:println("getLine: (direction = ", direction, ")(length = ", length,")");
        match direction {
            "R" => {steps = getLineSegment(line, [x, y], [x + length, y], steps); x += length;}
            "D" => {steps = getLineSegment(line, [x, y], [x, y - length], steps); y -= length;}
            "L" => {steps = getLineSegment(line, [x, y], [x - length, y], steps); x -= length;}
            "U" => {steps = getLineSegment(line, [x, y], [x, y + length], steps); y += length;}
            _ => {
                panic error("UNKNOWN DIRECTION", msg="(direction = " + direction + ")(length = " + length.toString() + ")");
            }
        }
        
    }
    return line;
}

function findIntersections(map<Point> line1, map<Point> line2) returns Point[] {
    Point[] intersections = [];
    foreach string key in line1.keys() {
        if (line2.hasKey(key)) {
            Point p1 = line1.get(key).clone();
            int steps = p1?.steps ?: 0;
            Point p2 = line2.get(key);
            io:println("findIntersections: (p1 = ", p1,")(p2 = ",p2,")");
            p1.steps = steps + (p2?.steps ?: 0);
            // io:println("intersection: ", key);
            // var parts = stringutils:split(key, ", ");
            // io:println("parts[1]: ", parts[1]);
            // int x = checkpanic 'int:fromString('string:substring(parts[0], 3));
            // io:println("y: ", 'string:substring(parts[1], 2, parts[1].length() - 1));
            // int y = checkpanic 'int:fromString('string:substring(parts[1], 2, parts[1].length() - 1));
            // if ((x != 0) && (y != 0)) {
            //     // skip origo
                // intersections.push(<Point>{x: x, y: y});
                intersections.push(p1);
            // } else {
            //     io:println("skipping: ", x, " ", y);
            // }
        }
    }
    return intersections;
}

function manhattanDistance(Point p1, Point p2) returns int {
    return 
        ('int:max(p1.x, p2.x) - 'int:min(p1.x, p2.x)) +
        ('int:max(p1.y, p2.y) - 'int:min(p1.y, p2.y))
    ;
}

function shortestDistance(Point[] intersections) returns int {
    int? shortest = ();
    foreach var point in intersections {
        // don't compare to origo
        if (point.x == 0 && point.y == 0) {
            // io:println("skip: ", point);
            continue;
        }
        var distance = manhattanDistance(<Point>{x: 0, y: 0}, point);
        // io:println("shortestDistance: (point = ", point, ")(distance = ", distance, ")");
        if (shortest is ()) {
            shortest = distance;
        } else if (shortest > distance) {
            shortest = distance;
        }
    }
    return <int>shortest;
}

function fewestSteps(Point[] intersections) returns int {
    int? fewest = ();
    foreach var point in intersections {
        // don't compare to origo
        if (point.x == 0 && point.y == 0) {
            io:println("fewestSteps: skip: ", point);
            continue;
        }
        var steps = point?.steps ?: 0;
        io:println("fewestSteps: (point = ", point, ")(steps = ", steps, ")");
        if (fewest is ()) {
            fewest = steps;
        } else if (fewest > steps) {
            fewest = steps;
        }
    }
    return <int>fewest;
}

function part1(string[] lines) returns int {
    map<Point> line1 = getLine(lines[0]);
    map<Point> line2 = getLine(lines[1]);
    Point[] intersections = findIntersections(line1, line2);
    return shortestDistance(intersections);
}

function part2(string[] lines) returns int {
    map<Point> line1 = getLine(lines[0]);
    map<Point> line2 = getLine(lines[1]);
    Point[] intersections = findIntersections(line1, line2);
    return fewestSteps(intersections);
}

public function main() {
    // puzzle data ------------------------------------------------------------
    io:ReadableByteChannel byteChannel = checkpanic io:openReadableFile("../../inputs/2019-03");
    io:ReadableCharacterChannel charChannel = new(byteChannel, "UTF-8");
    string puzzleStr = checkpanic charChannel.read(10000);
    string[] puzzleData = stringutils:split(puzzleStr, "\n");
    // io:println(puzzleData);

    // part 1 -----------------------------------------------------------------
    // int a1 = part1(puzzleData);
    // io:println("part 1 answer: ", a1);
    // test:assertEquals(a1, 860);

    // part 2 -----------------------------------------------------------------
    int a2 = part2(puzzleData);
    io:println("part 2 answer: ", a2);
    // test:assertEquals(a2, 5485);
}

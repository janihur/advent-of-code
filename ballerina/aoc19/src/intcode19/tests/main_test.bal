import ballerina/test;

@test:Config {}
function day02test1() {
    int[] program = [1,0,0,0,99];
    IntCode19 computer = checkpanic new(program);
    computer.run();
    test:assertEquals(computer.memory, <int[]>[2,0,0,0,99]);
}

@test:Config {}
function day02test2() {
    int[] program = [2,3,0,3,99];
    IntCode19 computer = checkpanic new(program);
    computer.run();
    test:assertEquals(computer.memory, <int[]>[2,3,0,6,99]);
}

@test:Config {}
function day02test3() {
    int[] program = [2,4,4,5,99,0];
    IntCode19 computer = checkpanic new(program);
    computer.run();
    test:assertEquals(computer.memory, <int[]>[2,4,4,5,99,9801]);
}

@test:Config {}
function day02test4() {
    int[] program = [1,1,1,4,99,5,6,0,99];
    IntCode19 computer = checkpanic new(program);
    computer.run();
    test:assertEquals(computer.memory, <int[]>[30,1,1,4,2,5,6,0,99]);
}

@test:Config {}
function day02test5() {
    int[] program = [1,9,10,3,2,3,11,0,99,30,40,50];
    IntCode19 computer = checkpanic new(program);
    computer.run();
    test:assertEquals(computer.memory, <int[]>[3500,9,10,70,2,3,11,0,99,30,40,50]);
}
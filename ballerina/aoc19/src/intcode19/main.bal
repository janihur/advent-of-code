const int ADD = 1;
const int MULTIPLY = 2;
const int HALT = 99;

type OPCODE ADD|MULTIPLY|HALT;

public type IntCode19 object {
    public int[] memory;
    int pc; // program counter

    public function __init(int[] program) returns error? {
        self.memory = program.clone();
        self.pc = 0;
    }

    public function run() {
        while (true) {
            match self.memory[self.pc] {
                ADD => {
                    int op1 = self.memory[self.memory[self.pc+1]];
                    int op2 = self.memory[self.memory[self.pc+2]];
                    int retPos = self.memory[self.pc+3];
                    self.memory[retPos] = op1 + op2;
                }
                MULTIPLY => {
                    int op1 = self.memory[self.memory[self.pc+1]];
                    int op2 = self.memory[self.memory[self.pc+2]];
                    int retPos = self.memory[self.pc+3];
                    self.memory[retPos] = op1 * op2;
                }
                HALT => {
                    break;
                }
            }
            self.pc += 4;
        }
    }
};
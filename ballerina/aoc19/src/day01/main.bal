import ballerina/io;
import ballerina/test;

type Mass record {
    int mass;
};

function fuelRequirement(int mass) returns int {
    if (mass < 9) {
        return 0;
    }
    return (mass / 3) - 2;
}

function part1(table<Mass> masses) returns int {
    int totalFuel = 0;
    foreach var item in masses {
        totalFuel += fuelRequirement(item.mass);
    }
    return totalFuel;
}

function part2(table<Mass> masses) returns int {
    int totalFuel = 0;
    foreach var item in masses {
        var requiredFuel = fuelRequirement(item.mass);
        while (requiredFuel > 0) {
            totalFuel += requiredFuel;
            requiredFuel = fuelRequirement(requiredFuel);
        }
    }
    return totalFuel;
}

public function main() {
    // puzzle data ------------------------------------------------------------
    var csv = checkpanic io:openReadableCsvFile("../../inputs/2019-01");
    var puzzleData = <table<Mass>> checkpanic csv.getTable(Mass);

    // part 1 -----------------------------------------------------------------
    int a1 = part1(puzzleData);
    io:println("part 1 answer: ", a1);
    test:assertEquals(a1, 3249140);

    // part 2 -----------------------------------------------------------------
    int a2 = part2(puzzleData);
    io:println("part 2 answer: ", a2);
    test:assertEquals(a2, 4870838);
}
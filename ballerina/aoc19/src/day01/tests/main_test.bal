import ballerina/test;

table<Mass> testData = table {
    {mass},
    [{12},{14},{1969},{100756}]
};

@test:Config {}
function test1() {
    int a = part1(testData);
    test:assertEquals(a, 34241);
}

@test:Config {}
function test2() {
    int a = part2(testData);
    test:assertEquals(a, 51316);
}
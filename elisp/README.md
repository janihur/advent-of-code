# Emacs Lisp

Run Emacs Lisp script that reads input from stdin from command line:

```
< INPUT_FILE emacs --no-init-file --no-site-file --script SCRIPT [ARGS...]
```

E.g.

```
< ../inputs/2018-01 emacs --no-init-file --no-site-file --script 2018-01.el
```

Byte compile:

```
emacs --no-init-file --no-site-file --batch --funcall batch-byte-compile *.el
```

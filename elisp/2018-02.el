;;-*- mode: emacs-lisp;-*-

;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Sequences-Arrays-Vectors.html
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Sequence-Functions.html

(eval-when-compile
  (require 'cl-lib)
  (require 'subr-x) ; hash-table-values string-join
  )


(defun any(list pred)
  "Returns t if PRED is true for any element in the LIST. Otherwise returns nil."
  (catch 'found
    (dolist (elem list)
      (if (funcall pred elem)
          (throw 'found t)))))

(defun id-match(a b)
  "Compares ids (strings) A and B and returns all common characters (as a string) if the ids differ only by one character. Otherwise returns nil."
  (let ((a-chars (mapcar 'char-to-string a))
        (b-chars (mapcar 'char-to-string b))
        (differences 0)
        (common-chars ()))
    (while (and a-chars b-chars)
      ;; (princ (format "(%s,%s -> %s)" (car a-chars) (car b-chars) (string-equal (car a-chars) (car b-chars))))
      (if (string-equal (car a-chars) (car b-chars))
          (push (car a-chars) common-chars)
        (setq differences (1+ differences)))
      (setq a-chars (cdr a-chars))
      (setq b-chars (cdr b-chars)))
    (if (= differences 1)
        (string-join (nreverse common-chars))
      nil)))
  
(let (id-list)
  (let (line)
    (while (setq line (ignore-errors (read-from-minibuffer "")))
      (push line id-list)))
  ;;
  ;; part 1
  ;;
  (let (checksum
        (twos 0)
        (threes 0))
    (dolist (id id-list)
      (let ((groups (make-hash-table :test 'equal)))
        (dolist (char (mapcar 'char-to-string id))
          (puthash char (1+ (or (gethash char groups) 0)) groups))
        ;; (princ (format "%s\n" groups))
        ;; (princ (format "%s\n" (hash-table-values groups)))
        ;; (princ (format "%s\n" (any (hash-table-values groups) (lambda (a) (= a 2)))))
        (let ((values (hash-table-values groups)))
          (setq twos   (+ twos   (if (any values (lambda (a) (= a 2))) 1 0)))
          (setq threes (+ threes (if (any values (lambda (a) (= a 3))) 1 0))))))
    (setq checksum (* twos threes))
    (princ (format "part 1: answer = %d\n" checksum))
    (cl-assert (= checksum 7134) t))
  ;;
  ;; part 2
  ;;
  (let (id-list2
        (common-chars (catch 'found
                        (while id-list
                          (setq id-list2 (copy-sequence (cdr id-list)))
                          (while id-list2
                            ;;(princ (format "%s - %s -> " (car id-list) (car id-list2)))
                            (let ((id-differences (id-match (car id-list) (car id-list2))))
                              (if id-differences
                                  (throw 'found id-differences)))
                            (setq id-list2 (cdr id-list2)))
                          (setq id-list (cdr id-list))))))
    (princ (format "part 2: answer = %s\n" common-chars))
    (cl-assert (string-equal common-chars "kbqwtcvzhmhpoelrnaxydifyb") t))
  )

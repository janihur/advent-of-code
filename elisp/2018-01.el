;;-*- mode: emacs-lisp;-*-

;; apply
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Calling-Functions.html
;; formatting strings
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Formatting-Strings.html
;; hash tables
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Hash-Tables.html
;; non-local exits
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Nonlocal-Exits.html
;; iteration
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Iteration.html

(eval-when-compile (require 'cl-lib))

(let (frequency-changes)
  (let (line)
    (while (setq line (ignore-errors (read-from-minibuffer "")))
      (push (string-to-number line) frequency-changes))
    )
  (setq frequency-changes (nreverse frequency-changes))
  ;;
  ;; part 1
  ;;
  (let ((resulting-frequency (apply '+ frequency-changes)))
    (princ (format "part 1: answer = %d\n" resulting-frequency))
    (cl-assert (= resulting-frequency 466) t)
    )
  ;;
  ;; part 2
  ;;
  (let ((resulting-frequency 0)
        (already-seen-resulting-frequencies (make-hash-table :test 'eq)))
    (catch 'break-out
      (while t
        (dolist (frequency-change frequency-changes)
          (setq resulting-frequency (+ resulting-frequency frequency-change))
          (if (not (gethash resulting-frequency already-seen-resulting-frequencies))
              (puthash resulting-frequency t already-seen-resulting-frequencies)
            (throw 'break-out nil)))))
    (princ (format "part 2: answer = %d\n" resulting-frequency))
    (cl-assert (= resulting-frequency 750) t)
    )
  )

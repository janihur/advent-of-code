module main

import arrays { shuffle }
import os { args, input, read_lines }
import rand { int_in_range }
import term
import time { sleep_ms }

fn main() {
	// convert csv to []MasterTicket
	filename := args[args.len - 1]
	tickets_total, master_tickets := get_master_tickets(filename)
	// convert []MasterTicket to shuffled []RaffleTicket
	raffle_tickets := get_raffle_tickets(master_tickets)
	// calculate winner
	winning_index := int_in_range(0, tickets_total)
	// visualization
	term_width, _ := term.get_terminal_size()
	print_input_data(master_tickets, term_width)
	input('press enter to show the raffle tickets')
	term.cursor_up(1)
	term.erase_line_toend()
	print_raffle_tickets(raffle_tickets, term_width)
	input('press enter to spin the wheel of fortune')
	term.cursor_up(1)
	term.erase_line_toend()
	print_raffle(raffle_tickets, winning_index, term_width)
	sleep_ms(2000)
	print_winner(raffle_tickets[winning_index].owner, term_width)
}

struct RaffleTicket {
	owner string [required]
}

struct MasterTicket {
	owner             string [required]
	number_of_tickets int    [required]
	winning_change    f32    [required]
}

fn print_winner(winner string, term_width int) {
	print_header('WINNER', term_width)
	println(term.bold(term.yellow(winner)))
	println('')
}

fn print_raffle(raffle_tickets []RaffleTicket, winning_index int, term_width int) {
	tickets_total := raffle_tickets.len
	delays := get_delay_sequence((2 * tickets_total) + winning_index + 1)
	mut delay_index := int(0)
	out: for {
		term.cursor_up(tickets_total)
		for i, raffle_ticket in raffle_tickets {
			delay_ms := delays[delay_index]
			if i > 0 {
				term.cursor_up(1)
				term.erase_line_toend()
				println('${i - 1}: ${raffle_tickets[i - 1].owner}')
			}
			term.erase_line_toend()
			println(term.bold(term.green('$i: $raffle_ticket.owner <<<')))
			sleep_ms(delay_ms)
			if i == tickets_total - 1 {
				term.cursor_up(1)
				term.erase_line_toend()
				println('$i: $raffle_ticket.owner')
			}
			delay_index++
			if delay_index > delays.len - 1 {
				term.cursor_up(1)
				term.erase_line_toend()
				println(term.bold(term.green('$i: $raffle_ticket.owner <<< WINNER')))
				break out
			}
		}
	}
	term.cursor_down(tickets_total - winning_index - 1)
	println('')
}

fn print_raffle_tickets(raffle_tickets []RaffleTicket, term_width int) {
	print_header('RAFFLE TICKETS', term_width)
	for i, raffle_ticket in raffle_tickets {
		println('$i: $raffle_ticket.owner')
	}
}

fn print_input_data(master_tickets []MasterTicket, term_width int) {
	print_header('INPUT DATA', term_width)
	for master_ticket in master_tickets {
		println('$master_ticket.owner: $master_ticket.number_of_tickets (${master_ticket.winning_change *
			100:.2f}%)')
	}
	println('')
}

fn get_raffle_tickets(master_tickets []MasterTicket) []RaffleTicket {
	mut raffle_tickets := []RaffleTicket{}
	for master_ticket in master_tickets {
		for _ in 0 .. master_ticket.number_of_tickets {
			raffle_tickets << RaffleTicket{master_ticket.owner}
		}
	}
	shuffle(mut raffle_tickets, 0)
	return raffle_tickets
}

fn get_master_tickets(filename string) (int, []MasterTicket) {
	tickets_csv := read_lines(filename) or { panic('error reading: $filename') }
	mut tickets_total := int(0)
	for line in tickets_csv {
		data := line.split(',')
		tickets := data[0].int()
		tickets_total += tickets
	}
	mut master_tickets := []MasterTicket{}
	for line in tickets_csv {
		data := line.split(',')
		tickets := data[0].int()
		master_tickets << MasterTicket{data[1], tickets, f32(tickets) / tickets_total}
	}
	return tickets_total, master_tickets
}

fn get_delay_sequence(len int) []int {
	step := 700 / len
	mut seq := []int{}
	seq << int(0)
	for i in 1 .. len {
		seq << seq[i - 1] + step
	}
	return seq
}

fn print_header(header string, term_width int) {
	println(term.bold(header))
	print_separator(term_width)
}

fn print_separator(term_width int) {
	for _ in 0 .. term_width {
		print(term.bold('-'))
	}
	println('')
}

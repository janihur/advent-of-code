Class aoc.y18.d05 Extends %RegisteredObject
{

ClassMethod Solve()
{
    do ..Part1()
    do ..Part2()
}

ClassMethod Part1()
{
    set data = ..getData()
    write "(answer (part1 ",..reactPolymer(data),"))",!
}

ClassMethod Part2()
{
    set data = ..getData()
    set minLength = ..reactPolymer(..removeUnit(data,"a")) // case a
    for i = $ascii("b"):1:$ascii("z") { // cases b-z
        set length = ..reactPolymer(..removeUnit(data,$char(i)))
        if (length < minLength) {
            set minLength = length
        }
    }
    write "(answer (part2 ",minLength,"))",!
}

ClassMethod reactPolymer(data As %String) As %Integer [ Private ]
{
    set i = 1
    for {
        set c2 = $extract(data,i+1) quit:(c2 = "")
        set c1 = $extract(data,i)
        if (..isOppositePolarity(c1,c2)) {
            set data = $extract(data,1,i-1) _ $extract(data,i+2,$length(data))
            set i = i - 1
        } else {
            set i = i + 1
        }
    }
    return $length(data)
}

ClassMethod removeUnit(data As %String, unit1 As %String) As %String [ Private ]
{
    set unit2 = $char($ascii(unit1) - 32)
    set i = 1
    for {
        set c = $extract(data,i) quit:(c = "")
        if ((c = unit1) || (c = unit2)) {
            set data = $extract(data,1,i-1) _ $extract(data,i+1,$length(data))
        } else {
            set i = i + 1
        }
    }
    return data
}

ClassMethod isOppositePolarity(c1 As %String, c2 As %String) As %Boolean [ Private ]
{
    return $zabs($ascii(c1) - $ascii(c2)) = 32
}

ClassMethod getData() As %String [ Private ]
{
    // have to split into two parts as the input is too long for a single string literal
    return $get(^data(0)) _ $get(^data(1))
}

}

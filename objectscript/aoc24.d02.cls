Class aoc24.d02
{

ClassMethod Solve()
{
	do ..readInput(.reportArr,.size)

	set numberOfSafeReports1 = 0
	set numberOfSafeReports2 = 0
	for i=1:1:size {
		#; write "report ",i,": ----------------------",!
		#; zwrite reportArr(i)
		do $increment(numberOfSafeReports1, ..isSafeReport2(.reportArr,i))
		do $increment(numberOfSafeReports2, ..isSafeReport2(.reportArr,i,1))
	}

	// 1) 670 2) 700
	write "(answers (part1 ",numberOfSafeReports1,")(part2 ",numberOfSafeReports2,"))",!
}

ClassMethod isSafeReport2(
	ByRef reportArr,
	reportNumber As %Integer,
	enableProblemDampener As %Boolean = 0) As %Boolean
{
	merge levelArr = reportArr(reportNumber)
	kill levelArr("size")

	set isSafe = ..validate(.levelArr)

	return:(isSafe) 1 // always safe
	return:('enableProblemDampener) isSafe // result when no dampening

	// run problem dampener
	set levelsSize = reportArr(reportNumber,"size")
	for i=1:1:levelsSize {
		kill copyLevelArr
		merge copyLevelArr = levelArr
		kill copyLevelArr(i)
		return:(..validate(.copyLevelArr)) 1 // safe after removing one level
	}
	return 0 // no safe report found
}

ClassMethod validate(ByRef levelArr) As %Boolean
{
	set i = $order(levelArr("")) // 1st level
	set j = $order(levelArr(i))  // 2nd level

	return:(levelArr(i) = levelArr(j)) 0 // no increase/decrease -> unsafe
	
	set isDecreaseExpected = levelArr(i) > levelArr(j)

	while (j '= "") {
		set difference = $zabs(levelArr(i) - levelArr(j))
		return:((difference < 1)||(difference > 3)) 0 // difference not in limits -> unsafe

		set isDecreaseActual = levelArr(i) > levelArr(j)
		return:(isDecreaseExpected '= isDecreaseActual) 0 // not expected direction -> unsafe

		set i = j
		set j = $order(levelArr(j))
	}

	return 1 // all checks passed -> safe
}

/// <p>The first implementation for part1. Left here for historical reference as
/// for part2 one had to rewrite it for a bit more general solution.</p>
ClassMethod isSafeReport(
	ByRef reportArr,
	reportNumber As %Integer) As %Boolean
{
	set firstLevel = reportArr(reportNumber,1)
	set secondLevel = reportArr(reportNumber,2)

	return:(firstLevel = secondLevel) 0 // no difference -> unsafe
	
	set isExpectedDecreasing = firstLevel > secondLevel
	set levelsSize = reportArr(reportNumber,"size")

	for j=1:1:levelsSize {
		return:(j = levelsSize) 1 // all compared - we're safe
		set difference = reportArr(reportNumber,j) - reportArr(reportNumber,j+1)
		set absDifference = $zabs(difference)
		return:((absDifference < 1)||(absDifference > 3)) 0 // difference not in limits -> unsafe

		set isActualDecreasing = reportArr(reportNumber,j) > reportArr(reportNumber,j+1)
		return:(isExpectedDecreasing '= isActualDecreasing) 0 // not expected direction -> unsafe
	}
	return 1
}

ClassMethod readInput(
	Output reportArr,
	Output size As %Integer)
{
    set data = ##class(aoc.data202402).%New()
    set i = 1
    while (data.GetNext(.value)) {
        do ..splitInputLine(value,.levels)
		#; write value," -> "  zwrite levels  write !
		merge reportArr(i) = levels
		do $increment(i)
	}

	set size = data.Size()
}

ClassMethod splitInputLine(
	line As %String,
	Output levels)
{
	kill levels
	for i=1:1:$length(line," ") {
    	set levels(i) = $piece(line," ",i)
	}
	set levels("size") = i
}

}

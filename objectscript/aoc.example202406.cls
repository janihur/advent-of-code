Class aoc.example202406 Extends aoc.data.base
{

Method %OnNew() As %Status
{
    do ##super()
    set ..data(0) = "....#....."
    set ..data(1) = ".........#"
    set ..data(2) = ".........."
    set ..data(3) = "..#......."
    set ..data(4) = ".......#.."
    set ..data(5) = ".........."
    set ..data(6) = ".#..^....."
    set ..data(7) = "........#."
    set ..data(8) = "#........."
    set ..data(9) = "......#..."
    set ..size = 10
    return $$$OK
}

}

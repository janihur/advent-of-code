Class aoc.data.base Extends %RegisteredObject
{

Property data As %String [ Final, MultiDimensional, Private ];

Property index As %Integer [ Final, InitialExpression = 0, Private ];

Property size As %Integer [ Final, InitialExpression = 0, Private ];

Method %OnNew(index As %Integer = 0) As %Status
{
    set ..index = index
    return $$$OK
}

Method GetNext(Output value As %String) As %Boolean
{
    #dim hasData as %Boolean = $data(..data(..index))#2
    if ('hasData) {
        set ..index = 0
        return 0
    }
    set value = $get(..data(..index))
	set ..index = ..index + 1
    return 1
}

Method Reset()
{
	set ..index = 0
}

Method Size() As %Integer
{
    return ..size
}

}

Class aoc24.d04 Extends %RegisteredObject
{

ClassMethod Solve()
{
	do ..readInput(.data, .size)

	// part 1 -----------------------------------------------------------------

	set wordCount = 0

	// 1) horizontal
	for i=1:1:size {
		do $increment(wordCount, ..countXmas(data(i)))
	}

	// 2) vertical
	do ..transposeToVertical(.data, .vertical, .size)
	for i=1:1:size {
		do $increment(wordCount, ..countXmas(vertical(i)))
	}

	// 3) diagonal right
	do ..transposeToDiagonalRight(.data, .diagonalRight, size, .sizeOut)
	for i=1:1:sizeOut {
		do $increment(wordCount, ..countXmas(diagonalRight(i)))
	}

	// 3) diagonal left
	do ..transposeToDiagonalLeft(.data, .diagonalLeft, size, .sizeOut)
	for i=1:1:sizeOut {
		do $increment(wordCount, ..countXmas(diagonalLeft(i)))
	}

	// part 2 -----------------------------------------------------------------

	set masCount = 0

	do ..transposeTo2dArray(.data, .data2d, size)

	// collect all coordinates of 'A'

	set row = $order(data2d(""))
	while (row '= "") {
		set col = $order(data2d(row,""))
		while (col '= "") {
			if (data2d(row,col) = "A") {
				set coordsOfA(row,col) = data2d(row,col)
			}
			set col = $order(data2d(row,col))
		}
		set row = $order(data2d(row))
	}

	// check every coordinate of 'A'

	set row = $order(coordsOfA(""))
	while (row '= "") {
		set col = $order(coordsOfA(row,""))
		while (col '= "") {
			do $increment(masCount, ..isMas(.data2d, size, row, col))
			set col = $order(coordsOfA(row,col))
		}
		set row = $order(coordsOfA(row))
	}

	// ------------------------------------------------------------------------

	// 1) 2483 2) 1925
	write "(answers (part1 ",wordCount,")(part2 ",masCount,"))",!
}

ClassMethod transposeTo2dArray(
	ByRef dataIn,
	Output dataOut,
	size As %Integer)
{
	for row=1:1:size {
		for col=1:1:$length(dataIn(row)) {
			set dataOut(row,col) = $extract(dataIn(row),col)
		}
	}
}

ClassMethod isMas(
	ByRef data,
	size As %Integer,
	row As %Integer,
	col As %Integer)
{
	return:(row < 2) 0
	return:(col < 2) 0
	return:(row > (size-1)) 0
	return:(col > (size-1)) 0

	set letters = data(row-1,col-1)_data(row-1,col+1)_data(row+1,col-1)_data(row+1,col+1)

	return:(letters = "MMSS") 1
	return:(letters = "SMSM") 1
	return:(letters = "SSMM") 1
	return:(letters = "MSMS") 1

	return 0
}

ClassMethod transposeToDiagonalLeft(
	ByRef dataIn,
	Output dataOut,
	sizeIn As %Integer,
	Output sizeOut As %Integer)
{
	do ..transposeTo2dArray(.dataIn, .temp, sizeIn)

	set i = 1

	for startCol=sizeIn:-1:1 {
		set row = 1
		for col=startCol:-1:1 {
			set dataOut(i) = $get(dataOut(i))_temp(row,col)
			do $increment(row)
		}
		do $increment(i)
	}

	for startRow=2:1:sizeIn {
		set col = sizeIn
		for row=startRow:1:sizeIn {
			set dataOut(i) = $get(dataOut(i))_temp(row,col)
			set col = col - 1
		}
		do $increment(i)
	}

	set sizeOut = i - 1
}

ClassMethod transposeToDiagonalRight(
	ByRef dataIn,
	Output dataOut,
	sizeIn As %Integer,
	Output sizeOut As %Integer)
{
	do ..transposeTo2dArray(.dataIn, .temp, sizeIn)

	set i = 1

	for startCol=1:1:sizeIn {
		set row = 1
		for col=startCol:1:sizeIn {
			set dataOut(i) = $get(dataOut(i))_temp(row,col)
			do $increment(row)
		}
		do $increment(i)
	}

	for startRow=2:1:sizeIn {
		set col = 1
		for row=startRow:1:sizeIn {
			set dataOut(i) = $get(dataOut(i))_temp(row,col)
			do $increment(col)
		}
		do $increment(i)
	}

	set sizeOut = i - 1
}

ClassMethod transposeToVertical(
	ByRef dataIn,
	Output dataOut,
	size As %Integer)
{
	for i=1:1:size {
		for j=1:1:$length(dataIn(i)) {
			set dataOut(j) = $get(dataOut(j))_$extract(dataIn(i),j)
		}
	}
}

ClassMethod countXmas(line As %String) As %Integer
{
	set count = 0

	set matcher = ##class(%Regex.Matcher).%New("XMAS|SAMX", line)
	set location = 1
	while (matcher.Locate(location)) {
		set x = matcher.StartGet()
		quit:(x = -2)
		do $increment(count)
		set location = x + 1
	}

	return count
}

ClassMethod readInput(
	Output data,
	Output size As %Integer)
{
	set data2 = ##class(aoc.data202404).%New()
	set i = 1
	while (data2.GetNext(.value)) {
		set data(i) = value
		do $increment(i)
	}

	set size = data2.Size()
}

}

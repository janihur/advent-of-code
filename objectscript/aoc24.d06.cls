Class aoc24.d06
{

ClassMethod Solve(useExampleData As %Boolean = 0)
{
	do ..readInput(.map,.size,.guardStartPos,useExampleData)

	set visited(guardStartPos.y,guardStartPos.x) = 1

	// part 1 -----------------------------------------------------------------

	set guard = ##class(aoc24.Guard).%New(guardStartPos.x,guardStartPos.y)

	set stop = 0
	for {
		#dim step as aoc.Coord = guard.StepForward(.map,.visited)

		set:(step.x < 1) stop = 1
		set:(step.x > size) stop = 1
		set:(step.y < 1) stop = 1
		set:(step.y > size) stop = 1
		
		if (stop) {
			kill visited(step.y,step.x)
			quit
		}
	}

	// 5312
	write "(answers (part1 ",..getSize(.visited),")"

	// part 2 -----------------------------------------------------------------

	set obstructionCount = 0

	merge candidates = visited
	// remove the starting position
	kill candidates(guardStartPos.y,guardStartPos.x)

	set y = ""
	for {
		set y = $order(candidates(y))
		quit:(y = "")
		set x = ""
		for {
			set x = $order(candidates(y,x))
			quit:(x = "")
			
			kill map2  merge map2 = map  set map2(y,x) = "#"
			
			set guard = ##class(aoc24.Guard).%New(guardStartPos.x,guardStartPos.y)

			for {
				#dim step2 as aoc.Coord = guard.StepForward(.map2,,.inLoop)

				if (inLoop) {
					set obstructionCount = obstructionCount + 1
					quit
				}

				quit:(step2.x < 1)
				quit:(step2.x > size)
				quit:(step2.y < 1)
				quit:(step2.y > size)
			}
		}
	}
	
	// 1748
	write "(part2 ",obstructionCount,"))",!
}

ClassMethod getSize(ByRef md) As %Integer [ PublicList = md ]
{
	set size = 0

	set ref = $query(md(""))
	while (ref '= "") {
		set size = size + 1
		set ref = $query(@ref)
	}

	return size
}

ClassMethod readInput(
	Output data,
	Output size As %Integer,
	Output guardPos As aoc.Coord,
	useExampleData As %Boolean = 0)
{
	if (useExampleData) {
		set data = ##class(aoc.example202406).%New()
	} else {
		set data = ##class(aoc.input202406).%New()
	}

	set guardPos = $$$NULLOREF

	set y = 1
	while (data.GetNext(.value)) {
		quit:(value = "")
		kill temp  do ##class(aoc.str).Split(value,.temp,"")
		if (guardPos = $$$NULLOREF) {
			set x = ""
			for {
				set x = $order(temp(x))
				quit:(x = "")
				if (temp(x) = "^") {
					set guardPos = ##class(aoc.Coord).%New(x,y)
					quit
				}
			}
		}
		merge data(y) = temp
		do $increment(y)
	}

	set size = data.Size()
}

}

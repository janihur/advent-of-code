/// <p>A simple <a href="https://en.wikipedia.org/wiki/List_(abstract_data_type)">list</a> with better ergonomics.</p>
Class aoc.list Extends %RegisteredObject
{

Property array [ MultiDimensional, Private ];

Property appendIndex As %Integer [ InitialExpression = 1, Private ];

Property size As %Integer [ InitialExpression = 0, Private ];

Property iterIndex As %Integer [ InitialExpression = 1, Private ];

Method %OnNew(
	input,
	inputIsArray = 0) As %Status
{
	if (inputIsArray) {
		set i = ""
		for {
			set i = $order(input(i))
			quit:(i = "")
			do ..Append(input(i))
		}
	}
	return $$$OK
}

ClassMethod CreateFromArray(ByRef array) As aoc.list
{
	return ##class(aoc.list).%New(.array, 1)
}

Method Append(value)
{
	set ..array(..appendIndex) = value
	set ..appendIndex = ..appendIndex + 1
	set ..size = ..size + 1
}

Method GetByIndex(index As %Integer) As %String
{
	return ..array(index)
}

Method GetNext(Output value) As %Boolean
{
	kill value
	if (..iterIndex > ..size) {
		do ..ResetIterator()
		return 0
	}
	set value = ..array(..iterIndex)
	set ..iterIndex = ..iterIndex + 1
	return 1
}

Method GetSize() As %Integer
{
	return ..size
}

Method ResetIterator()
{
	set ..iterIndex = 1
}

}

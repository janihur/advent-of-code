# Advent of Code - InterSystems ObjectScript

## 2024
The solutions expect a data from object that implements `aoc.data.base` "iterator" interface. The class can be created from the input data:

```
python3 data-import-v2.py PUZZLE_INPUT_FILE
```

This will generate data classes:
* `aoc.example<YEAR><DAY>.cls` - will be create only if `PUZZLE_INPUT_FILE-example` exists
* `aoc.input<YEAR><DAY>.cls`

The solution classes have the interface with parameter defining if it should load the example or the actual puzzle data:
```
ClassMethod Solve(useExampleData As %Boolean = 0)
```

Some solutions uses companion classes, so be sure to load them too:
* `aoc.Coord`
* `aoc.list`
* `aoc.stack`
* `aoc.StopWatch`
* `aoc.str`
* `aoc.topo`
* `aoc24.Guard`

## 2018
Solutions expect input data from array `^data` global. The `^data` global is populated with `aoc.data` class. The class is not in version control but needs to be constructed for each puzzle separately with `data-import.py`:

```
python3 data-import.py PUZZLE_INPUT_FILE > aoc.data.cls
```

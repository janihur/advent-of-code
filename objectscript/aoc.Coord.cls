Class aoc.Coord Extends %RegisteredObject
{

Property x As %Integer [ InitialExpression = 1 ];

Property y As %Integer [ InitialExpression = 1 ];

Method %OnNew(
	x As %Integer = 1,
	y As %Integer = 1) As %Status
{
	set ..x = x
	set ..y = y
	return $$$OK
}

Method Add(coord As aoc.Coord)
{
	set ..x = ..x + coord.x
	set ..y = ..y + coord.y
}

Method ToStr() As %String
{
	return "("_..x_","_..y_")"
}

}

Class aoc24.d03
{

ClassMethod Solve()
{
	do ..readInput(.program,.size)

	// part 1
	set mulResults1 = 0
	set i = $order(program("")) // 1st index
	while (i '= "") {
		set j = $order(program(i,"")) // 2nd index
		while (j '= "") {
			do $increment(mulResults1, ..executeInstruction(program(i,j)))
			set j = $order(program(i,j))
		}
		set i = $order(program(i))
	}

	// part 2
	do ..reduceProgram(.program)
	zw program

	set mulResults2 = 0
	set i = $order(program("")) // 1st index
	while (i '= "") {
		set j = $order(program(i,"")) // 2nd index
		while (j '= "") {
			do $increment(mulResults2, ..executeInstruction(program(i,j)))
			set j = $order(program(i,j))
		}
		set i = $order(program(i))
	}

	// 1) 155955228 2) 100189366
	write "(answers (part1 ",mulResults1,")(part2 ",mulResults2,"))",!
}

ClassMethod reduceProgram(ByRef program)
{
	set skipInstruction = 0
	set i = $order(program("")) // 1st index
	while (i '= "") {
		set j = $order(program(i,"")) // 2nd index
		while (j '= "") {
			// this will leave the 'do()' instructions in place but it doesn't
			// matter as only the 'mul()' instructions are executed later
			set instruction = program(i,j)
			set:(instruction = "do()") skipInstruction = 0
			set:(instruction = "don't()") skipInstruction = 1

			if (skipInstruction) {
				set k = j
				set j = $order(program(i,j))
				kill program(i,k)
			} else {
				set j = $order(program(i,j))
			}
		}
		set i = $order(program(i))
	}
}

ClassMethod executeInstruction(instruction As %String) As %Integer
{
	set matcher = ##class(%Regex.Matcher).%New("mul\((\d+),(\d+)\)",instruction)
	if (matcher.Locate()) {
		set a = matcher.Group(1)
		set b = matcher.Group(2)
		return (a*b)
	}
	return 0
}

ClassMethod splitInputLine(
	line As %String,
	Output program)
{
	kill program
	for i=1:1:$length(line,";") {
		set program(i) = $piece(line,";",i)
	}
}

ClassMethod readInput(
	Output program,
	Output size As %Integer)
{
	set data = ##class(aoc.data202403).%New()
	set i = 1
	while (data.GetNext(.value)) {
		set washedLine = ..washLine(value)
		do ..splitInputLine(washedLine,.lineInstructions)
		merge program(i) = lineInstructions
		do $increment(i)
	}
	set size = data.Size()
}

ClassMethod washLine(line As %String) As %String
{
	set matcher = ##class(%Regex.Matcher).%New("mul\(\d{1,3},\d{1,3}\)|don't\(\)|do\(\)",line)
	set washed = ""
	while (matcher.Locate()) {
		set washed = washed_matcher.Group(0)_";"
	}
	set washed = $extract(washed,1,*-1) // remove the final ;
	return washed
}

}

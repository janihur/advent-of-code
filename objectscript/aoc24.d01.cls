Class aoc24.d01
{

ClassMethod Solve()
{
    do ..readInput(.idArr1,.idArr2,.size)

    // part 1
    set totalDistance = 0
    for i=1:1:size {
        set distance = $zabs(idArr1(i) - idArr2(i))
        set totalDistance = totalDistance + distance
    }

    // part 2
    set similarityScore = 0
    for i=1:1:size {
        set number = idArr1(i)
        set appearances = 0
        for j=1:1:size {
            if (idArr2(j) = number) {
                do $increment(appearances)
            }
        }
        set similarityScore = similarityScore + (number * appearances)
    }

    write "(answers (part1 ",totalDistance,")(part2 ",similarityScore,"))",!
}

ClassMethod readInput(
	Output idArr1,
	Output idArr2,
	Output size As %Integer)
{
    set data = ##class(aoc.data202401).%New()
    set i = 1
    while (data.GetNext(.value)) {
        do ..splitInputLine(value,.id1,.id2)
        set idArr1(i) = id1
        set idArr2(i) = id2
        do $increment(i)
        #; write value," -> ", id1, " ", id2,!
    }

    set size = data.Size()
    do ..sortArray(.idArr1,size)
    do ..sortArray(.idArr2,size)
}

/// <p>A simple bubble sort.</p>
ClassMethod sortArray(
	ByRef array,
	size As %Integer)
{
    for i=1:1:size {
        for j=1:1:size-i {
            if (array(j) > array(j+1)) {
                set temp = array(j)
                set array(j) = array(j+1)
                set array(j+1) = temp
            }
        }
    }
}

ClassMethod splitInputLine(
	line As %String,
	Output id1 As %Integer,
	Output id2 As %Integer)
{
    set matcher = ##class(%Regex.Matcher).%New("(\d+)\s+(\d+)",line)
    do matcher.Locate()
    set id1 = matcher.Group(1)
    set id2 = matcher.Group(2)
}

}

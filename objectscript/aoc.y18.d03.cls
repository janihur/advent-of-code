Class aoc.y18.d03 Extends %RegisteredObject
{

ClassMethod Part1()
{
    set overlaps = 0

    // part 1
    set i = $order(^data(""))
    while (i '= "") {
        do ..split($get(^data(i)),.id,.x,.y,.w,.h)
        for j = x:1:(x+w-1) { // width
            for k = y:1:(y+h-1) { // height
                set fabric(j,k) = $get(fabric(j,k)) + 1
                if (fabric(j,k) > 1) { // this is an overlap
                    if (fabric(j,k) = 2) { // only count once when the first overlap is found
                        set overlaps = overlaps + 1
                    }
                    set idOverlaps(id) = 1
                }
            }
        }
        set i = $order(^data(i))
    }

    // part 2
    for id = 1:1:1341 {
        if ('$data(idOverlaps(id))) { // candidate
            do ..split($get(^data(id-1)),,.x,.y,.w,.h)
            quit:('..overlaps(.fabric,x,y,w,h))
        }

    }

    write "(answers (part1 ",overlaps,")(part2 ",id,"))",!
}

ClassMethod Part2()
{
    do ..Part1()
}

ClassMethod overlaps(fabric, x, y, w, h) As %Boolean
{
    for j = x:1:(x+w-1) { // width
        for k = y:1:(y+h-1) { // height
            if (fabric(j,k) > 1) { // this is an overlap
                return 1
            }
        }
    }
    return 0
}

ClassMethod split(claim, id, x, y, w, h)
{
    set matcher = ##class(%Regex.Matcher).%New("#(\d+) @ (\d+),(\d+): (\d+)x(\d+)",claim)
    do matcher.Locate()
    set id = matcher.Group(1)
    set x = matcher.Group(2)
    set y = matcher.Group(3)
    set w = matcher.Group(4)
    set h = matcher.Group(5)
}

}

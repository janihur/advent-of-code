Class aoc.StopWatch Extends %RegisteredObject
{

Property startTime As %TimeStamp [ InitialExpression = {$now()}, Private ];

Property endTime As %TimeStamp [ Private ];

Method %OnNew() As %Status
{
	return $$$OK
}

Method Stop()
{
	set ..endTime = $now()
}

Method GetTime() As %Decimal
{
	set start = $piece(..startTime,",",2)
	set end = $piece(..endTime,",",2)

	return end - start
}

}

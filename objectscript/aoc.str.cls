/// <p>Convenient <a href="https://en.wikipedia.org/wiki/String_(computer_science)">string</a> manipulation functions.</p>
Class aoc.str Extends %RegisteredObject
{

ClassMethod Split(
	in As %String,
	Output out,
	separator As %String = ",")
{
	kill out

	if (separator = "") {
		for i=1:1:$length(in) {
			set out(i) = $extract(in,i)
		}
		return
	}

	for i=1:1:$length(in,separator) {
		set out(i) = $piece(in,separator,i)
	}
}

}

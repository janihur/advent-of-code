Class aoc.y18.d04 Extends %RegisteredObject
{

ClassMethod Solve()
{
    // sort
    set i = $order(^data(""))
    while (i '= "") {
        set data($get(^data(i))) = 1
        set i = $order(^data(i))
    }

    // transform
    set i = $order(data(""))
    while (i '= "") {
        if (i["begins shift") {
            set id = ..getId(i)
        } elseif (i["falls asleep") {
            set begin = ..getMinute(i)
        } else { // wakes up
            set end = ..getMinute(i)
            set asleeps(id) = $get(asleeps(id)) + (end - begin)
            for j = begin:1:(end-1) { set asleeps(id,j) = $get(asleeps(id,j)) + 1 }
        }
        set i = $order(data(i))
    }

    // part 1
    set maxSleepTime = 0
    set i = $order(asleeps(""))
    while (i '= "") {
        if ($get(asleeps(i)) > maxSleepTime) {
            set maxSleepTime = $get(asleeps(i))
            set sleepiestId = i
        }
        set i = $order(asleeps(i))
    }

    set maxSleepTime = 0
    set i = $order(asleeps(sleepiestId,""))
    while (i '= "") {
        if ($get(asleeps(sleepiestId,i)) > maxSleepTime) {
            set maxSleepTime = $get(asleeps(sleepiestId,i))
            set sleepiestMinute = i
        }
        set i = $order(asleeps(sleepiestId,i))
    }

    set part1 = sleepiestId * sleepiestMinute

    // part 2
    set sleepiestId = ""
    set maxSleepCount = 0
    set i = $order(asleeps(""))
    while (i '= "") {
        set j = $order(asleeps(i,""))
        while (j '= "") {
            if ($get(asleeps(i,j)) > maxSleepCount) {
                set maxSleepCount = $get(asleeps(i,j))
                set sleepiestId = i
                set sleepiestMinute = j
            }
            set j = $order(asleeps(i,j))
        }
        set i = $order(asleeps(i))
    }

    set part2 = sleepiestId * sleepiestMinute
    
    write "(answers (part1 ",part1,")(part2 ",part2,"))",!
}

ClassMethod getId(record) As %Integer [ Private ]
{
    set matcher = ##class(%Regex.Matcher).%New("Guard #(\d+) begins shift",record)
    do matcher.Locate()
    return matcher.Group(1)
}

ClassMethod getMinute(record) As %Integer [ Private ]
{
    set matcher = ##class(%Regex.Matcher).%New(":(\d+)]",record)
    do matcher.Locate()
    return matcher.Group(1)
}

}

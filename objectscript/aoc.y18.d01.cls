Class aoc.y18.d01 Extends %RegisteredObject
{

ClassMethod Part1()
{
    set result = 0

    set i = $order(^data(""))
    while (i '= "") {
        set change = $get(^data(i))
        set result = result + change
        set i = $order(^data(i))
    }

    write "(answer ",result,")",!
}

ClassMethod Part2()
{
    set result = 0
    set stop = 0

    while ('stop) {
        set i = $order(^data(""))
        while (('stop) && (i '= "")) {
            set change = $get(^data(i))
            set result = result + change
            if ($data(alreadySeen(result))) {
                set stop = 1
            } else {
                set alreadySeen(result) = 1
                set i = $order(^data(i))
            }
        }
    }

    write "(answer ",result,")",!
}

}

Class aoc24.Guard Extends %RegisteredObject
{

Property pos As aoc.Coord;

/// UP RIGHT DOWN LEFT
Property facing As %String;

/// for loop tracking (in day6 part2)
Property visitedTurnPoints [ MultiDimensional ];

Method %OnNew(
	x As %Integer = 1,
	y As %Integer = 1,
	facing As %String = "UP") As %Status
{
	set ..pos = ##class(aoc.Coord).%New(x,y)
	set ..facing = facing
	return $$$OK
}

Method StepForward(
	ByRef map,
	ByRef visited,
	Output inLoop As %Boolean) As aoc.Coord
{
	set inLoop = 0
	set nextPos = ..pos.%ConstructClone(1)

	if (..facing = "UP") {
		do nextPos.Add(##class(aoc.Coord).%New(0,-1))
	} elseif (..facing = "RIGHT") {
		do nextPos.Add(##class(aoc.Coord).%New(1,0))
	} elseif (..facing = "DOWN") {
		do nextPos.Add(##class(aoc.Coord).%New(0,1))
	} elseif (..facing = "LEFT") {
		do nextPos.Add(##class(aoc.Coord).%New(-1,0))
	}

	if (..isObstruct(.map, nextPos)) {
		do $increment(..visitedTurnPoints(..facing,..pos.y,..pos.x))
		if (..visitedTurnPoints(..facing,..pos.y,..pos.x) > 1) {
			set inLoop = 1
			return ..pos.%ConstructClone(1)
		}
		do ..TurnRight()
		do ..StepForward(.map, .visited)
	} else {
		set ..pos = nextPos
		set visited(..pos.y,..pos.x) = 1
	}

	return ..pos.%ConstructClone(1)
}

Method TurnRight()
{
	if (..facing = "UP") {
		set ..facing = "RIGHT"
	} elseif (..facing = "RIGHT") {
		set ..facing = "DOWN"
	} elseif (..facing = "DOWN") {
		set ..facing = "LEFT"
	} elseif (..facing = "LEFT") {
		set ..facing = "UP"
	}
}

Method isObstruct(
	ByRef map,
	pos As aoc.Coord) As %Boolean
{
	if ($get(map(pos.y,pos.x)) = "#") {
		return 1
	}
	return 0
}

}

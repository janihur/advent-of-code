/// <p>Simple <a href="https://en.wikipedia.org/wiki/Topological_sorting">topological sorting</a>.</p>
Class aoc.topo Extends %RegisteredObject
{

Property rules As %String [ MultiDimensional, Private ];

/// <p><var>rules</var> is 2d array where index1 and index2 are vertices (nodes) and value is the weight of the edge. The edge direction is from index1 to index2.</p>
/// <example>
/// rules(47,13)=1
/// rules(47,29)=1
/// rules(47,53)=1
/// rules(47,61)=1
/// </example>
Method %OnNew(ByRef rules) As %Status
{
	merge ..rules = rules

	return $$$OK
}

/// <p><var>data</var> is 1d array where index has no special meaning and value is the vertex (node).</p>
/// <example>
/// data(1)=97
/// data(2)=75
/// data(3)=47
/// </example
Method Sort(ByRef data) As aoc.list
{
	// calculate edges
	set curr = ""
	for {
		set curr = $order(data(curr))
		quit:(curr = "")
		set next = ""
		for {
			set next = $order(data(next))
			quit:(next = "")
			continue:(data(curr) = data(next))
			do $increment(edges(data(next)), $get(..rules(data(curr),data(next)),0))
		}
	}

	// sort by number of edges
	set curr = ""
	for {
		set curr = $order(edges(curr))
		quit:(curr = "")
		set sorted1(edges(curr), curr) = ""
	}

	// convert to list
	set sorted2 = ##class(aoc.list).%New()

	set i = ""
	for {
		set i = $order(sorted1(i))
		quit:(i = "")
		set j = ""
		for {
			set j = $order(sorted1(i,j))
			quit:(j = "")
			do sorted2.Append(j)
		}
	}

	return sorted2
}

}

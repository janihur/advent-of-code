import sys

from pathlib import Path

def generate_data_class(input_file:Path, output_file:Path, classname:str):
    i = 0
    with open(input_file, 'r') as input:
        with open(output_file, 'w') as output:
            output.write(f'Class {classname} Extends aoc.data.base {{\n')
            output.write("Method %OnNew() As %Status {\n")
            output.write('    do ##super()\n')
            for line in input:
                output.write(f'    set ..data({i}) = "{line.strip()}"\n')
                i += 1
            output.write("    set ..size = " + str(i) + "\n")
            output.write("    return $$$OK\n")
            output.write("}\n")
            output.write("}\n")

# puzzle input data class -----------------------------------------------------

input_path = Path(sys.argv[1])

input_file = input_path.name
year = input_file.split('-')[0]
day = input_file.split('-')[1]
classname = f'aoc.input{year}{day}'
output_path = Path(f'{classname}.cls')

generate_data_class(input_path, output_path, classname)

# puzzle example data class ---------------------------------------------------

input_path = Path(sys.argv[1]+'-example')
if input_path.is_file():
    classname = f'aoc.example{year}{day}'
    output_path = Path(f'{classname}.cls')
    generate_data_class(input_path, output_path, classname)

# puzzle solver class ---------------------------------------------------------

classname = f'aoc{year[-2:]}.d{day}'
class_path = Path(f'{classname}.cls')

with open(class_path, 'w') as output:
    output.write(f'Class {classname} {{\n')
    output.write('ClassMethod Solve(useExampleData as %Boolean = 0) {}\n')
    output.write('ClassMethod readInput(useExampleData as %Boolean = 0) {\n')
    output.write('\tif (useExampleData) {\n')
    output.write(f'\t\tset data = ##class(aoc.example{year}{day}).%New()\n')
    output.write('\t} else {\n')
    output.write(f'\t\tset data = ##class(aoc.input{year}{day}).%New()\n')
    output.write('\t}\n')
    output.write('}\n')
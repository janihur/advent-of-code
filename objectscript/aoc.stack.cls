Class aoc.stack Extends %RegisteredObject
{

Property array [ MultiDimensional, Private ];

Property topIndex As %Integer [ InitialExpression = 1, Private ];

Method %OnNew(arg...) As %Status
{
	for i=1:1:$get(arg,0) {
		do ..Push(arg(i))
	}
	return $$$OK
}

Method Push(value)
{
	set ..array(..topIndex) = value
	set ..topIndex = ..topIndex + 1
}

Method Pop(Output value) As %Boolean
{
	if (..topIndex < 2) {
		return 0
	}
	set ..topIndex = ..topIndex - 1
	set value = ..array(..topIndex)
	kill ..array(..topIndex)
	return 1
}

}

Class aoc.y18.d02 Extends %RegisteredObject
{

ClassMethod Part1()
{
    set (twos, threes) = 0

    set i = $order(^data(""))
    while (i '= "") {
        set id = $get(^data(i))
        for j = 1:1:$length(id) {
            set letter = $extract(id,j)
            set count(letter) = $get(count(letter)) + 1
        }
        do ..hasValues(.count, .hasTwo, .hasTree)
        kill count
        set twos = twos + hasTwo
        set threes = threes + hasTree
        set i = $order(^data(i))
    }

    write "(answer ",twos*threes,")",!
}

ClassMethod hasValues(count, hasTwo, hasTree)
{
    set hasTwo = 0
    set hasTree = 0
    set i = $order(count(""))
    while (i '= "") {
        if (count(i) = 2) {
            set hasTwo = 1
        }
        if (count(i) = 3) {
            set hasTree = 1
        }
        set i = $order(count(i))
    }
}

ClassMethod Part2()
{
    for i = 0:1:249 {
        set id1 = $get(^data(i))
        for j = i+1:1:249 {
            set id2 = $get(^data(j))
            set diff = 0
            set common = ""
            for k = 1:1:$length(id1) {
                if ($extract(id1,k) '= $extract(id2,k)) {
                    set diff = diff + 1
                    quit:(diff > 1)
                } else {
                    set common = common_$extract(id1,k)
                }
            }
            if (diff = 1) {
                write "(answer ",common,")",!
                return 
            }
        }
    }
}

}

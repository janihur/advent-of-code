Class aoc24.d05 Extends %RegisteredObject
{

ClassMethod Solve()
{
	do ..readInput(.orderingRules, .pageNumbers)
	#; zwrite orderingRules
	#; zwrite pageNumbers

	set topo = ##class(aoc.topo).%New(.orderingRules)

	set pageNumberSum1 = 0
	set pageNumberSum2 = 0

	set i = $order(pageNumbers("")) // updates
	while (i '= "") {
		set isRuleViolation = 0
		set pageSize = pageNumbers(i,"size")
		for j=2:1:pageSize { // 1st page number can be skipped
			set currentPageNumber = pageNumbers(i,j)
			#; write "--- update: ",i," currentPageNumber: ",currentPageNumber,!
			// rules for current page number
			kill rules merge rules = orderingRules(currentPageNumber)
			// page numbers (k) before the current page number (j-1)
			for k=1:1:j-1 {
				set previousPageNumber = pageNumbers(i,k)
				if ($data(rules(previousPageNumber))) {
					kill temp merge temp = pageNumbers(i)
					if (..isPageNumberOnPageBeforeIndex(.temp, previousPageNumber,j-1)) {
						set isRuleViolation = 1
						goto endUpdatesLoop
					}
				}
			}
		}
endUpdatesLoop
		if ('isRuleViolation) { // part 1
			set middleIndex = $system.SQL.Functions.CEILING(pageSize/2)
			do $increment(pageNumberSum1, pageNumbers(i,middleIndex))
		} else { // part 2
			merge temp = pageNumbers(i)  kill temp("size")
			set list = topo.Sort(.temp)
			set middleIndex = $system.SQL.Functions.CEILING(list.GetSize()/2)
			do $increment(pageNumberSum2, list.GetByIndex(middleIndex))
		}
		set i = $order(pageNumbers(i))
	}

	// 1) 5588 2) 5331
	write "(answers (part1 ",pageNumberSum1,")(part2 ",pageNumberSum2,"))",!
}

ClassMethod isPageNumberOnPageBeforeIndex(
	ByRef pageNumbers,
	pageNumber As %Integer,
	index As %Integer) As %Boolean
{
	for i=1:1:index {
		if (pageNumbers(i) = pageNumber) {
			return 1
		}
	}
	return 0
}

ClassMethod splitStr(
	string As %String,
	Output array,
	separator As %String = ",")
{
	kill array
	for i=1:1:$length(string,separator) {
		set array(i) = $piece(string,separator,i)
	}
	set array("size") = i
}

ClassMethod splitOrderingRule(
	rule As %String,
	Output before As %String,
	Output after As %String)
{
	set before = $piece(rule, "|", 1)
	set after = $piece(rule, "|", 2)
}

ClassMethod readInput(
	Output orderingRules,
	Output pages)
{
	set data = ##class(aoc.data202405).%New()
	
	set i = 1
	while (data.GetNext(.value)) {
		quit:(value = "")
		do ..splitOrderingRule(value, .before, .after)
		set orderingRules(before, after) = 1
		do $increment(i)
	}

	set i = 1
	while (data.GetNext(.value)) {
		do ..splitStr(value,.temp)
		merge pages(i) = temp
		do $increment(i)
	}
}

}

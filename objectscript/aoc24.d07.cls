Class aoc24.d07
{

ClassMethod Solve(useExampleData As %Boolean = 0)
{
	do ..readInput(.calibrations,.size,useExampleData)
	
	// part 1 -----------------------------------------------------------------

	set testValueSum = 0

	set timer = ##class(aoc.StopWatch).%New()

	for i=1:1:size {
		set expectedResult = calibrations(i, "result")
		set values = calibrations(i, "values")

		// first two values ---------------------------------------------------

		do values.GetNext(.firstValue)
		do values.GetNext(.secondValue)

		kill stacks

		set stack = ##class(aoc.stack).%New(firstValue, secondValue, "*")
		do stack.Push(..operate(stack))

		set j = 1
		set stacks(j) = stack

		set stack = ##class(aoc.stack).%New(firstValue, secondValue, "+")
		do stack.Push(..operate(stack))

		set j = j + 1
		set stacks(j) = stack

		// all other values ---------------------------------------------------

		set nextStackIndex = j + 1

		while (values.GetNext(.value)) {
			do ..operateStacks(.stacks, .nextStackIndex, expectedResult, value, 1)
		}

		// --------------------------------------------------------------------

		set testValueSum = testValueSum + ..collectResult(.stacks, expectedResult)
	}

	do timer.Stop()

	// 5540634308362
	write "(part1 (answer ",testValueSum,")(time ",$fnumber(timer.GetTime(),"",3),"))",!

	// part 2 -----------------------------------------------------------------

	set testValueSum = 0
	
	set timer = ##class(aoc.StopWatch).%New()

	for i=1:1:size {
		set expectedResult = calibrations(i, "result")
		set values = calibrations(i, "values")

		// first two values ---------------------------------------------------

		do values.GetNext(.firstValue)
		do values.GetNext(.secondValue)

		kill stacks

		set stack = ##class(aoc.stack).%New(firstValue, secondValue, "*")
		do stack.Push(..operate(stack))

		set j = 1
		set stacks(j) = stack

		set stack = ##class(aoc.stack).%New(firstValue, secondValue, "+")
		do stack.Push(..operate(stack))

		set j = j + 1
		set stacks(j) = stack

		set stack = ##class(aoc.stack).%New(firstValue, secondValue, "||")
		do stack.Push(..operate(stack))

		set j = j + 1
		set stacks(j) = stack

		// all other values ---------------------------------------------------
		
		set nextStackIndex = j + 1

		while (values.GetNext(.value)) {
			do ..operateStacks(.stacks, .nextStackIndex, expectedResult, value, 2)
		}

		// --------------------------------------------------------------------
		
		set testValueSum = testValueSum + ..collectResult(.stacks, expectedResult)
	}

	do timer.Stop()

	// 472290821152397
	write "(part2 (answer ",testValueSum,")(time ",$fnumber(timer.GetTime(),"",3),"))",!
}

ClassMethod collectResult(
	ByRef stacks,
	expectedResult As %Integer) As %Integer [ Private ]
{
	set i = ""
	for {
		set i = $order(stacks(i))
		quit:(i = "")
		set stack = stacks(i)
		do stack.Pop(.value)
		if (value = expectedResult) {
			return value
		}
	}
	return 0
}

ClassMethod operateStacks(
	ByRef stacks,
	ByRef nextStackIndex As %Integer,
	expectedResult As %Integer,
	value As %Integer,
	part As %Integer) [ Private ]
{
	#; kill newStacks
	set i = ""
	for {
		set i = $order(stacks(i))
		quit:(i = "")

		// create new stacks --------------------------------------------------
		set stackMul = stacks(i)
		set stackAdd = stackMul.%ConstructClone(1)
		if (part = 2) {
			set stackCon = stackMul.%ConstructClone(1)
		}

		// populate stacks ----------------------------------------------------
		do stackMul.Push(value)
		do stackMul.Push("*")
		do stackAdd.Push(value)
		do stackAdd.Push("+")
		if (part = 2) {
			do stackCon.Push(value)
			do stackCon.Push("||")
		}

		// operate ------------------------------------------------------------
		set result = ..operate(stackMul)
		if (result > expectedResult) {
			kill stacks(i)
		} else {
			do stackMul.Push(result)
		}

		set result = ..operate(stackAdd)
		if (result > expectedResult) {
			// nothing to do 
		} else {
			do stackAdd.Push(result)
			set newStacks(nextStackIndex) = stackAdd
			set nextStackIndex = nextStackIndex + 1
		}

		if (part = 2) {
			set result = ..operate(stackCon)
			if (result > expectedResult) {
				// nothing to do 
			} else {
				do stackCon.Push(result)
				set newStacks(nextStackIndex) = stackCon
				set nextStackIndex = nextStackIndex + 1
			}
		}
	}
	merge stacks = newStacks
}

ClassMethod dumpStacks(ByRef stacks) [ Private ]
{
	set i = ""
	for {
		set i = $order(stacks(i))
		quit:(i = "")
		set stack = stacks(i)
		write "stack index: ",i,!
		zw stack
	}
}

ClassMethod operate(stack As aoc.stack) As %Integer [ Private ]
{
	do stack.Pop(.operation)
	do stack.Pop(.operand1)
	do stack.Pop(.operand2)

	return:(operation = "*") operand1 * operand2
	return:(operation = "+") operand1 + operand2
	return:(operation = "||") operand2 _ operand1
	return $$$NULLOREF
}

ClassMethod readInput(
	Output calibrations,
	Output size As %Integer,
	useExampleData As %Boolean = 0) [ Private ]
{
	if (useExampleData) {
		set data = ##class(aoc.example202407).%New()
	} else {
		set data = ##class(aoc.input202407).%New()
	}

	set size = data.Size()

	set i = 1
	while (data.GetNext(.value)) {
		kill temp  do ##class(aoc.str).Split(value,.temp,":")
		set calibrations(i, "result") = temp(1)

		kill temp2  do ##class(aoc.str).Split($zstrip(temp(2),"<W"),.temp2," ")
		set calibrations(i, "values") = ##class(aoc.list).CreateFromArray(.temp2)

		set i = i + 1
	}
}

}

import sys

i = 0
with open(sys.argv[1], 'r') as file:
    print("Class aoc.data Extends %RegisteredObject {")
    print("ClassMethod Import() {")
    print('    kill ^data')
    for line in file:
        print(f'    set ^data({i}) = "{line.strip()}"')
        i += 1
    print("}")
    print("}")
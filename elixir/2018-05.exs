assert = fn expected, actual, message ->
  if expected != actual do
    raise("#{message}: expected: #{expected} actual: #{actual}")
  end
end

# input data ------------------------------------------------------------------
polymer =
  File.stream!("../inputs/2018-05")
|> Enum.map(&String.trim/1)
|> Enum.at(0)
|> String.to_charlist()
# |> IO.inspect(label: "polymer")

# part 1 version #1 -----------------------------------------------------------
# accumulator has initial value

reacts? = fn u1, u2 ->
  abs(u1 - u2) == 32
end

length =
  polymer
|> List.foldr([0], fn curr, [prev|tail] = acc ->
  # IO.puts("----")
  # IO.inspect(acc,  label: " acc")
  # IO.inspect(curr, label: "curr")
  # IO.inspect(prev, label: "prev")
  # IO.inspect(tail, label: "tail")
  if reacts?.(curr, prev) do
    tail
  else
    [curr|acc]
  end
end)
# |> IO.inspect(label: "polymer")
|> Kernel.length()

length = length - 1

assert.(11118, length, "part 1 version #1")
IO.puts("part 1 version #1: " <> Integer.to_string(length))

# part 1 version #2 -----------------------------------------------------------
# accumulator is empty

defmodule Impl do
  def reacts?(u1, u2) do
    abs(u1 - u2) == 32
  end

  def react(curr, [prev|tail] = acc) do
    # IO.puts("----")
    # IO.inspect(acc,  label: " acc")
    # IO.inspect(curr, label: "curr")
    # IO.inspect(prev, label: "prev")
    # IO.inspect(tail, label: "tail")
    if reacts?(curr, prev) do
      tail
    else
      [curr|acc]
    end
  end

  def react(curr, [] = _acc) do
    # IO.puts("----")
    # IO.inspect(acc,  label: " acc")
    # IO.inspect(curr, label: "curr")
    [curr]
  end
end

length =
  polymer
# |> List.foldr([], fn curr, acc -> Impl.react(curr, acc) end)
|> List.foldr([], &Impl.react(&1, &2))
# |> IO.inspect(label: "polymer")
|> Kernel.length()

assert.(11118, length, "part 1 version #2")
IO.puts("part 1 version #2: " <> Integer.to_string(length))

# part 2 ----------------------------------------------------------------------

shortest_length =
  ?a..?z # units to be removed
|> Enum.map(fn removed_unit ->
  Enum.filter(polymer, fn unit ->
    (unit != removed_unit) && (unit != removed_unit - 32)
  end)
end)
# |> IO.inspect(label: "list of polymers without a single unit type (i.e. a/A, b/B)")
|> Enum.map(fn polymer ->
  List.foldr(polymer, [], &Impl.react(&1, &2)) |> Kernel.length()
end)
# |> IO.inspect(label: "list of lengths of fully reacted polymers")
|> Enum.min()

assert.(6948, shortest_length, "part 2")
IO.puts("part 2: " <> Integer.to_string(shortest_length))

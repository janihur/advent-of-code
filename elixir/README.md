# Advent of Code - Elixir

Elixir 1.14.1 on top of Erlang/OTP 24.2.1 (that is available OOTB in Lubuntu 22.04 LTS).

Run:
```
elixir <FILE>.exs
```

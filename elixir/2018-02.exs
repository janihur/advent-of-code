# input data ------------------------------------------------------------------
box_ids =
  File.read!("../inputs/2018-02")
|> String.split()
# |> IO.inspect()

# part 1 ----------------------------------------------------------------------
{twos, threes} =
  box_ids
|> Enum.reduce({0, 0}, fn id, {twos, threes} ->
  values = id |> String.graphemes() |> Enum.frequencies() |> Map.values()
  new_twos   = twos   + if Enum.any?(values, &(&1 == 2)), do: 1, else: 0
  new_threes = threes + if Enum.any?(values, &(&1 == 3)), do: 1, else: 0
  {new_twos, new_threes}
end)

checksum = twos * threes
IO.puts("part 1: " <> Integer.to_string(checksum))

# part 2 ----------------------------------------------------------------------
pairs = for id1 <- box_ids, id2 <- box_ids, id1 != id2, do: {id1, id2}

matching_ids =
  pairs
|> Enum.find(fn {id1, id2} ->
  diff_count =
      Enum.zip(String.graphemes(id1), String.graphemes(id2))
    |> Enum.count(fn {a, b} -> a != b end)
  diff_count == 1
end)

{id1, id2} = matching_ids

common_letters =
  Enum.zip(String.graphemes(id1), String.graphemes(id2))
|> Enum.reduce("", fn {a, b}, acc ->
  if a == b, do: acc <> a, else: acc
end)

IO.puts("part 2: " <> common_letters)

# input data ------------------------------------------------------------------
sequence_of_frequency_changes =
  File.read!("../inputs/2018-01")
|> String.split()
|> Enum.map(&String.to_integer/1)
# |> IO.inspect()

# part 1 ----------------------------------------------------------------------
resulting_frequency =
  sequence_of_frequency_changes
|> Enum.sum()

IO.puts("part 1: " <> Integer.to_string(resulting_frequency))

# part 2 ----------------------------------------------------------------------
first_double_frequency =
  sequence_of_frequency_changes
|> Stream.cycle()
|> Enum.reduce_while({0, MapSet.new()}, fn change, {current, already_seen} ->
  new = current + change
  if MapSet.member?(already_seen, new) do
    {:halt, new}
  else
    already_seen = MapSet.put(already_seen, new)
    {:cont, {new, already_seen}}
  end
end)

IO.puts("part 2: " <> Integer.to_string(first_double_frequency))

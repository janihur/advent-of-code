assert = fn expected, actual, message ->
  if expected != actual do
    raise("#{message}: expected: #{expected} actual: #{actual}")
  end
end

key_by_max_value = fn map ->
  {key, _} = Enum.max_by(map, fn {_k, v} -> v end)
  key
end

# input data ------------------------------------------------------------------
guards_data =
  File.stream!("../inputs/2018-04")
|> Enum.map(&String.trim/1)
|> Enum.sort()
|> Enum.map(fn line ->
  {
    NaiveDateTime.from_iso8601!(String.slice(line, 1, 16) <> ":00"),
    String.slice(line, 19..-1)
  }
end)
#|> IO.inspect(label: "guards data")

# sleep_times: %{ guard_id => [sleep_minutes_range, ...] }
sleep_times =
  guards_data
|> Enum.reduce(%{:current_guard_id => nil, :current_falls_asleep => nil}, fn item, acc ->
  {date, text} = item
  cond do
    String.starts_with?(text, "Guard") ->
      guard_id =
        Regex.named_captures(~r/#(?<guard_id>\d+)/, text)
        |> Map.get("guard_id")
        |> String.to_integer()
      acc = Map.put(acc, :current_guard_id, guard_id)
      acc = Map.put(acc, :current_falls_asleep, nil)
      case Map.has_key?(acc, guard_id) do
        false -> Map.put(acc, guard_id, [])
        _     -> acc
      end
    String.starts_with?(text, "falls") ->
      minute = case date.hour do
        23 -> 0
         0 -> date.minute
      end
      Map.put(acc, :current_falls_asleep, minute)
    String.starts_with?(text, "wakes") ->
      falls_asleep = Map.get(acc, :current_falls_asleep)
      wakes_up = date.minute - 1
      Map.update!(
        acc,
        Map.get(acc, :current_guard_id),
        fn current ->
          [falls_asleep..wakes_up|current]
        end
      )
  end
end)

{_, sleep_times} = Map.pop(sleep_times, :current_guard_id)
{_, sleep_times} = Map.pop(sleep_times, :current_falls_asleep)
# sleep_times |> IO.inspect(label: "sleep times")

# part 1 ----------------------------------------------------------------------
guard_ids = Map.keys(sleep_times)

total_sleep_times = # by guard_id
  guard_ids
|> Enum.reduce(%{}, fn guard_id, acc ->
  total_sleep_time =
    Map.get(sleep_times, guard_id)
    |> Enum.reduce(0, fn sleep_time, acc ->
        acc + Range.size(sleep_time)
    end)
  Map.put(acc, guard_id, total_sleep_time)
end)
# |> IO.inspect(label: "total sleep times")

sleepiest_guard_id = key_by_max_value.(total_sleep_times)

sleep_minutes =
  Map.get(sleep_times, sleepiest_guard_id)
|> Enum.reduce(%{}, fn item, acc ->
  x = Enum.reduce(item, %{}, fn item2, acc2 ->
    Map.put(acc2, item2, 1)
  end)
  Map.merge(acc, x, fn _key, val1, val2 -> val1 + val2 end)
end)
# |> IO.inspect(label: "sleep minutes")

sleepiest_minute = key_by_max_value.(sleep_minutes)

strategy1 = sleepiest_guard_id * sleepiest_minute

assert.(8950, strategy1, "part 1")
IO.puts("part 1: " <> Integer.to_string(strategy1))

# part 2 ----------------------------------------------------------------------

# sleep_minutes_by_guard_id: %{ guard_id => %{ minute => falls_asleep_count } }
sleep_minutes_by_guard_id =
  sleep_times
|> Enum.reduce(%{}, fn item, acc ->
  {guard_id, sleep_minutes} = item
  x = Enum.reduce(sleep_minutes, %{}, fn item2, acc2 ->
    y = Enum.reduce(item2, %{}, fn item3, acc3 ->
      Map.put(acc3, item3, 1)
    end)
    Map.merge(acc2, y, fn _key, val1, val2 -> val1 + val2 end)
  end)
  Map.put(acc, guard_id, x)
end)
# |> IO.inspect(label: "sleep minutes by guard id")

# sleepiest_minute_by_guard_id: %{ guard_id => minute }
sleepiest_minute_by_guard_id =
  sleep_minutes_by_guard_id
|> Enum.filter(fn item -> # remove guards not falling asleep
  {_guard_id, sleep_minutes} = item
  sleep_minutes |> Map.keys |> Kernel.length > 0
end)
|> Enum.reduce(%{}, fn item, acc ->
  {guard_id, sleep_minutes} = item
  Map.put(acc, guard_id, key_by_max_value.(sleep_minutes))
end)
# |> IO.inspect(label: "sleepiest minute by guard id")

# sleepiest_guard: { guard_id, minute, falls_asleep_count }
sleepiest_guard =
  sleepiest_minute_by_guard_id
|> Enum.map(fn item ->
  {guard_id, sleepiest_minute} = item
  x = Map.get(sleep_minutes_by_guard_id, guard_id)
  y = Map.get(x, sleepiest_minute)
  {guard_id, sleepiest_minute, y}
end)
|> Enum.max_by(fn {_guard_id, _minute, falls_asleep_count} ->
  falls_asleep_count
end)
# |> IO.inspect(label: "sleepiest guard")

{guard_id, minute, _} = sleepiest_guard

strategy2 = guard_id * minute

assert.(78452, strategy2, "part 2")
IO.puts("part 2: " <> Integer.to_string(strategy2))

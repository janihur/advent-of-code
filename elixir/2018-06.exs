assert = fn expected, actual, message ->
  if expected != actual do
    raise("#{message}: expected: #{expected} actual: #{actual}")
  end
end

# input data ------------------------------------------------------------------
landing_coordinates =
  File.stream!("../inputs/2018-06")
  # File.stream!("test-data")
|> Enum.map(&String.trim/1)
|> Enum.map(fn line ->
  [x, y] = String.split(line, ", ")
  {
    String.to_integer(x),
    String.to_integer(y)
  }
end)
|> MapSet.new()
# |> IO.inspect(label: "landing coordinates")

# common values ---------------------------------------------------------------
manhattan_distance = fn {x1, y1}, {x2, y2} ->
  (max(x1, x2) - min(x1, x2)) +
  (max(y1, y2) - min(y1, y2))
end

{min_x, min_y} =
  landing_coordinates
|> Enum.reduce({1000, 1000}, fn {x, y}, {min_x, min_y} = _acc ->
  {
    (if x < min_x, do: x, else: min_x),
    (if y < min_y, do: y, else: min_y)
  }
end)
# |> IO.inspect(label: "min coordinate")

{max_x, max_y} =
  landing_coordinates
|> Enum.reduce({0, 0}, fn {x, y}, {max_x, max_y} = _acc ->
  {
    (if x > max_x, do: x, else: max_x),
    (if y > max_y, do: y, else: max_y)
  }
end)
# |> IO.inspect(label: "max coordinate")

all_coordinates =
  for x <- min_x .. max_x,
      y <- min_y .. max_y do
    {x, y}
  end
|> MapSet.new()
# |> IO.inspect(label: "all coordinates")

# part 1 ----------------------------------------------------------------------

defmodule Impl do
  def smallest_distance({_coord1, _coord2, _distance} = current, {} = _smallest) do
    {:cont, current}
  end
  def smallest_distance(
    {current_coord1,   _current_coord2,  current_distance}  = current,
    {_smallest_coord1, _smallest_coord2, smallest_distance} = smallest
  ) do
    cond do
      current_distance == 0 ->
        {:halt, current}
      current_distance == smallest_distance -> # requires that smallest ones come first
        {:halt, {current_coord1, nil, nil}}
      current_distance < smallest_distance ->
        {:cont, current}
      true ->
        {:cont, smallest}
    end
  end
end

# calculate distance to all landing coordinates and pick the smallest or tie (nil)
closest_distances =
  all_coordinates
|> Enum.map(fn coord1 ->
  landing_coordinates
  |> Enum.map(fn coord2 ->
    {
      coord1,
      coord2,
      manhattan_distance.(coord1, coord2)
    }
  end)
  # |> IO.inspect(label: "landing coordinate distances to all coordinates [{coordinate, landing_coordinate, distance}]")
  |> Enum.sort(fn {_, _, a}, {_, _, b} -> a <= b end)
  |> Enum.reduce_while({}, &Impl.smallest_distance/2)
end)
# |> IO.inspect(label: "closest distances [{coordinate, closest_landing_coordinate, distance}]", limit: :infinity)

infinite_landing_coordinates =
  closest_distances
|> Enum.filter(fn {{x, y} = _coordinate, landing_coordinate, _distance} ->
  cond do
    landing_coordinate == nil -> false
    x == min_x -> true
    x == max_x -> true
    y == min_y -> true
    y == max_y -> true
    true       -> false
  end
end)
|> Enum.reduce(MapSet.new(), fn {_, landing_coordinate, _} = _item, acc -> MapSet.put(acc, landing_coordinate) end)
# |> IO.inspect(label: "infinite landing coordinates")

finite_landing_coordinates =
  landing_coordinates
|> Enum.reduce(MapSet.new(), fn item, acc ->
  if !MapSet.member?(infinite_landing_coordinates, item) do
    MapSet.put(acc, item)
  else
    acc
  end
end)
# |> IO.inspect(label: "finite landing coordinates")

largest_area_size =
  finite_landing_coordinates
|> Enum.map(fn landing_coordinate ->
  Enum.count(closest_distances, fn {_, landing_coordinate2, _} ->
    landing_coordinate == landing_coordinate2
  end)
end)
|> Enum.max()

assert.(5333, largest_area_size, "part 1")
IO.puts("part 1: " <> Integer.to_string(largest_area_size))

# part 2 ----------------------------------------------------------------------

area_size =
  all_coordinates
|> Enum.map(fn coord ->
  landing_coordinates |> Enum.map(&manhattan_distance.(coord, &1)) |> Enum.sum()
end)
|> Enum.count(&(&1 < 10_000))

assert.(35334, area_size, "part 2")
IO.puts("part 2: " <> Integer.to_string(area_size))

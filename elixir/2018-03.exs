assert = fn expected, actual, message ->
  if expected != actual do
    raise("#{message}: expected: #{expected} actual: #{actual}")
  end
end

# input data ------------------------------------------------------------------
rectangle_data =
  File.stream!("../inputs/2018-03")
  #File.stream!("test-data")
|> Enum.map(&String.trim/1)
|> Enum.map(fn line ->
  [claim_id, _, coordinate_str, size_str] = String.split(line)
  [coord_x, coord_y] = String.split(coordinate_str, ",")
  [size_x, size_y] = String.split(size_str, "x")
  {
    claim_id|> String.trim_leading("#")  |> String.to_integer(),
    coord_x |>                              String.to_integer(),
    coord_y |> String.trim_trailing(":") |> String.to_integer(),
    size_x  |>                              String.to_integer(),
    size_y  |>                              String.to_integer()
  }
end)
# |> IO.inspect(label: "rectangle data")

# part 1 ----------------------------------------------------------------------
all_coordinates =
  rectangle_data
|> Enum.into(%{}, fn data ->
  {claim_id, coord_x, coord_y, size_x, size_y} = data
  coordinates =
    for x <- coord_x .. coord_x + size_x - 1,
        y <- coord_y .. coord_y + size_y - 1 do
      {x,y}
    end
    # |> IO.inspect(label: "coordinates of \##{claim_id}")
    |> Enum.into(%MapSet{}, fn x -> x end)
  {claim_id, coordinates}
end)
# |> IO.inspect(label: "all coordinates")

overlapping_coordinates =
  all_coordinates
|> Enum.reduce(
  %{},
  fn claim, acc ->
    {_claim_id, claim_coordinates} = claim
    new_map = Enum.reduce(claim_coordinates, %{}, fn value, acc -> Map.update(acc, value, 1, fn value2 -> value2 + 1 end) end)
    Map.merge(acc, new_map, fn _key, value1, value2 -> value1 + value2 end)
  end
)
# |> IO.inspect(label: "overlapping coordinates")

overlapping_area =
  overlapping_coordinates
|> Enum.count(fn {_key, value} -> value > 1 end)

assert.(115348, overlapping_area, "part 1")
IO.puts("part 1: " <> Integer.to_string(overlapping_area))

# part 2 ----------------------------------------------------------------------
{non_overlapping_claim_id, _} =
  all_coordinates
|> Enum.find(fn claim ->
  {_claim_id, claim_coordinates} = claim
  Enum.all?(claim_coordinates, fn coordinate -> Map.get(overlapping_coordinates, coordinate) == 1 end)
end)
# |> IO.inspect(label: "part 2")

assert.(188, non_overlapping_claim_id, "part 2")
IO.puts("part 2: " <> Integer.to_string(non_overlapping_claim_id))

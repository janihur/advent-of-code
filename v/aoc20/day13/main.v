module main

import os
import part1

const (
	input_file = '../../../inputs/2020-13'
)

fn main() {
	puzzle_data := os.read_lines(input_file) or { panic('error reading: $input_file') }
	{
		earliest_depart_time := puzzle_data[0].int()
		bus_ids := puzzle_data[1].split(',').filter(it != 'x').map(it.int())
		answer := part1.resolve(earliest_depart_time, bus_ids)
		println('part1 answer: $answer')
		assert 2382 == answer
	}
}

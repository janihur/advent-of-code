module part1

pub fn resolve(earliest_depart_time int, bus_ids []int) int {
	mut results := []Result{}
	for id in bus_ids {
		x := earliest_depart_time / id
		x2 := earliest_depart_time % id
		x3 := if x2 > 0 { 1 } else { 0 }
		depart_time := id * (x + x3)
		// println('(id $id)(x $x)(x2 $x2)(x3 $x3)(depart_time $depart_time)')
		results << Result{id, depart_time - earliest_depart_time}
	}
	results.sort(a.wait_time < b.wait_time)
	// println(results)
	return results[0].id * results[0].wait_time
}

struct Result {
	id        int [required]
	wait_time int [required]
}

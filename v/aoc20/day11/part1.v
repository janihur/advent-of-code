module part1

pub fn count_seats(seat_type string, seat_map []string) int {
	mut count := int(0)
	for row in seat_map {
		for seat in row {
			if seat == seat_type[0] {
				count++
			}
		}
	}
	return count
}

pub fn evolve(original_seat_map []string) []string {
	mut current_seat_map := original_seat_map.clone()
	mut new_seat_map := []string{}
	for {
		new_seat_map = update_seat_map(current_seat_map)
		if new_seat_map == current_seat_map {
			break
		}
		current_seat_map = new_seat_map.clone()
	}
	return current_seat_map
}

fn update_seat_map(seat_map []string) []string {
	mut new_seat_map := []string{}
	for y in 0 .. seat_map.len {
		mut new_row := ''
		for x in 0 .. seat_map[y].len {
			new_row += next_seat_status(x, y, seat_map)
		}
		new_seat_map << new_row
	}
	return new_seat_map
}

fn next_seat_status(x int, y int, seat_map []string) string {
	seat := seat_map[y][x]
	return match seat {
		`L` {
			if count_adjacent_seats(x, y, seat_map) == 0 {
				'#'
			} else {
				'L'
			}
		}
		`#` {
			if count_adjacent_seats(x, y, seat_map) >= 4 {
				'L'
			} else {
				'#'
			}
		}
		else {
			'.'
		}
	}
}

fn count_adjacent_seats(x int, y int, seat_map []string) int {
	mut count := int(0)
	x_max := seat_map[0].len - 1
	y_max := seat_map.len - 1
	// no closures yet in v0.2
	// https://github.com/vlang/v/issues/4753
	// count_fn := fn(x_ int, y_ int) {
	// 	if x_ >= 0 && x_ <= x_max && y_ >= 0 && y_ <= y_max && seat_map[y_][x_] == `#` {
	//		count++
	//	}
	// }
	{ // up left
		x_ := x - 1
		y_ := y - 1
		if x_ >= 0 && x_ <= x_max && y_ >= 0 && y_ <= y_max && seat_map[y_][x_] == `#` {
			count++
		}
	}
	{ // up
		x_ := x
		y_ := y - 1
		if x_ >= 0 && x_ <= x_max && y_ >= 0 && y_ <= y_max && seat_map[y_][x_] == `#` {
			count++
		}
	}
	{ // up right
		x_ := x + 1
		y_ := y - 1
		if x_ >= 0 && x_ <= x_max && y_ >= 0 && y_ <= y_max && seat_map[y_][x_] == `#` {
			count++
		}
	}
	{ // left
		x_ := x - 1
		y_ := y
		if x_ >= 0 && x_ <= x_max && y_ >= 0 && y_ <= y_max && seat_map[y_][x_] == `#` {
			count++
		}
	}
	{ // right
		x_ := x + 1
		y_ := y
		if x_ >= 0 && x_ <= x_max && y_ >= 0 && y_ <= y_max && seat_map[y_][x_] == `#` {
			count++
		}
	}
	{ // down left
		x_ := x - 1
		y_ := y + 1
		if x_ >= 0 && x_ <= x_max && y_ >= 0 && y_ <= y_max && seat_map[y_][x_] == `#` {
			count++
		}
	}
	{ // down
		x_ := x
		y_ := y + 1
		if x_ >= 0 && x_ <= x_max && y_ >= 0 && y_ <= y_max && seat_map[y_][x_] == `#` {
			count++
		}
	}
	{ // down right
		x_ := x + 1
		y_ := y + 1
		if x_ >= 0 && x_ <= x_max && y_ >= 0 && y_ <= y_max && seat_map[y_][x_] == `#` {
			count++
		}
	}
	return count
}

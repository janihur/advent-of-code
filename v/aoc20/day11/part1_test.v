module part1

const (
	test_data_1 = [
		'L.LL.LL.LL',
		'LLLLLLL.LL',
		'L.L.L..L..',
		'LLLL.LL.LL',
		'L.LL.LL.LL',
		'L.LLLLL.LL',
		'..L.L.....',
		'LLLLLLLLLL',
		'L.LLLLLL.L',
		'L.LLLLL.LL',
	]
	test_data_2 = [
		'#.##.##.##',
		'#######.##',
		'#.#.#..#..',
		'####.##.##',
		'#.##.##.##',
		'#.#####.##',
		'..#.#.....',
		'##########',
		'#.######.#',
		'#.#####.##',
	]
	test_data_3 = [
		'#.LL.L#.##',
		'#LLLLLL.L#',
		'L.L.L..L..',
		'#LLL.LL.L#',
		'#.LL.LL.LL',
		'#.LLLL#.##',
		'..L.L.....',
		'#LLLLLLLL#',
		'#.LLLLLL.L',
		'#.#LLLL.##',
	]
	test_data_6 = [
		'#.#L.L#.##',
		'#LLL#LL.L#',
		'L.#.L..#..',
		'#L##.##.L#',
		'#.#L.LL.LL',
		'#.#L#L#.##',
		'..L.L.....',
		'#L#L##L#L#',
		'#.LLLLLL.L',
		'#.#L#L#.##',
	]
)

fn test_count_adjacent_seats_1() {
	// top (1st) row
	assert 0 == count_adjacent_seats(0, 0, test_data_1)
	assert 0 == count_adjacent_seats(2, 0, test_data_1)
	assert 0 == count_adjacent_seats(3, 0, test_data_1)
	assert 0 == count_adjacent_seats(5, 0, test_data_1)
	assert 0 == count_adjacent_seats(6, 0, test_data_1)
	assert 0 == count_adjacent_seats(8, 0, test_data_1)
	assert 0 == count_adjacent_seats(9, 0, test_data_1)
	// third row
	assert 0 == count_adjacent_seats(0, 2, test_data_1)
	assert 0 == count_adjacent_seats(2, 2, test_data_1)
	assert 0 == count_adjacent_seats(4, 2, test_data_1)
	assert 0 == count_adjacent_seats(7, 2, test_data_1)
	// bottom (9th) row
	assert 0 == count_adjacent_seats(0, 9, test_data_1)
	assert 0 == count_adjacent_seats(2, 9, test_data_1)
	assert 0 == count_adjacent_seats(3, 9, test_data_1)
	assert 0 == count_adjacent_seats(4, 9, test_data_1)
	assert 0 == count_adjacent_seats(5, 9, test_data_1)
	assert 0 == count_adjacent_seats(6, 9, test_data_1)
	assert 0 == count_adjacent_seats(8, 9, test_data_1)
	assert 0 == count_adjacent_seats(9, 9, test_data_1)
}

fn test_count_adjacent_seats_2() {
	// top (1st) row
	assert 2 == count_adjacent_seats(0, 0, test_data_2)
	assert 4 == count_adjacent_seats(2, 0, test_data_2)
	assert 4 == count_adjacent_seats(3, 0, test_data_2)
	assert 4 == count_adjacent_seats(5, 0, test_data_2)
	assert 3 == count_adjacent_seats(6, 0, test_data_2)
	assert 3 == count_adjacent_seats(8, 0, test_data_2)
	assert 3 == count_adjacent_seats(9, 0, test_data_2)
	// third row
	assert 4 == count_adjacent_seats(0, 2, test_data_2)
	assert 6 == count_adjacent_seats(2, 2, test_data_2)
	assert 5 == count_adjacent_seats(4, 2, test_data_2)
	assert 4 == count_adjacent_seats(7, 2, test_data_2)
	// bottom (9th) row
	assert 1 == count_adjacent_seats(0, 9, test_data_2)
	assert 3 == count_adjacent_seats(2, 9, test_data_2)
	assert 5 == count_adjacent_seats(3, 9, test_data_2)
	assert 5 == count_adjacent_seats(4, 9, test_data_2)
	assert 5 == count_adjacent_seats(5, 9, test_data_2)
	assert 4 == count_adjacent_seats(6, 9, test_data_2)
	assert 3 == count_adjacent_seats(8, 9, test_data_2)
	assert 2 == count_adjacent_seats(9, 9, test_data_2)
}

fn test_next_seat_status() {
	// top (1st) row
	assert '#' == next_seat_status(0, 0, test_data_2)
	assert '.' == next_seat_status(1, 0, test_data_2)
	assert 'L' == next_seat_status(2, 0, test_data_2)
	assert 'L' == next_seat_status(3, 0, test_data_2)
	assert '.' == next_seat_status(4, 0, test_data_2)
	assert 'L' == next_seat_status(5, 0, test_data_2)
	assert '#' == next_seat_status(6, 0, test_data_2)
	assert '.' == next_seat_status(7, 0, test_data_2)
	assert '#' == next_seat_status(8, 0, test_data_2)
	assert '#' == next_seat_status(9, 0, test_data_2)
	// second row
	assert '#' == next_seat_status(0, 1, test_data_2)
	assert 'L' == next_seat_status(1, 1, test_data_2)
	assert 'L' == next_seat_status(2, 1, test_data_2)
	assert 'L' == next_seat_status(3, 1, test_data_2)
	assert 'L' == next_seat_status(4, 1, test_data_2)
	assert 'L' == next_seat_status(5, 1, test_data_2)
	assert 'L' == next_seat_status(6, 1, test_data_2)
	assert '.' == next_seat_status(7, 1, test_data_2)
	assert 'L' == next_seat_status(8, 1, test_data_2)
	assert '#' == next_seat_status(9, 1, test_data_2)
}

fn test_update_seat_map() {
	assert test_data_2 == update_seat_map(test_data_1)
	assert test_data_3 == update_seat_map(test_data_2)
}

fn test_evolve() {
	assert test_data_6 == evolve(test_data_1)
}

fn test_count_seats() {
	assert 37 == count_seats('#', test_data_6)
}

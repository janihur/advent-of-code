module part2

const (
	test_data_1 = [
		'L.LL.LL.LL',
		'LLLLLLL.LL',
		'L.L.L..L..',
		'LLLL.LL.LL',
		'L.LL.LL.LL',
		'L.LLLLL.LL',
		'..L.L.....',
		'LLLLLLLLLL',
		'L.LLLLLL.L',
		'L.LLLLL.LL',
	]
	test_data_2 = [
		'#.##.##.##',
		'#######.##',
		'#.#.#..#..',
		'####.##.##',
		'#.##.##.##',
		'#.#####.##',
		'..#.#.....',
		'##########',
		'#.######.#',
		'#.#####.##',
	]
	test_data_3 = [
		'#.LL.LL.L#',
		'#LLLLLL.LL',
		'L.L.L..L..',
		'LLLL.LL.LL',
		'L.LL.LL.LL',
		'L.LLLLL.LL',
		'..L.L.....',
		'LLLLLLLLL#',
		'#.LLLLLL.L',
		'#.LLLLL.L#',
	]
	test_data_4 = [
		'#.L#.##.L#',
		'#L#####.LL',
		'L.#.#..#..',
		'##L#.##.##',
		'#.##.#L.##',
		'#.#####.#L',
		'..#.#.....',
		'LLL####LL#',
		'#.L#####.L',
		'#.L####.L#',
	]
	test_data_5 = [
		'#.L#.L#.L#',
		'#LLLLLL.LL',
		'L.L.L..#..',
		'##LL.LL.L#',
		'L.LL.LL.L#',
		'#.LLLLL.LL',
		'..L.L.....',
		'LLLLLLLLL#',
		'#.LLLLL#.L',
		'#.L#LL#.L#',
	]
	test_data_6 = [
		'#.L#.L#.L#',
		'#LLLLLL.LL',
		'L.L.L..#..',
		'##L#.#L.L#',
		'L.L#.#L.L#',
		'#.L####.LL',
		'..#.#.....',
		'LLL###LLL#',
		'#.LLLLL#.L',
		'#.L#LL#.L#',
	]
	test_data_7 = [
		'#.L#.L#.L#',
		'#LLLLLL.LL',
		'L.L.L..#..',
		'##L#.#L.L#',
		'L.L#.LL.L#',
		'#.LLLL#.LL',
		'..#.L.....',
		'LLL###LLL#',
		'#.LLLLL#.L',
		'#.L#LL#.L#',
	]
)

fn test_count_visible_seats() {
	seat_map_1 := [
		'.......#.',
		'...#.....',
		'.#.......',
		'.........',
		'..#L....#',
		'....#....',
		'.........',
		'#........',
		'...#.....',
	]
	assert 8 == count_visible_seats(3, 4, seat_map_1)
	seat_map_2 := [
		'.............',
		'.L.L.#.#.#.#.',
		'.............',
	]
	assert 0 == count_visible_seats(1, 1, seat_map_2)
	seat_map_3 := [
		'.##.##.',
		'#.#.#.#',
		'##...##',
		'...L...',
		'##...##',
		'#.#.#.#',
		'.##.##.',
	]
	assert 0 == count_visible_seats(3, 3, seat_map_3)
}

fn test_update_seat_map() {
	assert test_data_2 == update_seat_map(test_data_1)
	assert test_data_3 == update_seat_map(test_data_2)
	assert test_data_4 == update_seat_map(test_data_3)
	assert test_data_5 == update_seat_map(test_data_4)
	assert test_data_6 == update_seat_map(test_data_5)
	assert test_data_7 == update_seat_map(test_data_6)
	assert test_data_7 == update_seat_map(test_data_7)
}

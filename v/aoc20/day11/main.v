module main

import os
import part1
import part2

const (
	input_file = '../../../inputs/2020-11'
)

fn main() {
	seat_map := os.read_lines(input_file) or { panic('error reading: $input_file') }
	{
		occupied_seats := part1.count_seats('#', part1.evolve(seat_map))
		println('part1 answer: $occupied_seats')
		assert 2424 == occupied_seats
	}
	{
		occupied_seats := part2.count_seats('#', part2.evolve(seat_map))
		println('part2 answer: $occupied_seats')
		assert 2208 == occupied_seats
	}
}

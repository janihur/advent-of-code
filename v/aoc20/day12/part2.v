module part2

import part1

pub fn run(navigation_instructions []part1.NavigationInstruction) part1.Position {
	mut waypoint := part1.Position{
		x: 10 // 10 east
		y: 1 // 1 north
	}
	mut ship := part1.Position{
		x: 0
		y: 0
	}
	for instruction in navigation_instructions {
		match instruction {
			part1.East {
				waypoint.x += instruction.value
			}
			part1.North {
				waypoint.y += instruction.value
			}
			part1.South {
				waypoint.y -= instruction.value
			}
			part1.West {
				waypoint.x -= instruction.value
			}
			part1.Left {
				modified_rotation := match instruction.value {
					90 { int(270) }
					270 { int(90) }
					else { instruction.value }
				}
				waypoint = recalculate_waypoint(waypoint, modified_rotation)
			}
			part1.Right {
				waypoint = recalculate_waypoint(waypoint, instruction.value)
			}
			part1.Forward {
				ship.x += instruction.value * waypoint.x
				ship.y += instruction.value * waypoint.y
			}
		}
		// print('(ship ($ship.x, $ship.y))')
		// print('(waypoint ($waypoint.x, $waypoint.y))')
		// println('')
	}
	return ship
}

fn recalculate_waypoint(current_waypoint part1.Position, rotation int) part1.Position {
	next_waypoint := fn (x int, y int) (int, int) {
		return y, -1 * x
	}
	mut x := current_waypoint.x
	mut y := current_waypoint.y
	for _ in 0 .. rotation / 90 {
		x, y = next_waypoint(x, y)
	}
	return part1.Position{x, y}
}

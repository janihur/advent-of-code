module main

import os
import part1
import part2

const (
	input_file = '../../../inputs/2020-12'
)

fn main() {
	navigation_instructions := os.read_lines(input_file) or { panic('error reading: $input_file') }
	{
		distance := part1.manhattan_distance(part1.run(part1.parse(navigation_instructions)))
		println('part1 answer: $distance')
		assert 1457 == distance
	}
	{
		distance := part1.manhattan_distance(part2.run(part1.parse(navigation_instructions)))
		println('part2 answer: $distance')
		assert 106860 == distance
	}
}

module part1

const (
	test_data = [
		'F10',
		'N3',
		'F7',
		'R90',
		'F11',
	]
)

fn test_parse() {
	// failed to figure out the correct initialization syntax
	// mut expected := []NavigationInstruction{}
	// expected << Forward{10}
	// expected << North{3}
	// expected << Forward{7}
	// expected << Right{90}
	// expected << Forward{11}
	// compilation failure:
	// assert expected == parse(test_data)
	got := parse(test_data)
	assert got[0] is Forward
	g0 := got[0] as Forward
	assert 10 == g0.value
	assert got[1] is North
	g1 := got[1] as North
	assert 3 == g1.value
	//	assert expected[0].type_name() == got[0].type_name()
	//	assert expected[0].value == got[0].value
}

fn test_run() {
	// failed to figure out the correct initialization syntax
	mut navigation_instructions := []NavigationInstruction{}
	navigation_instructions << Forward{10}
	navigation_instructions << North{3}
	navigation_instructions << Forward{7}
	navigation_instructions << Right{90}
	navigation_instructions << Forward{11}
	got := run(navigation_instructions)
	assert 17 == got.x
	assert -8 == got.y
}

fn test_new_direction() {
	{
		got := new_direction(East{}, Right{90})
		assert 'part1.South' == got.type_name()
	}
	{
		got := new_direction(East{}, Right{180})
		assert 'part1.West' == got.type_name()
	}
	{
		got := new_direction(East{}, Right{270})
		assert 'part1.North' == got.type_name()
	}
	{
		got := new_direction(East{}, Left{90})
		assert 'part1.North' == got.type_name()
	}
	{
		got := new_direction(East{}, Left{180})
		assert 'part1.West' == got.type_name()
	}
	{
		got := new_direction(East{}, Left{270})
		assert 'part1.South' == got.type_name()
	}
}

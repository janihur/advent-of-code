module part2

import part1

const (
	test_data = [
		'F10',
		'N3',
		'F7',
		'R90',
		'F11',
	]
)

fn test_run() {
	// failed to figure out the correct initialization syntax
	mut navigation_instructions := []part1.NavigationInstruction{}
	navigation_instructions << part1.Forward{10}
	navigation_instructions << part1.North{3}
	navigation_instructions << part1.Forward{7}
	navigation_instructions << part1.Right{90}
	navigation_instructions << part1.Forward{11}
	got := run(navigation_instructions)
	assert 214 == got.x
	assert -72 == got.y
}

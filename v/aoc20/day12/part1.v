module part1

pub struct Position {
pub mut:
	x int [required]
	y int [required]
}

pub fn manhattan_distance(position Position) int {
	return int(f32_abs(position.x) + f32_abs(position.y))
}

pub fn run(navigation_instructions []NavigationInstruction) Position {
	mut current_direction := NavigationInstruction{}
	current_direction = East{0}
	mut x := int(0)
	mut y := int(0)
	for instruction in navigation_instructions {
		match instruction {
			East { x += instruction.value }
			North { y += instruction.value }
			South { y -= instruction.value }
			West { x -= instruction.value }
			Left { current_direction = new_direction(current_direction, instruction) }
			Right { current_direction = new_direction(current_direction, instruction) }
			Forward { match current_direction {
					East { x += instruction.value }
					North { y += instruction.value }
					South { y -= instruction.value }
					West { x -= instruction.value }
					else { panic }
				} }
		}
		//		print(x)
		//		print(' ')
		//		print(y)
		//		print(' ')
		//		print(current_direction.type_name())
		//		println('')
	}
	return Position{
		x: x
		y: y
	}
}

fn new_direction(old_direction NavigationInstruction, steering NavigationInstruction) NavigationInstruction {
	mut directions_right := []NavigationInstruction{}
	directions_right << North{}
	directions_right << East{}
	directions_right << South{}
	directions_right << West{}
	mut directions_left := []NavigationInstruction{}
	directions_left << North{}
	directions_left << West{}
	directions_left << South{}
	directions_left << East{}
	match steering {
		Left {
			direction_offset := steering.value / 90
			old_direction_index := index_of(old_direction, directions_left)
			new_direction_index := get_new_direction_index(old_direction_index, direction_offset)
			return directions_left[new_direction_index]
		}
		Right {
			direction_offset := steering.value / 90
			old_direction_index := index_of(old_direction, directions_right)
			new_direction_index := get_new_direction_index(old_direction_index, direction_offset)
			//			print(old_direction_index)
			//			print(' + ')
			//			print(direction_offset)
			//			print(' => ')
			//			print(new_direction_index)
			//			println('')
			return directions_right[new_direction_index]
		}
		else {}
	}
	return old_direction
}

fn get_new_direction_index(old_index int, offset int) int {
	x := old_index + offset
	if x <= 3 {
		return x
	} else {
		return x % 4
	}
}

fn index_of(value NavigationInstruction, array []NavigationInstruction) int {
	for i in 0 .. array.len {
		if value.type_name() == array[i].type_name() {
			return i
		}
	}
	return -1
}

// builder error
// fn index_of(value NavigationInstruction, array []NavigationInstruction) int {
//	for i, item in array {
//		if value.type_name() == item.type_name() {
//			return i
//		}
//	}
//	return -1
//}
pub struct North {
pub:
	value int
}

pub struct South {
pub:
	value int
}

pub struct East {
pub:
	value int
}

pub struct West {
pub:
	value int
}

pub struct Left {
pub:
	value int [required]
}

pub struct Right {
pub:
	value int [required]
}

pub struct Forward {
pub:
	value int [required]
}

// trying to use sum type of sum types caused all kind of probelesm
// type Direction = East | North | South | West
// type Steering = Left | Right
type NavigationInstruction = East | Forward | Left | North | Right | South | West

pub fn parse(navigation_instructions []string) []NavigationInstruction {
	mut new_navigation_instructions := []NavigationInstruction{}
	for single_instruction in navigation_instructions {
		action := single_instruction[0]
		value := single_instruction[1..].int()
		match action {
			`E` { new_navigation_instructions << East{value} }
			`F` { new_navigation_instructions << Forward{value} }
			`L` { new_navigation_instructions << Left{value} }
			`N` { new_navigation_instructions << North{value} }
			`R` { new_navigation_instructions << Right{value} }
			`S` { new_navigation_instructions << South{value} }
			`W` { new_navigation_instructions << West{value} }
			else { panic }
		}
	}
	return new_navigation_instructions
}

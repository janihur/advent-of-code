# Advent of Code - V

The V programming language - https://vlang.io/

Compiled with version [0.2](https://vlang.io/):

```
$ v version
V 0.2 e4f94b6
```

Have to be compiled and run from the module directory because relative dependency to the input data in [`../inputs`](../inputs):

```
cd aocNN/dayNN
v -stats test .
v run .
# or
v . && ./dayNN
```

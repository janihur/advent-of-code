module intcode19v2;

import std.algorithm : reverse;
import std.range : dropOne, empty;
import std.stdio : writefln;

enum OpCode : uint {
  ADD = 1,
  MULTIPLY = 2,
  SAVE = 3,
  OUT = 4,
  JUMP_IF_TRUE = 5,
  JUMP_IF_FALSE  = 6,
  LESS_THAN  = 7,
  EQUALS = 8,
  HALT = 99,
}

enum ParamMode : uint {
  POSITION = 0,
  IMMEDIATE = 1,
}

enum Status : uint {
  INITIALIZED = 0,
  RUNNING = 1,
  SUSPENDED = 2,
  HALTED = 3,
}

int[] splitOpCode(int opcode) {
  int[] opcodes;
  opcodes ~= opcode / 10000;
  opcodes ~= (opcode % 10000) / 1000;
  opcodes ~= ((opcode % 10000) % 1000) / 100;
  opcodes ~= ((opcode % 10000) % 1000) % 100;
  return opcodes.reverse;
}

unittest {
  assert(splitOpCode(54321) == [21, 3, 4, 5]);
  assert(splitOpCode( 4321) == [21, 3, 4, 0]);
  assert(splitOpCode(  321) == [21, 3, 0, 0]);
  assert(splitOpCode(   21) == [21, 0, 0, 0]);
  assert(splitOpCode(    1) == [ 1, 0, 0, 0]);
  assert(splitOpCode(   99) == [99, 0, 0, 0]);
}

struct IntCode19 {
  Status status;
  int[] memory;
  int pc; // program counter
  int[] output;
  int[] input;

  @disable this();
  
  this(immutable int[] program, int[] input) {
    this.status = Status.INITIALIZED;
    this.memory = program.dup;
    this.input = input.dup;
  }

  this(immutable int[] program) {
    this.status = Status.INITIALIZED;
    this.memory = program.dup;
  }

  void addInput(int[] input) {
    this.input ~= input.dup;
  }

  void setNoun(int noun) {
    this.memory[1] = noun;
  }

  void setVerb(int verb) {
    this.memory[2] = verb;
  }

  ref int at(int pos, int mode) {
    switch (mode) {
    case ParamMode.POSITION:
      return memory[memory[pos]];
    case ParamMode.IMMEDIATE:
      return memory[pos];
    default:
      assert(0);
    }
  }
  
  void run() {
    status = Status.RUNNING;
  out_: while (true) {
      debug writefln("(pc = %s)(memory[pc] = %s)(memory = %s)", pc, memory[pc], memory);
      auto opcode = memory[pc].splitOpCode;
      switch (opcode[0]) {
      case OpCode.ADD:
        int op1 = at(pc+1, opcode[1]);
        int op2 = at(pc+2, opcode[2]);
        at(pc+3, opcode[3]) = op1 + op2;
        pc += 4;
        break;
      case OpCode.MULTIPLY:
        int op1 = at(pc+1, opcode[1]);
        int op2 = at(pc+2, opcode[2]);
        at(pc+3, opcode[3]) = op1 * op2;
        pc += 4;
        break;
      case OpCode.SAVE:
        if (input.empty) {
          status = Status.SUSPENDED;
          break out_;
        } else {
          at(pc+1, opcode[1]) = input[0];
          input = input.dropOne;
          pc += 2;
        }
        break;
      case OpCode.OUT:
        output ~= at(pc+1, opcode[1]);
        pc += 2;
        break;
      case OpCode.JUMP_IF_TRUE:
        int op1 = at(pc+1, opcode[1]);
        int op2 = at(pc+2, opcode[2]);
        if (op1 != 0) {
          pc = op2;
        } else {
          pc += 3;
        }
        break;
      case OpCode.JUMP_IF_FALSE:
        int op1 = at(pc+1, opcode[1]);
        int op2 = at(pc+2, opcode[2]);
        if (op1 == 0) {
          pc = op2;
        } else {
          pc += 3;
        }
        break;
      case OpCode.LESS_THAN:
        int op1 = at(pc+1, opcode[1]);
        int op2 = at(pc+2, opcode[2]);
        if (op1 < op2) {
          at(pc+3, opcode[3]) = 1;
        } else {
          at(pc+3, opcode[3]) = 0;
        }
        pc += 4;
        break;
      case OpCode.EQUALS:
        int op1 = at(pc+1, opcode[1]);
        int op2 = at(pc+2, opcode[2]);
        if (op1 == op2) {
          at(pc+3, opcode[3]) = 1;
        } else {
          at(pc+3, opcode[3]) = 0;
        }
        pc += 4;
        break;
      case OpCode.HALT:
        status = Status.HALTED;
        break out_;
      default:
        writefln("unknown opcode: (opcode = %s)(memory = %s)", opcode, memory);
        assert(0);
      }
    }
  }
}

// version 1 tests (DAY02) -----------------------------------------------------

unittest {
  auto computer = IntCode19([1,0,0,0,99]);
  computer.run;
  assert(computer.memory == [2,0,0,0,99]);
}
unittest {
  auto computer = IntCode19([2,3,0,3,99]);
  computer.run;
  assert(computer.memory == [2,3,0,6,99]);
}
unittest {
  auto computer = IntCode19([2,4,4,5,99,0]);
  computer.run;
  assert(computer.memory == [2,4,4,5,99,9801]);
}
unittest {
  auto computer = IntCode19([1,1,1,4,99,5,6,0,99]);
  computer.run;
  assert(computer.memory == [30,1,1,4,2,5,6,0,99]);
}
unittest {
  auto computer = IntCode19([1,9,10,3,2,3,11,0,99,30,40,50]);
  computer.run;
  assert(computer.memory == [3500,9,10,70,2,3,11,0,99,30,40,50]);
}

// version 2 tests (DAY05) -----------------------------------------------------

unittest {
  auto computer = IntCode19([1002,4,3,4,33]);
  computer.run;
  assert(computer.memory == [1002,4,3,4,99]);
}
unittest {
  auto computer = IntCode19([1101,100,-1,4,0]);
  computer.run;
  assert(computer.memory == [1101,100,-1,4,99]);
}
unittest {
  auto computer = IntCode19([3,9,8,9,10,9,4,9,99,-1,8], [8]);
  computer.run;
  assert(computer.output == [1]);
}
unittest {
  auto computer = IntCode19([3,9,7,9,10,9,4,9,99,-1,8], [8]);
  computer.run;
  assert(computer.output == [0]);
}
unittest {
  auto computer = IntCode19([3,3,1108,-1,8,3,4,3,99], [8]);
  computer.run;
  assert(computer.output == [1]);
}
unittest {
  auto computer = IntCode19([3,3,1107,-1,8,3,4,3,99], [8]);
  computer.run;
  assert(computer.output == [0]);
}
unittest {
  import std.typecons : Tuple, tuple;
  alias T = Tuple!(int[], "input", int[], "expected");

  T[3] tests;
  tests[0].input = [7];
  tests[0].expected = [999];

  tests[1].input = [8];
  tests[1].expected = [1000];

  tests[2].input = [9];
  tests[2].expected = [1001];

  foreach (test; tests) {
    auto computer = IntCode19([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], test.input);
    computer.run;
    assert(computer.output == test.expected);
  }
}

// version 3 tests (DAY07) -----------------------------------------------------

unittest {
  immutable int[] program = [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5];

  // init and the first run
  auto amp1 = IntCode19(program, [9, 0]);
  amp1.run;
  // writefln("(amp1.status = %s)", amp1.status);
  // writefln("(amp1.output = %s)", amp1.output);
  // writefln("(amp1.memory = %s)", amp1.memory);

  auto amp2 = IntCode19(program, [8, amp1.output[0]]);
  amp2.run;
  // writefln("(amp2.status = %s)", amp2.status);
  // writefln("(amp2.output = %s)", amp2.output);
  // writefln("(amp2.memory = %s)", amp2.memory);

  auto amp3 = IntCode19(program,[7, amp2.output[0]]);
  amp3.run;
  // writefln("(amp3.status = %s)", amp3.status);
  // writefln("(amp3.output = %s)", amp3.output);
  // writefln("(amp3.memory = %s)", amp3.memory);
  
  auto amp4 = IntCode19(program,[6, amp3.output[0]]);
  amp4.run;
  // writefln("(amp4.status = %s)", amp4.status);
  // writefln("(amp4.output = %s)", amp4.output);
  // writefln("(amp4.memory = %s)", amp4.memory);

  auto amp5 = IntCode19(program,[5, amp4.output[0]]);
  amp5.run;
  // writefln("(amp5.status = %s)", amp5.status);
  // writefln("(amp5.output = %s)", amp5.output);
  // writefln("(amp5.memory = %s)", amp5.memory);
  
  // the following runs
  // foreach (i; 2 .. 6) {
  while (amp5.status != Status.HALTED) {
    amp1.addInput([amp5.output[$-1]]);
    amp1.run;

    // writefln("(%s)(amp1.status = %s)", i, amp1.status);
    // writefln("(%s)(amp1.output = %s)", i, amp1.output);
    // writefln("(%s)(amp1.memory = %s)", i, amp1.memory);

    amp2.addInput([amp1.output[$-1]]);
    amp2.run;

    // writefln("(%s)(amp2.status = %s)", i, amp2.status);
    // writefln("(%s)(amp2.output = %s)", i, amp2.output);
    // writefln("(%s)(amp2.memory = %s)", i, amp2.memory);
  
    amp3.addInput([amp2.output[$-1]]);
    amp3.run;

    // writefln("(%s)(amp3.status = %s)", i, amp3.status);
    // writefln("(%s)(amp3.output = %s)", i, amp3.output);
    // writefln("(%s)(amp3.memory = %s)", i, amp3.memory);

    amp4.addInput([amp3.output[$-1]]);
    amp4.run;

    // writefln("(%s)(amp4.status = %s)", i, amp4.status);
    // writefln("(%s)(amp4.output = %s)", i, amp4.output);
    // writefln("(%s)(amp4.memory = %s)", i, amp4.memory);
  
    amp5.addInput([amp4.output[$-1]]);
    amp5.run;

    // writefln("(%s)(amp5.status = %s)", i, amp5.status);
    // writefln("(%s)(amp5.output = %s)", i, amp5.output);
    // writefln("(%s)(amp5.memory = %s)", i, amp5.memory);
  }

  assert(amp5.output == [129, 4257, 136353, 4363425, 139629729]);
}

unittest {
  immutable int[] program = [3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10];

  // init and the first run

  auto amp1 = IntCode19(program, [9, 0]);
  amp1.run;

  auto amp2 = IntCode19(program, [7, amp1.output[0]]);
  amp2.run;

  auto amp3 = IntCode19(program, [8, amp2.output[0]]);
  amp3.run;

  auto amp4 = IntCode19(program, [5, amp3.output[0]]);
  amp4.run;

  auto amp5 = IntCode19(program, [6, amp4.output[0]]);
  amp5.run;

  while (amp5.status != Status.HALTED) {
    amp1.addInput([amp5.output[$-1]]);
    amp1.run;

    amp2.addInput([amp1.output[$-1]]);
    amp2.run;

    amp3.addInput([amp2.output[$-1]]);
    amp3.run;

    amp4.addInput([amp3.output[$-1]]);
    amp4.run;

    amp5.addInput([amp4.output[$-1]]);
    amp5.run;
  }

  assert(amp5.output == [19, 58, 128, 271, 552, 1123, 2266, 4544, 9103, 18216]);
}

module aoc19day08;

import std.algorithm : count, map;
import std.array : array, replace, split;
import std.stdio : writefln;
import std.string : chomp;

string[] getLayers(string image, uint layerSize)
pure
{
  string[] layers;
  for (uint i = 0; i < image.length; i+=layerSize) {
    layers ~= image[i .. i+layerSize];
  }
  return layers;
}

ulong part1(string image, uint width, uint height)
pure
{
  auto layers = getLayers(image, width * height);

  ulong x = width * height + 1;
  string minLayer;
  foreach(layer; layers) {
    auto y = layer.count("0");
    if (y < x) {
      x = y;
      minLayer = layer;
    }
  }

  return minLayer.count("1") * minLayer.count("2");
}

char[][] part2(string image, uint width, uint height)
pure
{
  auto layers = getLayers(image, width * height);

  char[] singleLayer = new char[](width * height);
  for (uint i = 0; i < width * height; i++) {
    singleLayer[i] = '.';
  }
  
  foreach (layer; layers) {
    foreach (i, digit; layer) {
      if (singleLayer[i] == '.' && digit != '2') {
        singleLayer[i] = digit;
      }
    }
  }

  char[][] picture;
  for (uint i = 0; i < singleLayer.length; i+=width) {
    picture ~= singleLayer[i .. i+width];
  }
  return picture;
} unittest {
  assert(["01", "10"] == part2("0222112222120000", 2, 2));
  assert(["011", "011"] == part2("022222112222221222000011", 3, 2));
}

void main()
{
  immutable string image = import("2019-08").chomp;

  {
    auto answer = part1(image, 25, 6);
    writefln("Part1: (answer = %s)", answer);
    assert(answer == 1620);
  }
  {
    auto answer = part2(image, 25, 6);
    
    writefln("Part2: (answer = %s)", answer);
    assert(answer == ["1110001100100011111011110",
                      "1001010010100011000010000",
                      "1110010000010101110011100",
                      "1001010000001001000010000",
                      "1001010010001001000010000",
                      "1110001100001001111010000",]);
    foreach (row; answer) {
      writefln(row.replace("0", " ").replace("1", "█"));
    }
  }
}

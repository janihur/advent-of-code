module aoc19day07;

import std.algorithm : map, max, maxElement, permutations;
import std.array : array;
import std.conv : to;
import std.range : dropOne, empty, iota;
import std.stdio : writefln;
import std.string : chomp, split;

import intcode19v2;

int compute1(immutable int[] program, int phase, int inputSignal) {
  auto computer = IntCode19(program, [phase, inputSignal]);
  computer.run;
  return computer.output[0];
}

int computeOutputSignal(immutable int[] program, int[] phaseSettings, int inputSignal) {
  debug writefln("(phaseSettings = %s)(inputSignal = %s)", phaseSettings, inputSignal);
  if (phaseSettings.empty) { return inputSignal; }

  auto signal = compute1(program, phaseSettings[0], inputSignal);
  return computeOutputSignal(program, phaseSettings.dropOne, signal);
} unittest {
  immutable int[] program = [3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0];
  int[] phaseSettings = [4,3,2,1,0];
  auto outputSignal = computeOutputSignal(program, phaseSettings, 0);
  assert(outputSignal == 43210);
} unittest {
  immutable int[] program = [3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0];
  int[] phaseSettings = [0,1,2,3,4];
  auto outputSignal = computeOutputSignal(program, phaseSettings, 0);
  assert(outputSignal == 54321);
} unittest {
  immutable int[] program = [3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0];
  int[] phaseSettings = [1,0,4,3,2];
  auto outputSignal = computeOutputSignal(program, phaseSettings, 0);
  assert(outputSignal == 65210);
}

int part1(immutable int[] program)
{
  auto phaseSettings = 5.iota.permutations;
  int[] results;
  foreach (phaseSetting; phaseSettings) {
    results ~= computeOutputSignal(program, phaseSetting.array, 0);
  }
  return results.maxElement;
}

int part2(immutable int[] program)
{
  int maxThrusterSignal;
  auto phaseSettings = iota(5, 10).permutations;

  foreach (phaseSetting; phaseSettings) {
    // initialize the amplifiers and run them first time
    auto amp1 = IntCode19(program, [phaseSetting[0], 0]);
    amp1.run;
    auto amp2 = IntCode19(program, [phaseSetting[1], amp1.output[$-1]]);
    amp2.run;
    auto amp3 = IntCode19(program, [phaseSetting[2], amp2.output[$-1]]);
    amp3.run;
    auto amp4 = IntCode19(program, [phaseSetting[3], amp3.output[$-1]]);
    amp4.run;
    auto amp5 = IntCode19(program, [phaseSetting[4], amp4.output[$-1]]);
    amp5.run;

    // subsequent runs
    while (amp5.status != Status.HALTED) {
      amp1.addInput([amp5.output[$-1]]);
      amp1.run;

      amp2.addInput([amp1.output[$-1]]);
      amp2.run;

      amp3.addInput([amp2.output[$-1]]);
      amp3.run;

      amp4.addInput([amp3.output[$-1]]);
      amp4.run;

      amp5.addInput([amp4.output[$-1]]);
      amp5.run;
    }

    maxThrusterSignal = max(maxThrusterSignal, amp5.output[$-1]);
  }

  return maxThrusterSignal;
}

void main()
{
  immutable string[] puzzleData = import("2019-07").chomp.split(",");
  immutable int[] program = puzzleData.map!(to!int).array;
  // writefln("(program = %s)", program);

  {
    auto answer = part1(program);
    writefln("Part1: (answer = %s)", answer);
    assert(answer == 46248);
  }
  {
    auto answer = part2(program);
    writefln("Part2: (answer = %s)", answer);
    assert(answer == 54163586);
  }
}

module intcode19;

import std.stdio : writefln;

enum OpCode : uint {
  ADD = 1,
  MULTIPLY = 2,
  HALT = 99,
}

struct IntCode19 {
  int[] memory;
  int pc; // program counter

  @disable this();
  
  this(immutable int[] program) {
    this.memory = program.dup;
  }

  void setNoun(int noun) {
    this.memory[1] = noun;
  }

  void setVerb(int verb) {
    this.memory[2] = verb;
  }

  void run() {
  out_: while (true) {
      debug writefln("(pc = %s)(memory[pc] = %s)", pc, memory[pc]);
      switch (memory[pc]) {
      case OpCode.ADD:
        int op1 = memory[memory[pc+1]];
        int op2 = memory[memory[pc+2]];
        int retPos = memory[pc+3];
        memory[retPos] = op1 + op2;
        break;
      case OpCode.MULTIPLY:
        int op1 = memory[memory[pc+1]];
        int op2 = memory[memory[pc+2]];
        int retPos = memory[pc+3];
        memory[retPos] = op1 * op2;
        break;
      case OpCode.HALT:
        break out_;
      default:
        assert(0); // unknown opcode
      }
      pc += 4;
    }
  }
}

unittest { // version 1 tests (DAY02)
  {
    auto computer = IntCode19([1,0,0,0,99]);
    computer.run;
    assert(computer.memory == [2,0,0,0,99]);
  }
  {
    auto computer = IntCode19([2,3,0,3,99]);
    computer.run;
    assert(computer.memory == [2,3,0,6,99]);
  }
  {
    auto computer = IntCode19([2,4,4,5,99,0]);
    computer.run;
    assert(computer.memory == [2,4,4,5,99,9801]);
  }
  {
    auto computer = IntCode19([1,1,1,4,99,5,6,0,99]);
    computer.run;
    assert(computer.memory == [30,1,1,4,2,5,6,0,99]);
  }
  {
    auto computer = IntCode19([1,9,10,3,2,3,11,0,99,30,40,50]);
    computer.run;
    assert(computer.memory == [3500,9,10,70,2,3,11,0,99,30,40,50]);
  }
}

module aoc2018day6;

import std.array : array;
import std.typecons : Nullable, Tuple;

version(unittest) {
  immutable coordinateStringsTest = ["1, 1",
                                     "1, 6",
                                     "8, 3",
                                     "3, 4",
                                     "5, 5",
                                     "8, 9",];
}

// coordinate system: topLeft = 0, 0 then increments to right and down
struct Point {
  uint x;
  uint y;

  // this was unnecessary in the end but left here as example
  // this is compatible with default generated opEquals
  int opCmp(ref const Point rhs) const {
    if (x == rhs.x && y == rhs.y) return 0;
    if (x < rhs.x) return -1;
    if (x == rhs.x && y < rhs.y) return -1;
    return +1;
  }
} unittest {
  import std.algorithm : fold, sort;
  import std.algorithm.comparison : min, max;

  auto points = [Point(275, 276),
                 Point(176, 108),
                 Point(192, 220),
                 Point(270, 134),
                 Point(192, 224),
                 Point(252, 104)];
  assert(points.length == 6);
  assert(points.fold!(min) == Point(176, 108));
  assert(points.fold!(max) == Point(275, 276));

  // no way to compare arrays so compare elements one by one
  auto sorted = points.sort.array;
  assert(sorted[0] == Point(176, 108));
  assert(sorted[1] == Point(192, 220));
  assert(sorted[2] == Point(192, 224));
  assert(sorted[3] == Point(252, 104));
  assert(sorted[4] == Point(270, 134));
  assert(sorted[5] == Point(275, 276));
}

Point makePoint(in string point)
pure @safe
{
  import std.array : split;
  import std.conv : to;

  immutable p = point.split(", ");
  return Point(to!uint(p[0]), to!uint(p[1]));
} unittest {
  assert(Point(1,1) == makePoint("1, 1"));
  assert(Point(139,318) == makePoint("139, 318"));
}

alias CanvasCorners = Tuple!(Point, "topLeft", Point, "bottomRight");
alias Canvas = Tuple!(Point, "point",
                      Nullable!Point, "closest" // if null -> a tie
                      )[];

CanvasCorners makeCanvasCorners(in Point[] coordinates)
pure @safe
{
  import std.algorithm : fold, map;
  import std.algorithm.comparison : max, min;

  CanvasCorners corners;

  immutable cornersX = coordinates.map!(a => a.x).fold!(min, max);
  immutable cornersY = coordinates.map!(a => a.y).fold!(min, max);
  corners.topLeft = Point(cornersX[0], cornersY[0]);
  corners.bottomRight = Point(cornersX[1], cornersY[1]);

  return corners;
} unittest {
  import std.algorithm : map;
  immutable coordinates = coordinateStringsTest.map!(a => makePoint(a)).array;
  immutable corners = makeCanvasCorners(coordinates);
  assert(corners.topLeft == Point(1,1));
  assert(corners.bottomRight == Point(8,9));
}

Canvas makeCanvas(in CanvasCorners corners)
pure @safe
{
  immutable canvasXSize = corners.bottomRight.x - corners.topLeft.x + 1;
  immutable canvasYSize = corners.bottomRight.y - corners.topLeft.y + 1;

  Canvas canvas = new Canvas(canvasXSize * canvasYSize);

  auto x = cast()corners.topLeft.x;
  auto y = cast()corners.topLeft.y;

  foreach(index, ref location; canvas) {
    location.point = Point(x, y);
    if ((index + 1) % canvasXSize == 0) {
      x = corners.topLeft.x;
      y += 1;
    } else {
      x += 1;
    }
  }

  return canvas;
} unittest {
  import std.algorithm : map;
  immutable coordinates = coordinateStringsTest.map!(a => makePoint(a)).array;
  immutable corners = makeCanvasCorners(coordinates);
  immutable canvas = makeCanvas(corners);
  assert(canvas.length == 72);
  assert(canvas[0].point == Point(1,1));
  assert(canvas[71].point == Point(8,9));
}

uint manhattanDistance(in Point lhs, in Point rhs)
pure @safe
{
  // coordinates are unsigned
  import std.algorithm : max, min;
  return
    (max(lhs.x, rhs.x) - min(lhs.x, rhs.x)) +
    (max(lhs.y, rhs.y) - min(lhs.y, rhs.y))
    ;
} unittest {
  assert(9 == manhattanDistance(Point(1,1), Point(8,3)));
  assert(6 == manhattanDistance(Point(8,9), Point(8,3)));
  assert(0 == manhattanDistance(Point(8,3), Point(8,3)));
}

uint part1(in Point[] coordinates)
pure
{
  CanvasCorners corners = makeCanvasCorners(coordinates);
  Canvas canvas = makeCanvas(corners);

  // calculate all distances
  foreach(ref location; canvas) {
    uint[Point] distances;
    foreach(coordinate; coordinates) {
      distances[coordinate] = manhattanDistance(location.point, coordinate);
    }
    // find closest & ties
    import std.algorithm : filter, minElement;
    auto minValue = distances.byKeyValue.minElement!(a => a.value);
    auto allMinValues = distances.byKeyValue.filter!(a => a.value == minValue.value);
    if (allMinValues.array.length == 1) {
      location.closest = allMinValues.array[0].key;
    } // else there is multiple minimum values -> a tie
  }

  // count coordinate area sizes
  uint[Point] coordinateAreaSizes;
  foreach(location; canvas) {
    if (!location.closest.isNull) {
      coordinateAreaSizes[location.closest] += 1;
    }
  }

  // find all finite size areas (simulate set with associative array)
  uint[Point] finiteSizeAreas;
  foreach(point; coordinateAreaSizes.keys) {
    if (point.x != corners.topLeft.x &&
        point.y != corners.topLeft.y &&
        point.x != corners.bottomRight.x &&
        point.y != corners.bottomRight.y) {
      finiteSizeAreas[point] = 1;
    }
  }

  // find the largest finite area size
  uint maxAreaSize;
  foreach(point; finiteSizeAreas.keys) {
    if (maxAreaSize < coordinateAreaSizes[point]) {
      maxAreaSize = coordinateAreaSizes[point];
    }
  }

  return maxAreaSize;
} unittest {
  import std.algorithm : map;
  auto coordinates = coordinateStringsTest.map!(a => makePoint(a)).array;
  assert(17 == part1(coordinates));
}

uint part2(in Point[] coordinates, in uint maxTotalDistance)
pure @safe
{
  CanvasCorners corners = makeCanvasCorners(coordinates);
  Canvas canvas = makeCanvas(corners);

  uint safeRegionSize;

  foreach(location; canvas) {
    uint totalDistance;
    foreach(coordinate; coordinates) {
      totalDistance += manhattanDistance(location.point, coordinate);
      if (totalDistance >= maxTotalDistance) {
        break; // no need to calculate further
      }
    }
    if (totalDistance < maxTotalDistance) {
      safeRegionSize += 1;
    }
  }

  return safeRegionSize;
} unittest {
  import std.algorithm : map;
  auto coordinates = coordinateStringsTest.map!(a => makePoint(a)).array;
  assert(16 == part2(coordinates, 32));
}

void main()
{
  import std.algorithm : map;
  import std.array : split;
  import std.stdio : writefln;
  import std.string : chomp;

  immutable string[] coordinateStrings = import("2018-06").chomp.split("\n");

  immutable coordinates = coordinateStrings.map!(a => makePoint(a)).array;

  {
    auto answer = part1(coordinates);
    writefln("Part1: (answer = %s)", answer);
    assert(answer == 5333);
  }
  {
    auto answer = part2(coordinates, 10000);
    writefln("Part2: (answer = %s)", answer);
    assert(answer == 35334);
  }
}

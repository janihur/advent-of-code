module aoc19day06;

import std.algorithm : countUntil, map;
import std.array : array, split;
import std.stdio : writefln;
import std.string : chomp;

version(unittest) {
  immutable orbitStringsTest1 = ["COM)B",
                                "B)C",
                                "C)D",
                                "D)E",
                                "E)F",
                                "B)G",
                                "G)H",
                                "D)I",
                                "E)J",
                                "J)K",
                                "K)L",];
  immutable orbitStringsTest2 = ["COM)B",
                                "B)C",
                                "C)D",
                                "D)E",
                                "E)F",
                                "B)G",
                                "G)H",
                                "D)I",
                                "E)J",
                                "J)K",
                                "K)L",
                                "K)YOU",
                                "I)SAN",];
}

alias Orbits = string[string];

void countOrbits(ref uint totalOrbits, in Orbits orbits, string orbit)
pure @safe
{
  if (auto o = orbit in orbits) {
    totalOrbits++;
    countOrbits(totalOrbits, orbits, orbits[orbit]);
  }
}

uint part1(in Orbits orbits)
pure
{
  uint totalOrbits = 0;
  foreach(orbit; orbits.keys) {
    countOrbits(totalOrbits, orbits, orbit);
  }
  return totalOrbits;
} unittest {
  Orbits orbits;
  foreach (orbit; orbitStringsTest1) {
    immutable o = orbit.split(")");
    orbits[o[1]] = o[0];
  }
  assert(42 == part1(orbits));
}

void getPath(ref string[] orbitPath, in Orbits orbits, string orbit)
pure
{
  if (auto o = orbit in orbits) {
    orbitPath ~= orbits[orbit];
    getPath(orbitPath, orbits, orbits[orbit]);
  }
}

long part2(in Orbits orbits)
pure
{
  // extract orbit paths for YOU and SAN (Santa)
  string[] you;
  getPath(you, orbits, "YOU");
  debug writefln("(you = %s)", you);
  string[] san;
  getPath(san, orbits, "SAN");
  debug writefln("(san = %s)", san);
  // find the common orbit
  string commonOrbit;
  foreach(orbit; you) {
    if (san.countUntil(orbit) > -1) {
      commonOrbit = orbit;
      break;
    }
  }
  // the number of the required orbital transfers is the same than the index
  // of YOU and SAN path arrays
  return you.countUntil(commonOrbit) + san.countUntil(commonOrbit);
} unittest {
  Orbits orbits;
  foreach (orbit; orbitStringsTest2) {
    immutable o = orbit.split(")");
    orbits[o[1]] = o[0];
  }
  assert(4 == part2(orbits));
}

void main()
{
  // the key is to change the orbit direction
  // from string "COM)B" to associative array B -> COM
  Orbits orbits;
  {
    immutable string[] orbitStrings = import("2019-06").chomp.split("\n");
    foreach (orbit; orbitStrings) {
      immutable o = orbit.split(")");
      orbits[o[1]] = o[0];
    }
  }

  {
    auto answer = part1(orbits);
    writefln("Part1: (answer = %s)", answer);
    assert(answer == 251208);
  }
  {
    auto answer = part2(orbits);
    writefln("Part2: (answer = %s)", answer);
    assert(answer == 397);
  }
}

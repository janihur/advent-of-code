module aoc19day09;

import std.algorithm : map;
import std.array : array;
import std.conv : to;
import std.stdio : writefln;
import std.string : chomp, split;

import intcode19v4;

long part1(immutable long[] program)
{
  auto computer = IntCode19(program, [1]);
  computer.run;
  return computer.output[0];
}

long part2(immutable long[] program)
{
  auto computer = IntCode19(program, [2]);
  computer.run;
  return computer.output[0];
}

void main()
{
  immutable string[] puzzleData = import("2019-09").chomp.split(",");
  immutable long[] program = puzzleData.map!(to!long).array;

  {
    auto answer = part1(program);
    writefln("Part1: (answer = %s)", answer);
    assert(answer == 2350741403);
  }
  {
    auto answer = part2(program);
    writefln("Part2: (answer = %s)", answer);
    assert(answer == 53088);
  }
}

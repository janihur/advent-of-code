module aoc19day10;

import std.algorithm : chunkBy, max, min, reverse, sort;
import std.array : array;
import std.conv : to;
import std.format : format;
import std.math : PI, abs, atan, sqrt, tan;
import std.numeric : gcd;
import std.range : iota;
import std.stdio : writefln;
import std.string : chomp, split;

// https://forum.dlang.org/post/mailman.367.1369848177.13711.digitalmars-d-learn@puremagic.com
T[][] multidup(T)(T[][] x)
{
  T[][] tmp;
  foreach(row; x) {
    tmp ~= row.dup;
  }
  return tmp;
}

struct Coordinate {
  int x;
  int y;

  string toString() {
    return format("(%s, %s)", x, y);
  }
}

struct Asteroid {
  Coordinate pos;
  int quadrant;
  float angle;
  float magnitude;
  int destroyed;

  this(Coordinate origo, Coordinate pos) {
    this.pos = pos;
    this.quadrant = quadrantImpl(origo, pos);
    this.angle = angleImpl(origo, pos);
    this.magnitude = magnitudeImpl(origo, pos);
    this.destroyed = 0;
  }
  
  string toString() {
    return format("(pos = %s)(quadrant = %s)(angle = %s)(magnitude = %s)(destroyed = %s)", this.pos, this.quadrant, this.angle, this.magnitude, this.destroyed);
  }

  // int opCmp(Asteroid rhs) const {
  //   if (this.magnitude < rhs.magnitude) return -1;
  //   if (this.magnitude > rhs.magnitude) return  1;
  //   if (this.magnitude == rhs.magnitude) {
  //     if (this.angle < rhs.angle) return -1;
  //     if (this.angle > rhs.angle) return  1;
  //   }
  //   return 0;
  // }

  int opCmp(Asteroid rhs) const {
    if (this.angle < rhs.angle) return -1;
    if (this.angle > rhs.angle) return  1;
    if (this.angle == rhs.angle) {
      if (this.magnitude < rhs.magnitude) return -1;
      if (this.magnitude > rhs.magnitude) return  1;
    }
    return 0;
  }
  
  int quadrantImpl(Coordinate a, Coordinate b) {
    if (a == b) { return 0; }
    if (a.x <= b.x && a.y >  b.y) { return 1; }
    if (a.x <  b.x && a.y <= b.y) { return 2; }
    if (a.x >= b.x && a.y <  b.y) { return 3; }
    if (a.x >  b.x && a.y >= b.y) { return 4; }
    assert(0, format("(a = %s)(b = %s)", a, b));
  }
  
  float angleImpl(Coordinate a, Coordinate b) {
    auto c = diff(a, b);
    switch (this.quadrant) {
    case 0:
      return 0;
      // break;
    case 1:
      if (c.x == 0) return 0;
      return 90 - atan(abs(cast(float)c.y / cast(float)c.x)) * (180 / PI);
      // break;
    case 2:
      return 90 + atan(abs(cast(float)c.y / cast(float)c.x)) * (180 / PI);
      // break;
    case 3:
      if (c.x == 0) return 180;
      return 180 + 90 - atan(abs(cast(float)c.y / cast(float)c.x)) * (180 / PI);
      // break;
    case 4:
      return 270 + atan(abs(cast(float)c.y / cast(float)c.x)) * (180 / PI);
      // break;
    default:
      assert(0, format("(a = %s)(b = %s)", a, b));
    }
  }
  
  // float slopeImpl(Coordinate a, Coordinate b) {
  //   // https://stackoverflow.com/a/16544330/272735
  //   // dot = x1*x2 + y1*y2     # dot product between [x1, y1] and [x2, y2]
  //   // det = x1*y2 - y1*x2     # determinant
  //   // angle = atan2(det, dot)
  //   auto dot = a.x * b.x + a.y * b.y;
  //   auto det = a.x * b.y - a.y * b.x;
  //   import std.math : atan2;
  //   return atan2(cast(float)det, cast(float)dot);
  // }

  float magnitudeImpl(Coordinate a, Coordinate b) {
    auto c = diff(a, b);
    return sqrt(cast(float)c.x ^^ 2 + cast(float)c.y ^^ 2);
  }
}
// unittest {
//   auto a = diff(Coordinate(8,3), Coordinate(9,2));
//   writefln("%s", a);
//   auto b = abs(cast(float)a.y / cast(float)a.x);
//   writefln("%s", b);
//   auto c = tan(PI/4); // 45 deg -> II/4 -> 1
//   writefln("%s", c);
//   auto d = atan(cast(float)1);
//   writefln("%s", d);
// }
unittest {
  auto origo = Coordinate(8,3);
  assert(Asteroid(origo, origo).quadrant == 0);
  // runs counter-clockwise
  // 1st circle around origo
  assert(Asteroid(origo, Coordinate(8,2)).quadrant == 1);
  assert(Asteroid(origo, Coordinate(9,2)).quadrant == 1);
  assert(Asteroid(origo, Coordinate(9,3)).quadrant == 2);
  assert(Asteroid(origo, Coordinate(9,4)).quadrant == 2);
  assert(Asteroid(origo, Coordinate(8,4)).quadrant == 3);
  assert(Asteroid(origo, Coordinate(7,4)).quadrant == 3);
  assert(Asteroid(origo, Coordinate(7,3)).quadrant == 4);
  assert(Asteroid(origo, Coordinate(7,2)).quadrant == 4);
  // 2nd circle around origo
  assert(Asteroid(origo, Coordinate(8,1)).quadrant == 1);
  assert(Asteroid(origo, Coordinate(9,1)).quadrant == 1);
  assert(Asteroid(origo, Coordinate(10,1)).quadrant == 1);
  assert(Asteroid(origo, Coordinate(10,2)).quadrant == 1);
  assert(Asteroid(origo, Coordinate(10,3)).quadrant == 2);
  assert(Asteroid(origo, Coordinate(10,4)).quadrant == 2);
  assert(Asteroid(origo, Coordinate(10,5)).quadrant == 2);
  assert(Asteroid(origo, Coordinate(9,5)).quadrant == 2);
  assert(Asteroid(origo, Coordinate(8,5)).quadrant == 3);
  assert(Asteroid(origo, Coordinate(7,5)).quadrant == 3);
  assert(Asteroid(origo, Coordinate(6,5)).quadrant == 3);
  assert(Asteroid(origo, Coordinate(6,4)).quadrant == 3);
  assert(Asteroid(origo, Coordinate(6,3)).quadrant == 4);
  assert(Asteroid(origo, Coordinate(6,2)).quadrant == 4);
  assert(Asteroid(origo, Coordinate(6,1)).quadrant == 4);
  assert(Asteroid(origo, Coordinate(7,1)).quadrant == 4);
}
unittest {
  auto origo = Coordinate(8,3);
  Asteroid[] asteroids = [// 1st circle around origo
                          Asteroid(origo, Coordinate(8,2)),
                          Asteroid(origo, Coordinate(9,2)),
                          Asteroid(origo, Coordinate(9,3)),
                          Asteroid(origo, Coordinate(9,4)),
                          Asteroid(origo, Coordinate(8,4)),
                          Asteroid(origo, Coordinate(7,4)),
                          Asteroid(origo, Coordinate(7,3)),
                          Asteroid(origo, Coordinate(7,2)),
                          // 2nd circle around origo                          
                          Asteroid(origo, Coordinate(8,1)),
                          Asteroid(origo, Coordinate(9,1)),
                          Asteroid(origo, Coordinate(10,1)),
                          Asteroid(origo, Coordinate(10,2)),
                          Asteroid(origo, Coordinate(10,3)),
                          Asteroid(origo, Coordinate(10,4)),
                          Asteroid(origo, Coordinate(10,5)),
                          Asteroid(origo, Coordinate(9,5)),
                          Asteroid(origo, Coordinate(8,5)),
                          Asteroid(origo, Coordinate(7,5)),
                          Asteroid(origo, Coordinate(6,5)),
                          Asteroid(origo, Coordinate(6,4)),
                          Asteroid(origo, Coordinate(6,3)),
                          Asteroid(origo, Coordinate(6,2)),
                          Asteroid(origo, Coordinate(6,1)),
                          Asteroid(origo, Coordinate(7,1)),
                          ];

  auto sorted = sort(asteroids).chunkBy!((a,b) => a.angle == b.angle).array;
  int destroyed = 1;
  for (auto i = 0; i < 3; i++) {
    foreach (ref a; sorted) {
      foreach (ref b; a) {
        if (b.destroyed == 0) {
          b.destroyed = destroyed;
          destroyed += 1;
          //writefln("Asteroid: %s", b);
          break;
        }
      }
    }
  }
}

Coordinate diff(Coordinate a, Coordinate b) {
  auto x = a.x - b.x;
  auto y = a.y - b.y;
  return Coordinate(x, y);
}
unittest {
  assert(Coordinate(-6, -4) == diff(Coordinate(0, 0), Coordinate(6, 4)));
  assert(Coordinate(-3, 6) == diff(Coordinate(3, 6), Coordinate(6, 0)));
}

Coordinate add(Coordinate a, Coordinate b) {
  auto x = a.x + (-1 * b.x);
  auto y = a.y + (-1 * b.y);
  return Coordinate(x, y);
}
unittest {
  assert(Coordinate(4,4) == add(Coordinate(3,6), Coordinate(-1,2)));
  assert(Coordinate(5,2) == add(Coordinate(4,4), Coordinate(-1,2)));
  assert(Coordinate(6,0) == add(Coordinate(5,2), Coordinate(-1,2)));

  assert(Coordinate(5,7) == add(Coordinate(3,6), Coordinate(-2,-1)));
  assert(Coordinate(7,8) == add(Coordinate(5,7), Coordinate(-2,-1)));
}

Coordinate minStep(Coordinate a) {
  if (a.x == 0 || a.y == 0) { return a; }
  auto d = gcd(abs(a.x), abs(a.y));
  auto x = a.x / d;
  auto y = a.y / d;
  return Coordinate(x, y);
}
unittest {
  assert(Coordinate(3, 2) == minStep(Coordinate(15, 10)));
  assert(Coordinate(-3, -2) == minStep(Coordinate(-6, -4)));
  assert(Coordinate(7, 3) == minStep(Coordinate(7, 3)));
  assert(Coordinate(0, 3) == minStep(Coordinate(0, 3)));
  assert(Coordinate(3, 0) == minStep(Coordinate(3, 0)));
}

struct MapCell {
  bool asteroid;
  bool blocked;
}

alias AsteroidsMap = MapCell[][];

void print(AsteroidsMap map) {
  foreach(yIdx, yLine; map) {
    auto line = "";
    foreach(xIdx, mapCell; yLine) {
      line ~= mapCell.asteroid ? (mapCell.blocked ? "X" : "#") : ".";
    }
    // writefln("%s", line);
  }
}

AsteroidsMap toMap(immutable string[] strMap)
{
  const auto yLength = strMap.length;
  const auto xLength = strMap[0].length;
  AsteroidsMap map = new AsteroidsMap(yLength, xLength);
  
  foreach(yIdx, yLine; strMap) {
    foreach(xIdx, xChar; yLine) {
      map[yIdx][xIdx].asteroid = (xChar == '#');
    }
  }

  return map;
}
unittest {
  immutable string[] strMap = [".#..#",
                               ".....",
                               "#####",
                               "....#",
                               "...##",];
  auto asteroidsMap = toMap(strMap);
  // writefln("(asteroidsMap = %s)", asteroidsMap);
}

AsteroidsMap calculateVisibilityMap(Coordinate pos, AsteroidsMap asteroidsMap)
{
  AsteroidsMap visibilityMap = asteroidsMap.multidup;
  auto mapDimension = Coordinate(to!int(visibilityMap[0].length),
                                 to!int(visibilityMap.length));
  // writefln("(mapDimension = %s)", mapDimension);
  // writefln("(pos = %s)", pos);
  
  // RIGHT
  {
    bool first = false;
    foreach (x; pos.x+1 .. mapDimension.x) {
      if (visibilityMap[pos.y][x].asteroid) {
        if (!first) {
          first = true;
        } else {
          visibilityMap[pos.y][x].blocked = true;
        }
      }
    }
  }
  // diagonal RIGHT-DOWN
  {
    bool first = false;
    auto x = pos.x+1;
    auto y = pos.y+1;
    while (x < mapDimension.x && y < mapDimension.y) {
      if (visibilityMap[y][x].asteroid) {
        if (!first) {
          first = true;
        } else {
          visibilityMap[y][x].blocked = true;
        }
      }
      x += 1;
      y += 1;
    }
  }
  // DOWN
  {
    bool first = false;
    foreach (y; pos.y+1 .. mapDimension.y) {
      if (visibilityMap[y][pos.x].asteroid) {
        if (!first) {
          first = true;
        } else {
          visibilityMap[y][pos.x].blocked = true;
        }
      }
    }
  }
  // diagonal DOWN-LEFT
  {
    bool first = false;
    auto x = pos.x-1;
    auto y = pos.y+1;
    while (x >= 0 && x < mapDimension.x &&
           y < mapDimension.y) {
      if (visibilityMap[y][x].asteroid) {
        if (!first) {
          first = true;
        } else {
          visibilityMap[y][x].blocked = true;
        }
      }
      x -= 1;
      y += 1;
    }
  }
  // LEFT
  {
    bool first = false;
    foreach (x; iota(0, pos.x).array.reverse) {
      if (visibilityMap[pos.y][x].asteroid) {
        if (!first) {
          first = true;
        } else {
          visibilityMap[pos.y][x].blocked = true;
        }
      }
    }
  }
  // diagonal LEFT-UP
  {
    bool first = false;
    auto x = pos.x-1;
    auto y = pos.y-1;
    while (x >= 0 && x < mapDimension.x &&
           y >= 0 && y < mapDimension.y) {
      if (visibilityMap[y][x].asteroid) {
        if (!first) {
          first = true;
        } else {
          visibilityMap[y][x].blocked = true;
        }
      }
      x -= 1;
      y -= 1;
    }
  }
  // UP
  {
    bool first = false;
    foreach (y; iota(0, pos.y).array.reverse) {
      if (visibilityMap[y][pos.x].asteroid) {
        if (!first) {
          first = true;
        } else {
          visibilityMap[y][pos.x].blocked = true;
        }
      }
    }
  }
  // diagonal UP-RIGHT
  {
    bool first = false;
    auto x = pos.x+1;
    auto y = pos.y-1;
    while (x < mapDimension.x &&
           y >= 0 && y < mapDimension.y) {
      if (visibilityMap[y][x].asteroid) {
        if (!first) {
          first = true;
        } else {
          visibilityMap[y][x].blocked = true;
        }
      }
      x += 1;
      y -= 1;
    }
  }

  // calculate all other angles if not already blocked
  {
    foreach (y; 0 .. mapDimension.y) {
      foreach (x; 0 .. mapDimension.x) {
        // find non-blocked asteroids
        if (visibilityMap[y][x].asteroid && !visibilityMap[y][x].blocked) {
          // calculate "distance" and minimum step
          auto pos2 = Coordinate(x, y);
          auto d1 = diff(pos, pos2);
          auto d2 = minStep(d1);
          debug writefln("(pos = %s)(pos2 = %s)(d1 = %s)(d2 = %s)", pos, pos2, d1, d2);
          // skip when no difference
          if (d2 == Coordinate(0,0)) { continue; }
          {
            bool first = false;
            auto pos3 = add(pos, d2);
            while (pos3.x >= 0 && pos3.x < mapDimension.x &&
                   pos3.y >= 0 && pos3.y < mapDimension.y) {
              if (visibilityMap[pos3.y][pos3.x].asteroid) {
                if (!first) {
                  first = true;
                } else {
                  visibilityMap[pos3.y][pos3.x].blocked = true;
                }
              }
              pos3 = add(pos3, d2);
            }
          }
        }
      }
    }
  }

  return visibilityMap;
}

uint calculateVisibleAsteroids(AsteroidsMap map) {
  uint visibleAsteroids;
  const yMax = to!int(map.length);
  const xMax = to!int(map[0].length);

  foreach(y; 0 .. yMax ) {
    foreach(x; 0 .. xMax) {
      if (map[y][x].asteroid && !map[y][x].blocked) {
        visibleAsteroids += 1;
      }
    }
  }

  // ignore self
  return visibleAsteroids - 1;
}

struct Max {
  uint val;
  Coordinate pos;
}

Max part1(immutable string[] strMap)
{
  AsteroidsMap map = toMap(strMap);
  const yMax = to!int(map.length);
  const xMax = to!int(map[0].length);

  Max max;
  
  foreach(y; 0 .. yMax ) {
    foreach(x; 0 .. xMax) {
      if (map[y][x].asteroid) {
        auto pos = Coordinate(x, y);
        // writefln("--- asteroid: %s", pos);
        auto visiMap = calculateVisibilityMap(pos, map);
        // print(visiMap);
        auto visible = calculateVisibleAsteroids(visiMap);
        // writefln("(visible = %s)", visible);
        if (visible > max.val) {
          max.val = visible;
          max.pos = pos;
        }
      }
    }
  }
  return max;
}
unittest {
  immutable string[] asteroidsMap = [".#..#",
                                     ".....",
                                     "#####",
                                     "....#",
                                     "...##",];
  auto c = part1(asteroidsMap);
  assert(Max(8,Coordinate(3,4)) == c);
}
unittest {
  immutable string[] asteroidsMap = ["......#.#.",
                                     "#..#.#....",
                                     "..#######.",
                                     ".#.#.###..",
                                     ".#..#.....",
                                     "..#....#.#",
                                     "#..#....#.",
                                     ".##.#..###",
                                     "##...#..#.",
                                     ".#....####",];
  auto c = part1(asteroidsMap);
  assert(Max(33, Coordinate(5,8)) == c);
}
unittest {
  immutable string[] asteroidsMap = ["#.#...#.#.",
                                     ".###....#.",
                                     ".#....#...",
                                     "##.#.#.#.#",
                                     "....#.#.#.",
                                     ".##..###.#",
                                     "..#...##..",
                                     "..##....##",
                                     "......#...",
                                     ".####.###.",];
  auto c = part1(asteroidsMap);
  assert(Max(35, Coordinate(1,2)) == c);
}
unittest {
  immutable string[] asteroidsMap = [".#..#..###",
                                     "####.###.#",
                                     "....###.#.",
                                     "..###.##.#",
                                     "##.##.#.#.",
                                     "....###..#",
                                     "..#.#..#.#",
                                     "#..#.#.###",
                                     ".##...##.#",
                                     ".....#.#..",];
  auto c = part1(asteroidsMap);
  assert(Max(41, Coordinate(6,3)) == c);
}
// unittest {
//   immutable string[] asteroidsMap = [".#..##.###...#######",
//                                      "##.############..##.",
//                                      ".#.######.########.#",
//                                      ".###.#######.####.#.",
//                                      "#####.##.#.##.###.##",
//                                      "..#####..#.#########",
//                                      "####################",
//                                      "#.####....###.#.#.##",
//                                      "##.#################",
//                                      "#####.##.###..####..",
//                                      "..######..##.#######",
//                                      "####.##.####...##..#",
//                                      ".#####..#.######.###",
//                                      "##...#.##########...",
//                                      "#.##########.#######",
//                                      ".####.#.###.###.#.##",
//                                      "....##.##.###..#####",
//                                      ".#.#.###########.###",
//                                      "#.#.#.#####.####.###",
//                                      "###.##.####.##.#..##",];
//   auto c = part1(asteroidsMap);
//   assert(Max(210, Coordinate(11,13)) == c);
// }

// PART 2 ----------------------------------------------------------------------
// https://www.mathsisfun.com/algebra/vectors.html

float calculateSlope(Coordinate pos1, Coordinate pos2) {
  // https://stackoverflow.com/a/16544330/272735
  // dot = x1*x2 + y1*y2     # dot product between [x1, y1] and [x2, y2]
  // det = x1*y2 - y1*x2     # determinant
  // angle = atan2(det, dot)
  auto dot = pos1.x * pos2.x + pos1.y * pos2.y;
  auto det = pos1.x * pos2.y - pos1.y * pos2.x;
  import std.math : atan2;
  return atan2(cast(float)det, cast(float)dot);
}
unittest {
  // writefln("calculateSlope: %s",
  calculateSlope(Coordinate(8,3), Coordinate(8,0));
  calculateSlope(Coordinate(8,3), Coordinate(8,1));
  calculateSlope(Coordinate(8,3), Coordinate(8,2));
  calculateSlope(Coordinate(8,3), Coordinate(8,3));

  calculateSlope(Coordinate(8,3), Coordinate(9,0));
  calculateSlope(Coordinate(8,3), Coordinate(9,1));

  calculateSlope(Coordinate(0,-3), Coordinate(0,3));
}

Asteroid part2(immutable string[] strMap, Coordinate stationLocation, int searchIndex)
{
  Asteroid[] asteroids;
  foreach(yIdx, yLine; strMap) {
    foreach(xIdx, xChar; yLine) {
      if (xChar == '#') {
        if (stationLocation.x == xIdx &&
            stationLocation.y == yIdx) {
        } else {
          asteroids ~= Asteroid(stationLocation,
                                Coordinate(to!int(xIdx), to!int(yIdx)));
        }
      }
    }
  }
  auto sorted = sort(asteroids).chunkBy!((a,b) => a.angle == b.angle).array;
  int destroyed = 1;
  while (destroyed < asteroids.length + 1) {
    foreach (ref a; sorted) {
      foreach (ref b; a) {
        if (b.destroyed == 0) {
          b.destroyed = destroyed;
          destroyed += 1;
          // writefln("Asteroid: %s", b);
          if (b.destroyed == searchIndex) {
            return b;
          }
          break;
        }
      }
    }
  }
  return Asteroid(Coordinate(0,0), Coordinate(0,0));
}
unittest {
  // monitoring station location: 8, 3
  // immutable string[] asteroidsMap = [".#....#####...#..",
  //                                    "##...##.#####..##",
  //                                    "##...#...#.#####.",
  //                                    "..#.....#...###..",
  //                                    "..#.#.....#....##",];
  immutable string[] asteroidsMap = [".#..##.###...#######",
                                     "##.############..##.",
                                     ".#.######.########.#",
                                     ".###.#######.####.#.",
                                     "#####.##.#.##.###.##",
                                     "..#####..#.#########",
                                     "####################",
                                     "#.####....###.#.#.##",
                                     "##.#################",
                                     "#####.##.###..####..",
                                     "..######..##.#######",
                                     "####.##.####...##..#",
                                     ".#####..#.######.###",
                                     "##...#.##########...",
                                     "#.##########.#######",
                                     ".####.#.###.###.#.##",
                                     "....##.##.###..#####",
                                     ".#.#.###########.###",
                                     "#.#.#.#####.####.###",
                                     "###.##.####.##.#..##",];

  part2(asteroidsMap, Coordinate(11,13), -1);
}

void main()
{
  immutable string[] puzzleData = import("2019-10").chomp.split("\n");
  // writefln("(puzzleData = %s)", puzzleData);

  {
    auto answer = part1(puzzleData);
    writefln("Part1: (answer = %s)", answer.val);
    writefln("Part1: (answer = %s)", answer);
    assert(answer.val == 347);
  }
  {
    auto asteroid = part2(puzzleData, Coordinate(26, 36), 200);
    writefln("Part2: (asteroid = %s)", asteroid);
    auto answer = asteroid.pos.x * 100 + asteroid.pos.y;
    writefln("Part2: (answer = %s)", answer);
    assert(answer == 829);
  }
}

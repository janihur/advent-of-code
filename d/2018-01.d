module aoc2018day1;

version(unittest) {
  import std.conv: to;
}

int part1Loop(int[] numbers)
pure
{
  int sum = 0;
  foreach (n; numbers) {
    sum += n;
  }
  return sum;
} unittest {
  int answer = part1Loop([1, -2, 3, 1]);
  assert(answer == 3, "Got: " ~ to!string(answer));
}

int part1Algo(int[] numbers)
pure
{
  import std.algorithm: sum;
  return numbers.sum;
} unittest {
  int answer = part1Algo([1, -2, 3, 1]);
  assert(answer == 3, "Got: " ~ to!string(answer));
}

int part2Loop(int[] numbers)
pure
{
  int[int] alreadySeen = [0:1];
  int accu = 0;

 out_: while(true) {
    foreach(n; numbers) {
      accu += n;
      if (int* p = accu in alreadySeen) {
        break out_;
      } else {
        alreadySeen[accu] = 1;
      }
    }
  }

  return accu;
} unittest {
  int answer = 0;

  answer = part2Loop([1, -2, 3, 1]);
  assert(answer == 2, "Got: " ~ to!string(answer));

  answer = part2Loop([1, -1]);
  assert(answer == 0, "Got: " ~ to!string(answer));

  answer = part2Loop([3, 3, 4, -2, -4]);
  assert(answer == 10, "Got: " ~ to!string(answer));

  answer = part2Loop([-6, 3, 8, 5, -6]);
  assert(answer == 5, "Got: " ~ to!string(answer));

  answer = part2Loop([7, 7, -2, -7, -4]);
  assert(answer == 14, "Got: " ~ to!string(answer));
}

int part2Algo(int[] numbers)
pure
{
  import std.range: cycle;
  
  int[int] alreadySeen = [0:1];
  int accu = 0;

  // reduce can't be used as there's no early exit option.
  // see: https://stackoverflow.com/q/53700781/272735
  foreach(n; numbers.cycle) {
    accu += n;
    if (int* p = accu in alreadySeen) {
      break;
    } else {
      alreadySeen[accu] = 1;
    }
  }

  return accu;
} unittest {
  int answer = 0;

  answer = part2Algo([1, -2, 3, 1]);
  assert(answer == 2, "Got: " ~ to!string(answer));

  answer = part2Algo([1, -1]);
  assert(answer == 0, "Got: " ~ to!string(answer));

  answer = part2Algo([3, 3, 4, -2, -4]);
  assert(answer == 10, "Got: " ~ to!string(answer));

  answer = part2Algo([-6, 3, 8, 5, -6]);
  assert(answer == 5, "Got: " ~ to!string(answer));

  answer = part2Algo([7, 7, -2, -7, -4]);
  assert(answer == 14, "Got: " ~ to!string(answer));
}

void main()
{
  import aoc2018: getIntegers;
  import std.stdio: writefln;
  
  {
    int answer = part1Loop(getIntegers("../inputs/2018-01"));
    writefln("Part 1 (loop): (answer = %d)", answer);
    assert(answer == 466);
  }
  {
    int answer = part1Algo(getIntegers("../inputs/2018-01"));
    writefln("Part 1 (algo): (answer = %d)", answer);
    assert(answer == 466);
  }
  {
    int answer = part2Loop(getIntegers("../inputs/2018-01"));
    writefln("Part 2 (loop): (answer = %d)", answer);
    assert(answer == 750);
  }
  {
    int answer = part2Algo(getIntegers("../inputs/2018-01"));
    writefln("Part 2 (algo): (answer = %d)", answer);
    assert(answer == 750);
  }
}

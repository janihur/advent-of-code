module intcode19v2;

import std.algorithm : reverse;
import std.range : dropOne;
import std.stdio : writefln;

enum OpCode : uint {
  ADD = 1,
  MULTIPLY = 2,
  SAVE = 3,
  OUT = 4,
  JUMP_IF_TRUE = 5,
  JUMP_IF_FALSE  = 6,
  LESS_THAN  = 7,
  EQUALS = 8,
  HALT = 99,
}

enum ParamMode : uint {
  POSITION = 0,
  IMMEDIATE = 1,
}

int[] splitOpCode(int opcode) {
  int[] opcodes;
  opcodes ~= opcode / 10000;
  opcodes ~= (opcode % 10000) / 1000;
  opcodes ~= ((opcode % 10000) % 1000) / 100;
  opcodes ~= ((opcode % 10000) % 1000) % 100;
  return opcodes.reverse;
}

unittest {
  assert(splitOpCode(54321) == [21, 3, 4, 5]);
  assert(splitOpCode( 4321) == [21, 3, 4, 0]);
  assert(splitOpCode(  321) == [21, 3, 0, 0]);
  assert(splitOpCode(   21) == [21, 0, 0, 0]);
  assert(splitOpCode(    1) == [ 1, 0, 0, 0]);
  assert(splitOpCode(   99) == [99, 0, 0, 0]);
}

struct IntCode19 {
  int[] memory;
  int pc; // program counter
  int[] output;
  int[] input;

  @disable this();
  
  this(immutable int[] program, int[] input) {
    this.memory = program.dup;
    this.input = input.dup;
  }

  this(immutable int[] program) {
    this.memory = program.dup;
  }

  void setNoun(int noun) {
    this.memory[1] = noun;
  }

  void setVerb(int verb) {
    this.memory[2] = verb;
  }

  ref int at(int pos, int mode) {
    switch (mode) {
    case ParamMode.POSITION:
      return memory[memory[pos]];
    case ParamMode.IMMEDIATE:
      return memory[pos];
    default:
      assert(0);
    }
  }
  
  void run() {
  out_: while (true) {
      debug writefln("(pc = %s)(memory[pc] = %s)", pc, memory[pc]);
      auto opcode = memory[pc].splitOpCode;
      switch (opcode[0]) {
      case OpCode.ADD:
        int op1 = at(pc+1, opcode[1]);
        int op2 = at(pc+2, opcode[2]);
        at(pc+3, opcode[3]) = op1 + op2;
        pc += 4;
        break;
      case OpCode.MULTIPLY:
        int op1 = at(pc+1, opcode[1]);
        int op2 = at(pc+2, opcode[2]);
        at(pc+3, opcode[3]) = op1 * op2;
        pc += 4;
        break;
      case OpCode.SAVE:
        at(pc+1, opcode[1]) = input[0]; input = input.dropOne;
        pc += 2;
        break;
      case OpCode.OUT:
        output ~= at(pc+1, opcode[1]);
        pc += 2;
        break;
      case OpCode.JUMP_IF_TRUE:
        int op1 = at(pc+1, opcode[1]);
        int op2 = at(pc+2, opcode[2]);
        if (op1 != 0) {
          pc = op2;
        } else {
          pc += 3;
        }
        break;
      case OpCode.JUMP_IF_FALSE:
        int op1 = at(pc+1, opcode[1]);
        int op2 = at(pc+2, opcode[2]);
        if (op1 == 0) {
          pc = op2;
        } else {
          pc += 3;
        }
        break;
      case OpCode.LESS_THAN:
        int op1 = at(pc+1, opcode[1]);
        int op2 = at(pc+2, opcode[2]);
        if (op1 < op2) {
          at(pc+3, opcode[3]) = 1;
        } else {
          at(pc+3, opcode[3]) = 0;
        }
        pc += 4;
        break;
      case OpCode.EQUALS:
        int op1 = at(pc+1, opcode[1]);
        int op2 = at(pc+2, opcode[2]);
        if (op1 == op2) {
          at(pc+3, opcode[3]) = 1;
        } else {
          at(pc+3, opcode[3]) = 0;
        }
        pc += 4;
        break;
      case OpCode.HALT:
        break out_;
      default:
        writefln("unknown opcode: (opcode = %s)(memory = %s)", opcode, memory);
        assert(0);
      }
    }
  }
}

// version 1 tests (DAY02) -----------------------------------------------------

unittest {
  auto computer = IntCode19([1,0,0,0,99]);
  computer.run;
  assert(computer.memory == [2,0,0,0,99]);
}
unittest {
  auto computer = IntCode19([2,3,0,3,99]);
  computer.run;
  assert(computer.memory == [2,3,0,6,99]);
}
unittest {
  auto computer = IntCode19([2,4,4,5,99,0]);
  computer.run;
  assert(computer.memory == [2,4,4,5,99,9801]);
}
unittest {
  auto computer = IntCode19([1,1,1,4,99,5,6,0,99]);
  computer.run;
  assert(computer.memory == [30,1,1,4,2,5,6,0,99]);
}
unittest {
  auto computer = IntCode19([1,9,10,3,2,3,11,0,99,30,40,50]);
  computer.run;
  assert(computer.memory == [3500,9,10,70,2,3,11,0,99,30,40,50]);
}

// version 2 tests (DAY05) -----------------------------------------------------

unittest {
  auto computer = IntCode19([1002,4,3,4,33]);
  computer.run;
  assert(computer.memory == [1002,4,3,4,99]);
}
unittest {
  auto computer = IntCode19([1101,100,-1,4,0]);
  computer.run;
  assert(computer.memory == [1101,100,-1,4,99]);
}
unittest {
  auto computer = IntCode19([3,9,8,9,10,9,4,9,99,-1,8], [8]);
  computer.run;
  assert(computer.output == [1]);
}
unittest {
  auto computer = IntCode19([3,9,7,9,10,9,4,9,99,-1,8], [8]);
  computer.run;
  assert(computer.output == [0]);
}
unittest {
  auto computer = IntCode19([3,3,1108,-1,8,3,4,3,99], [8]);
  computer.run;
  assert(computer.output == [1]);
}
unittest {
  auto computer = IntCode19([3,3,1107,-1,8,3,4,3,99], [8]);
  computer.run;
  assert(computer.output == [0]);
}
unittest {
  import std.typecons : Tuple, tuple;
  alias T = Tuple!(int[], "input", int[], "expected");

  T[3] tests;
  tests[0].input = [7];
  tests[0].expected = [999];

  tests[1].input = [8];
  tests[1].expected = [1000];

  tests[2].input = [9];
  tests[2].expected = [1001];

  foreach (test; tests) {
    auto computer = IntCode19([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], test.input);
    computer.run;
    assert(computer.output == test.expected);
  }
}

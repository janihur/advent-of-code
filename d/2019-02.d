module aoc19day02;

import std.algorithm : map;
import std.array : array;
import std.conv : to;
import std.range : iota;
import std.stdio : writefln;
import std.string : chomp, split;

import intcode19;

int part1(immutable int[] program, int noun, int verb)
{
  auto computer = IntCode19(program);
  computer.setNoun(noun);
  computer.setVerb(verb);
  computer.run;
  debug writefln("(computer.memory = %s)", computer.memory);
  return computer.memory[0];
}

int part2(immutable int[] program, int expected)
{
  foreach (noun; iota(0, 99 + 1)) {
    foreach (verb; iota(0, 99 + 1)) {
      auto computer = IntCode19(program);
      computer.setNoun(noun);
      computer.setVerb(verb);
      computer.run;
      if (computer.memory[0] == expected) {
        return (100 * noun) + verb;
      }
    }
  }
  assert(0); // should not happen
}

void main()
{
  immutable string[] puzzleData = import("2019-02").chomp.split(",");
  immutable int[] program = puzzleData.map!(to!int).array;
  // writefln("(program = %s)", program);

  {
    auto answer = part1(program, 12, 2);
    writefln("Part1: (answer = %s)", answer);
    assert(answer == 4570637);
  }
  {
    auto answer = part2(program, 19690720);
    writefln("Part2: (answer = %s)", answer);
    assert(answer == 5485);
  }
}

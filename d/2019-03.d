module aoc19day03;

import std.algorithm : max, min, reverse, setIntersection, sort;
import std.array : array;
import std.conv : to;
import std.range : iota;
import std.stdio : writefln;
import std.string : chomp, split;

struct Point {
  int x;
  int y;
  uint step;

  int opCmp(ref const Point rhs) const {
    if (x == rhs.x && y == rhs.y) return 0;
    if (x == rhs.x) return y > rhs.y ? 1 : -1;
    return x > rhs.x ? 1 : -1;
  }
  bool opEquals(ref const typeof(this) rhs) const @safe pure nothrow {
    if (x == rhs.x && y == rhs.y) return true;
    return false;
  }
  size_t toHash() const @safe pure nothrow {
    return (x * 10000) + y;
  }
}

alias Wire = Point[];

uint manhattanDistance(in Point lhs, in Point rhs)
pure @safe
{
  return
    (max(lhs.x, rhs.x) - min(lhs.x, rhs.x)) +
    (max(lhs.y, rhs.y) - min(lhs.y, rhs.y))
    ;
} unittest {
  assert(9 == manhattanDistance(Point(1,1), Point(8,3)));
  assert(6 == manhattanDistance(Point(8,9), Point(8,3)));
  assert(0 == manhattanDistance(Point(8,3), Point(8,3)));
}

Wire moveToWireSegment(Point from, string move, uint initialStep)
pure
{
  Wire segment;
  auto direction = move[0];
  auto length = to!int(move[1..$]);

  uint steps = initialStep;
  switch (direction) {
  case 'R':
    foreach(x; iota(from.x + 1, from.x + length + 1)) {
      segment ~= Point(x, from.y, steps);
      steps += 1;
    }
    break;
  case 'D':
    foreach(y; iota(from.y - length, from.y).array.reverse) {
      segment ~= Point(from.x, y, steps);
      steps += 1;
    }
    break;
  case 'L':
    foreach(x; iota(from.x - length, from.x).array.reverse) {
      segment ~= Point(x, from.y, steps);
      steps += 1;
    }    
    break;
  case 'U':
    foreach(y; iota(from.y + 1, from.y + length + 1)) {
      segment ~= Point(from.x, y, steps);
      steps += 1;
    }
    break;
  default:
    assert(0, "Unknown direction");
  }
  return segment;
}

uint part1(string[][] moves)
{
  auto segmentStartingPoint = Point(0,0);

  Wire w1;
  foreach(move; moves[0]) {
    w1 ~= moveToWireSegment(segmentStartingPoint, move, 0);
    segmentStartingPoint = w1[$-1];
  }

  segmentStartingPoint = Point(0,0);
  
  Wire w2;
  foreach(move; moves[1]) {
    w2 ~= moveToWireSegment(segmentStartingPoint, move, 0);
    segmentStartingPoint = w2[$-1];
  }

  uint minDistance = uint.max;
  foreach (intersection; setIntersection(w1.sort, w2.sort)) {
    minDistance = min(minDistance, manhattanDistance(Point(0,0), intersection));
  }

  return minDistance;
}
unittest {
  auto moves = [["R8","U5","L5","D3"], ["U7","R6","D4","L4"]];
  assert(6 == part1(moves));
}
unittest {
  auto moves = [["R75","D30","R83","U83","L12","D49","R71","U7","L72"],
                ["U62","R66","U55","R34","D71","R55","D58","R83"]];
  assert(159 == part1(moves));
}
unittest {
  auto moves = [["R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"],
                ["U98","R91","D20","R16","D67","R40","U7","R15","U6","R7"]];
  assert(135 == part1(moves));
}

uint part2(string[][] moves)
{
  // wire 1 ---
  auto segmentStartingPoint = Point(0,0);
  uint initialStep = 1;
  
  Wire w1;
  foreach(move; moves[0]) {
    w1 ~= moveToWireSegment(segmentStartingPoint, move, initialStep);
    segmentStartingPoint = w1[$-1];
    initialStep = segmentStartingPoint.step + 1;
  }
  uint[Point] ww1;
  foreach(p; w1) {
    if (p in ww1) {
    } else {
      ww1[p] = p.step;
    }
  }

  // wire 2 ---
  segmentStartingPoint = Point(0,0);
  initialStep = 1;
  
  Wire w2;
  foreach(move; moves[1]) {
    w2 ~= moveToWireSegment(segmentStartingPoint, move, initialStep);
    segmentStartingPoint = w2[$-1];
    initialStep = segmentStartingPoint.step + 1;
  }
  uint[Point] ww2;
  foreach(p; w2) {
    if (p in ww2) {
    } else {
      ww2[p] = p.step;
    }
  }

  // find intersections ---
  uint[Point] intersections;
  foreach(key; ww1.keys) {
    if (key in ww2) {
      intersections[key] = 0;
    }
  }

  // minimum steps ---
  uint minSteps = uint.max;
  foreach(point; intersections.keys) {
    minSteps = min(minSteps, ww1[point] + ww2[point]);
  }
  
  return minSteps;
}
unittest {
  auto moves = [["R8","U5","L5","D3"], ["U7","R6","D4","L4"]];
  assert(30 == part2(moves));
}
unittest {
  auto moves = [["R75","D30","R83","U83","L12","D49","R71","U7","L72"],
                ["U62","R66","U55","R34","D71","R55","D58","R83"]];
  assert(610 == part2(moves));
}
unittest {
  auto moves = [["R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"],
                ["U98","R91","D20","R16","D67","R40","U7","R15","U6","R7"]];
  assert(410 == part2(moves));
}

void main()
{
  immutable string[] puzzleData = import("2019-03").chomp.split("\n");
  // writefln("(puzzleData = %s)""", puzzleData);
  auto moves = [[""],[""]];
  moves[][0] = puzzleData[0].split(",");
  moves[][1] = puzzleData[1].split(",");
  // writefln("(moves = %s)", moves);
  
  {
    auto answer = part1(moves);
    writefln("Part1: (answer = %s)", answer);
    assert(answer == 860);
  }
  {
    auto answer = part2(moves);
    writefln("Part2: (answer = %s)", answer);
    assert(answer == 9238);
  }
}

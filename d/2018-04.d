module aoc2018day4;

import std.datetime : Duration, Interval;
import std.datetime.date : DateTime;

version(unittest) {
  string[] watchStringRecords_ut =
    [
     "[1518-11-01 00:00] Guard #10 begins shift",
     "[1518-11-01 00:05] falls asleep",
     "[1518-11-01 00:25] wakes up",
     "[1518-11-01 00:30] falls asleep",
     "[1518-11-01 00:55] wakes up",
     "[1518-11-01 23:58] Guard #99 begins shift",
     "[1518-11-02 00:40] falls asleep",
     "[1518-11-02 00:50] wakes up",
     "[1518-11-03 00:05] Guard #10 begins shift",
     "[1518-11-03 00:24] falls asleep",
     "[1518-11-03 00:29] wakes up",
     "[1518-11-04 00:02] Guard #99 begins shift",
     "[1518-11-04 00:36] falls asleep",
     "[1518-11-04 00:46] wakes up",
     "[1518-11-05 00:03] Guard #99 begins shift",
     "[1518-11-05 00:45] falls asleep",
     "[1518-11-05 00:55] wakes up",
     ];

  Watch[] watchRecords_ut =
    [
     Watch(DateTime(1518, 11, 01, 00, 00, 0), "Guard #10 begins shift"),
     Watch(DateTime(1518, 11, 01, 00, 05, 0), "falls asleep"),
     Watch(DateTime(1518, 11, 01, 00, 25, 0), "wakes up"),
     Watch(DateTime(1518, 11, 01, 00, 30, 0), "falls asleep"),
     Watch(DateTime(1518, 11, 01, 00, 55, 0), "wakes up"),
     Watch(DateTime(1518, 11, 01, 23, 58, 0), "Guard #99 begins shift"),
     Watch(DateTime(1518, 11, 02, 00, 40, 0), "falls asleep"),
     Watch(DateTime(1518, 11, 02, 00, 50, 0), "wakes up"),
     Watch(DateTime(1518, 11, 03, 00, 05, 0), "Guard #10 begins shift"),
     Watch(DateTime(1518, 11, 03, 00, 24, 0), "falls asleep"),
     Watch(DateTime(1518, 11, 03, 00, 29, 0), "wakes up"),
     Watch(DateTime(1518, 11, 04, 00, 02, 0), "Guard #99 begins shift"),
     Watch(DateTime(1518, 11, 04, 00, 36, 0), "falls asleep"),
     Watch(DateTime(1518, 11, 04, 00, 46, 0), "wakes up"),
     Watch(DateTime(1518, 11, 05, 00, 03, 0), "Guard #99 begins shift"),
     Watch(DateTime(1518, 11, 05, 00, 45, 0), "falls asleep"),
     Watch(DateTime(1518, 11, 05, 00, 55, 0), "wakes up"),
     ];

  GuardsSleepTimes guardsSleepTimeRecords_ut;

  shared static this() {
    guardsSleepTimeRecords_ut =
      [10:
       [
        Interval!DateTime(DateTime(1518, 11, 01, 00, 05, 0),
                          DateTime(1518, 11, 01, 00, 25, 0)),
        Interval!DateTime(DateTime(1518, 11, 01, 00, 30, 0),
                          DateTime(1518, 11, 01, 00, 55, 0)),
        Interval!DateTime(DateTime(1518, 11, 03, 00, 24, 0),
                          DateTime(1518, 11, 03, 00, 29, 0)),
        ],
       99:
       [
        Interval!DateTime(DateTime(1518, 11, 02, 00, 40, 0),
                          DateTime(1518, 11, 02, 00, 50, 0)),
        Interval!DateTime(DateTime(1518, 11, 04, 00, 36, 0),
                          DateTime(1518, 11, 04, 00, 46, 0)),
        Interval!DateTime(DateTime(1518, 11, 05, 00, 45, 0),
                          DateTime(1518, 11, 05, 00, 55, 0)),
        ],
       ];
  }
}

struct Watch {
  DateTime date;
  string rawData;

  int opCmp()(auto ref const Watch rhs) const {
    if (date == rhs.date) return 0;
    return date < rhs.date ? -1 : +1;
  }
}

Watch toWatch(string record)
pure
{
  import std.conv : to;

  auto dt = DateTime(to!int(record[1 .. 5]),
                     to!int(record[6 .. 8]),
                     to!int(record[9 .. 11]),
                     to!int(record[12 .. 14]),
                     to!int(record[15 .. 17]),
                     0);
  return Watch(dt, record[19 .. $]);
} unittest {
  Watch w = watchStringRecords_ut[0].toWatch;
  assert(w == Watch(DateTime(1518, 11, 1, 0, 0, 0), "Guard #10 begins shift"));
}

alias DateTimeIntervalArray = Interval!(DateTime)[];
alias GuardsSleepTimes = DateTimeIntervalArray[int];

GuardsSleepTimes toGuardsSleepTimes(Watch[] watches)
{
  import std.conv : to;
  import std.regex : regex, matchAll;

  auto guardIdRegex = regex(r"Guard #(\d+) begins shift");

  GuardsSleepTimes guards;

  int guardId;
  DateTime fallsAsleep;
  DateTime wakesUp;

  foreach(w; watches) {
    if (w.rawData == "falls asleep") {
      fallsAsleep = w.date;
    } else if (w.rawData == "wakes up") {
      wakesUp = w.date;
      guards[guardId] ~= Interval!DateTime(fallsAsleep, wakesUp);
    } else {
      auto m = matchAll(w.rawData, guardIdRegex);
      guardId = to!int(m.front[1]);
      if (guardId in guards) {
      } else {
        guards[guardId] = [];
      }
    }
  }

  return guards;
} unittest {
  auto guardsSleepTimes = watchRecords_ut.toGuardsSleepTimes;
  assert(guardsSleepTimes == guardsSleepTimeRecords_ut);
}

alias GuardsTotalSleepTimes = Duration[int];

GuardsTotalSleepTimes toGuardsTotalSleepTimes(GuardsSleepTimes guardsSleepTimes)
pure
{
  import std.algorithm : map, sum;
  import std.array : array;

  GuardsTotalSleepTimes totals;

  foreach (id, sleepTimes; guardsSleepTimes) {
    auto totalSleepTime = sleepTimes
      .map!(a => a.length)
      .array
      .sum(Duration.zero);
    totals[id] = totalSleepTime;
  }
  return totals;
} unittest {
  import std.datetime : minutes;
  auto guardsTotalSleepTimes = toGuardsTotalSleepTimes(guardsSleepTimeRecords_ut);
  assert(guardsTotalSleepTimes ==
         [
          10: 50.minutes,
          99: 30.minutes
          ]
         );
}

alias GuardSleepMinutes = int[int];

// What minutes of an hour does the guard is asleep
GuardSleepMinutes toGuardSleepMinutes(DateTimeIntervalArray guardSleepTimes)
{
  import std.algorithm : each;
  import std.datetime : minutes;

  GuardSleepMinutes sleepMinutes;

  foreach(interval; guardSleepTimes) {
    auto intervalRange = interval.fwdRange(dt => dt + 1.minutes);
    intervalRange.each!((dt) {
        if (dt.minute in sleepMinutes) {
          ++sleepMinutes[dt.minute];
        } else {
          sleepMinutes[dt.minute] = 1;
        }
      });
  }

  return sleepMinutes;
} unittest {
  auto sleepMinutes = toGuardSleepMinutes(guardsSleepTimeRecords_ut[10]);
  assert(sleepMinutes == [45:1, 26:1, 52:1, 43:1, 17:1, 34:1, 51:1, 42:1, 15:1, 24:2, 37:1, 19:1, 6:1, 46:1, 28:1, 33:1, 53:1, 10:1, 44:1, 8:1, 35:1, 13:1, 30:1, 39:1, 48:1, 47:1, 21:1, 38:1, 40:1, 12:1, 14:1, 23:1, 32:1, 5:1, 22:1, 31:1, 49:1, 54:1, 41:1, 50:1, 20:1, 7:1, 11:1, 16:1, 27:1, 25:1, 36:1, 18:1, 9:1]);
}

int part1(GuardsSleepTimes guardsSleepTimes)
{
  import std.algorithm : maxElement;
  import std.array : byPair;

  // find the sleepiest guard
  auto guardsTotalSleepTimes = guardsSleepTimes.toGuardsTotalSleepTimes;
  auto sleepiestGuardId = guardsTotalSleepTimes
    .byPair
    .maxElement!(a => a.value)
    .key;

  // find the sleepiest minute
  auto sleepMinutes = guardsSleepTimes[sleepiestGuardId].toGuardSleepMinutes;
  auto sleepiestMinute = sleepMinutes
    .byPair
    .maxElement!(a => a.value)
    .key;

  return sleepiestGuardId * sleepiestMinute;
} unittest {
  auto sleepyNumber = part1(guardsSleepTimeRecords_ut);
  assert(sleepyNumber == 10 * 24);
}

int part2(GuardsSleepTimes guardsSleepTimes)
{
  import std.algorithm : maxElement;
  import std.array : byPair;
  import std.typecons : Tuple;

  // find the sleepiest minute of all guards
  alias GuardSleepiestMinute =
    Tuple!(int, "guardId", int, "minute", int, "count");
  GuardSleepiestMinute[] guardsSleepiestMinute;
  foreach (guardId, sleepTimes; guardsSleepTimes) {
    auto sleepMinutes = sleepTimes.toGuardSleepMinutes;
    if (sleepMinutes.length > 0) {
      auto sleepiestMinute = sleepMinutes
        .byPair
        .maxElement!(a => a.value);

      guardsSleepiestMinute ~= GuardSleepiestMinute(guardId,
                                                    sleepiestMinute.key,
                                                    sleepiestMinute.value);
    }
  }

  auto sleepiestGuardByMinute = guardsSleepiestMinute
    .maxElement!(a => a.count);

  return sleepiestGuardByMinute.guardId * sleepiestGuardByMinute.minute;

} unittest {
  auto sleepyNumber = part2(guardsSleepTimeRecords_ut);
  assert(sleepyNumber == 99 * 45);
}

void main()
{
  import aoc2018 : getStrings;
  import std.algorithm : map, sort;
  import std.array : array;
  import std.stdio : writefln;

  string[] watchStringRecords = getStrings("../inputs/2018-04");
  Watch[] sortedWatchRecords = watchStringRecords
    .map!toWatch
    .array
    .sort
    .array;
  GuardsSleepTimes guardsSleepTimes = sortedWatchRecords.toGuardsSleepTimes;

  {
    auto answer = part1(guardsSleepTimes);
    writefln("Part1: (answer = %s)", answer);
    assert(answer == 8950);
  }
  {
    auto answer = part2(guardsSleepTimes);
    writefln("Part2: (answer = %s)", answer);
    assert(answer == 78452);
  }
}

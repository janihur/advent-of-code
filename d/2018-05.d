module aoc2018day5;

debug import std.stdio : writefln, writeln, write;

immutable char[] unitTypes =
  ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
   'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

int[string] reactives;
shared static this() {
  reactives = [
               "aA": 1, "Aa": 1,
               "bB": 1, "Bb": 1,
               "cC": 1, "Cc": 1,
               "dD": 1, "Dd": 1,
               "eE": 1, "Ee": 1,
               "fF": 1, "Ff": 1,
               "gG": 1, "Gg": 1,
               "hH": 1, "Hh": 1,
               "iI": 1, "Ii": 1,
               "jJ": 1, "Jj": 1,
               "kK": 1, "Kk": 1,
               "lL": 1, "Ll": 1,
               "mM": 1, "Mm": 1,
               "nN": 1, "Nn": 1,
               "oO": 1, "Oo": 1,
               "pP": 1, "Pp": 1,
               "qQ": 1, "Qq": 1,
               "rR": 1, "Rr": 1,
               "sS": 1, "Ss": 1,
               "tT": 1, "Tt": 1,
               "uU": 1, "Uu": 1,
               "vV": 1, "Vv": 1,
               "wW": 1, "Ww": 1,
               "xX": 1, "Xx": 1,
               "yY": 1, "Yy": 1,
               "zZ": 1, "Zz": 1,
               ];
}

void clean(ref char[] polymer, char unit)
{
  import std.algorithm : remove;
  import std.ascii : toLower, toUpper;
  
  polymer = polymer.remove!(a => a == unit.toLower || a == unit.toUpper);
} unittest {
  char[] polymer;

  polymer = cast(char[])['A', 'a'];
  clean(polymer, 'a');
  assert(polymer == []);

  polymer = cast(char[])['a', 'b', 'B', 'A'];
  clean(polymer, 'B');
  assert(polymer == ['a', 'A']);
}

void react(ref char[] polymer)
{
  import std.algorithm : remove;
  
  size_t i = 0;
  
  do {
    immutable size_t iNext = i + 1;
    immutable(char)[] adjacentUnits = [polymer[i]];

    if (iNext < polymer.length) {
      adjacentUnits ~= polymer[iNext];
    }
    
    debug write(i, ": ", adjacentUnits);

    if (adjacentUnits in reactives) {
      polymer = polymer.remove(i, iNext);
      debug write(": polymer.length: ", polymer.length);
      // don't underflow
      if (i > 0) --i;
    } else {
      ++i;
    }
    
    debug writeln;
  } while (i < polymer.length);
} unittest {
  char[] polymer;

  polymer = cast(char[])['A', 'a'];
  react(polymer);
  assert(polymer == []);

  polymer = cast(char[])['a', 'b', 'B', 'A'];
  react(polymer);
  assert(polymer == []);

  polymer = cast(char[])['a', 'b', 'A', 'B'];
  react(polymer);
  assert(polymer == ['a', 'b', 'A', 'B']);

  polymer = cast(char[])['a', 'a', 'b', 'A', 'A', 'B'];
  react(polymer);
  assert(polymer == ['a', 'a', 'b', 'A', 'A', 'B']);

  polymer = cast(char[])['d', 'a', 'b', 'A', 'c', 'C', 'a', 'C', 'B', 'A', 'c', 'C', 'c', 'a', 'D', 'A'];
  react(polymer);
  assert(polymer == ['d', 'a', 'b', 'C', 'B', 'A', 'c', 'a', 'D', 'A']);

  polymer = cast(char[])['A', 'a', 'A', 'B', 'a', 'C', 'c'];
  react(polymer);
  assert(polymer == ['A', 'B', 'a']);
  
  polymer = cast(char[])['a', 'b', 'c', 'd', 'e', 'f', 'g', 'G', 'F', 'E', 'D', 'C', 'B', 'A'];
  react(polymer);
  assert(polymer == []);
}

ulong part1(char[] polymer)
{
  react(polymer);
  return polymer.length;
} unittest {
  ulong length;
  
  length = part1("Aa".dup);
  assert(length == 0);

  length = part1("abBA".dup);
  assert(length == 0);

  length = part1("abAB".dup);
  assert(length == 4);

  length = part1("aabAAB".dup);
  assert(length == 6);

  length = part1("dabAcCaCBAcCcaDA".dup);
  assert(length == 10);

  length = part1("AaABaCc".dup);
  assert(length == 3);
  
  length = part1("abcdefgGFEDCBA".dup);
  assert(length == 0);
}

ulong part2(char[] polymer)
{
  import std.algorithm : minElement;
  import std.array : byPair;

  // calculate all polymers
  ulong[char] polymerLengthWithoutUnit;
  foreach (unit; unitTypes) {
    char[] polymer2 = polymer.dup;
    clean(polymer2, unit);
    react(polymer2);
    polymerLengthWithoutUnit[unit] = polymer2.length;
  }

  // find the shortest polymer
  auto shortest = polymerLengthWithoutUnit
    .byPair
    .minElement!(a => a.value);

  return shortest.value;
} unittest {
  ulong length = part2("dabAcCaCBAcCcaDA".dup);
  assert(length == 4);
}

void main()
{
  import std.stdio : writefln;

  auto polymer = cast(char[])import("2018-05");
  
  {
    auto answer = part1(polymer.dup);
    writefln("Part1: (answer = %s)", answer);
    assert(answer == 11118);
  }
  {
    auto answer = part2(polymer.dup);
    writefln("Part2: (answer = %s)", answer);
    assert(answer == 6948);
  }
}

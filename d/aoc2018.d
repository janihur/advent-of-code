int[] getIntegers(string inputFilePath)
out(result) {
  // conceptually not perfect contract because depends on user input, but in
  // this case the input can be considered as a part of the program
  assert(result.length > 0);
} do {
  import std.algorithm : map;
  import std.array : array;
  import std.conv : to;
  import std.stdio : File;

  return File(inputFilePath)
    .byLine
    .map!(to!int)
    .array;
}

string[] getStrings(string inputFilePath)
out(result) {
  // conceptually not perfect contract because depends on user input, but in
  // this case the input can be considered as a part of the program
  assert(result.length > 0);
} do {
  import std.algorithm : map;
  import std.array : array;
  import std.conv : to;
  import std.stdio : File;

  return File(inputFilePath)
    .byLine
    .map!(to!string)
    .array;
}

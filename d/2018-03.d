module aoc2018day3;

version(unittest) {
  import std.conv : to;
  import std.stdio : writeln, writefln;
}

struct Claim {
  int id;
  int x; // top left
  int y; // top left
  int width; // from left to right
  int height; // from top to bottom
}

Claim makeClaim(in string claim)
{
  import std.conv : to;
  import std.regex : regex, matchAll;

  auto r = regex(r"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)");
  auto m = matchAll(claim, r);
  auto id = to!int(m.front[1]);
  auto x = to!int(m.front[2]);
  auto y = to!int(m.front[3]);
  auto width = to!int(m.front[4]);
  auto height = to!int(m.front[5]);

  return Claim(id, x, y, width, height);
} unittest {
  Claim claim;

  claim = makeClaim("#1 @ 1,3: 4x4");
  assert(claim == Claim(1, 1, 3, 4, 4));

  claim = makeClaim("#49 @ 5,782: 24x16");
  assert(claim == Claim(49, 5, 782, 24, 16));

  claim = makeClaim("#1340 @ 952,280: 19x26");
  assert(claim == Claim(1340, 952, 280, 19, 26));
}

void positionClaimOnFabric(in Claim claim, ref ushort[][] fabric)
{
  foreach(x; claim.x .. claim.x + claim.width) {
    foreach(y; claim.y .. claim.y + claim.height) {
      ++fabric[y][x];
    }
  }
} unittest {
  auto fabric = new ushort[][](8, 8);

  positionClaimOnFabric(Claim(1, 1, 3, 4, 4), fabric);
  positionClaimOnFabric(Claim(2, 3, 1, 4, 4), fabric);
  positionClaimOnFabric(Claim(3, 5, 5, 2, 2), fabric);

  assert(fabric == [[0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 1, 1, 1, 1, 0],
                    [0, 0, 0, 1, 1, 1, 1, 0],
                    [0, 1, 1, 2, 2, 1, 1, 0],
                    [0, 1, 1, 2, 2, 1, 1, 0],
                    [0, 1, 1, 1, 1, 1, 1, 0],
                    [0, 1, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0]]);
}

bool isNonOverlappingClaim(in Claim claim, ref ushort[][] fabric)
{
  foreach(x; claim.x .. claim.x + claim.width) {
    foreach(y; claim.y .. claim.y + claim.height) {
      if (fabric[y][x] > 1) {
        return false;
      }
    }
  }
  return true;
} unittest {
  Claim[] claims = [Claim(1, 1, 3, 4, 4),
                    Claim(2, 3, 1, 4, 4),
                    Claim(3, 5, 5, 2, 2)];

  ushort[][] fabric = [[0, 0, 0, 0, 0, 0, 0, 0],
                       [0, 0, 0, 1, 1, 1, 1, 0],
                       [0, 0, 0, 1, 1, 1, 1, 0],
                       [0, 1, 1, 2, 2, 1, 1, 0],
                       [0, 1, 1, 2, 2, 1, 1, 0],
                       [0, 1, 1, 1, 1, 1, 1, 0],
                       [0, 1, 1, 1, 1, 1, 1, 0],
                       [0, 0, 0, 0, 0, 0, 0, 0]];

  auto id = part2(claims, fabric);

  assert(id == 3);
}

ulong part1(ref Claim[] claims, ref ushort[][] fabric)
{
  import std.algorithm : count, each;

  claims.each!(a => positionClaimOnFabric(a, fabric));

  int overlappingArea = 0;
  fabric.each!(y => overlappingArea += y.count!(a => a > 1));

  return overlappingArea;
} unittest {
  Claim[] claims = [Claim(1, 1, 3, 4, 4),
                    Claim(2, 3, 1, 4, 4),
                    Claim(3, 5, 5, 2, 2)];

  auto fabric = new ushort[][](8, 8);

  auto overlappingArea = part1(claims, fabric);

  assert(overlappingArea == 4);
}

int part2(ref Claim[] claims, ref ushort[][] fabric)
{
  int id;

  foreach(claim; claims) {
    if (isNonOverlappingClaim(claim, fabric)) {
      id = claim.id;
      break;
    }
  }

  return id;
} unittest {
  Claim[] claims = [Claim(1, 1, 3, 4, 4),
                    Claim(2, 3, 1, 4, 4),
                    Claim(3, 5, 5, 2, 2)];

  ushort[][] fabric = [[0, 0, 0, 0, 0, 0, 0, 0],
                       [0, 0, 0, 1, 1, 1, 1, 0],
                       [0, 0, 0, 1, 1, 1, 1, 0],
                       [0, 1, 1, 2, 2, 1, 1, 0],
                       [0, 1, 1, 2, 2, 1, 1, 0],
                       [0, 1, 1, 1, 1, 1, 1, 0],
                       [0, 1, 1, 1, 1, 1, 1, 0],
                       [0, 0, 0, 0, 0, 0, 0, 0]];

  auto nonOverlappingId = part2(claims, fabric);

  assert(nonOverlappingId == 3);
}

void main()
{
  import aoc2018 : getStrings;
  import std.algorithm : map;
  import std.array : array;
  import std.stdio : writefln;

  string[] claimStrings = getStrings("../inputs/2018-03");

  auto claims = claimStrings.map!(a => makeClaim(a)).array;

  auto fabric = new ushort[][](1000, 1000);

  {
    auto answer = part1(claims, fabric);
    writefln("Part1: (answer = %s)", answer);
    assert(answer == 115348);
  }

  {
    auto answer = part2(claims, fabric);
    writefln("Part2: (answer = %s)", answer);
    assert(answer == 188);
  }
}

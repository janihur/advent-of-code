module aoc2018day2;

import std.typecons: Tuple;

version(unittest) {
  import std.conv: to;
}

int part1(string[] boxIds)
pure
out(result) {
  assert(result >= 0);
} do {
  import std.algorithm;
  import std.array;
  import std.stdio: writefln;

  alias Accu = Tuple!(uint, "two", uint, "three");
  Accu accu;

  foreach(idOriginal; boxIds) {
    auto idSorted = sort(idOriginal.array);
    auto groups = idSorted.group.assocArray;
    auto values = groups.values;
    bool two = any!(a => a == 2)(values);
    bool three = any!(a => a == 3)(values);
    if (two) accu.two++;
    if (three) accu.three++;
    debug {
      writefln("Box: (idOriginal = %s)", idOriginal);
      writefln("     (idSorted = %s)",   idSorted);
      writefln("     (groups = %s)",     groups);
      writefln("     (values = %s)",     values);
      writefln("     (two = %s)",        two);
      writefln("     (three = %s)",      three);
      writefln("     (accu = %s)",       accu);
    }
  }
  return accu.two * accu.three;
} unittest {
  int answer = part1(["abcdef",
                      "bababc",
                      "abbcde",
                      "abcccd",
                      "aabcdd",
                      "abcdee",
                      "ababab"]);
  assert(answer == 12, "Got: " ~ to!string(answer));
}

alias Match = Tuple!(bool, "isMatch", char[], "matchString");

Match match(string str1, string str2)
pure
in {
  assert(str1.length > 0);
  assert(str1.length == str2.length);
} do {
  int matches;
  char[] matchString;

  foreach (i, c1; str1) {
    if (c1 != str2[i]) {
      matches++;
    } else {
      matchString ~= str2[i];
    }

    if (matches > 1) break;
  }

  Match result;
  result.isMatch = false;

  if (matches == 1) {
    result.isMatch = true;
    result.matchString = matchString;
  }

  return result;
} unittest {
  Match answer;

  answer = match("a", "a");
  assert(answer.isMatch == false);

  answer = match("a", "b");
  assert(answer.isMatch == true);
  assert(answer.matchString == "");

  answer = match("abcde", "fghij");
  assert(answer.isMatch == false);

  answer = match("fghij", "fguij");
  assert(answer.isMatch == true);
  assert(answer.matchString == "fgij");
}

Match part2(string[] boxIds)
pure
{
  Match result;
 out_: foreach(i, id; boxIds) {
    foreach(j, id2; boxIds[i+1 .. $]) {
      result = match(id, id2);
      debug {
        import std.stdio: writefln;
        writefln("Box(%d): (id = %s)", i, id);
        writefln("comparing to: (j = %d)(id = %s)", j, id2);
        writefln("match: (isMatch = %s)(matchString = %s)", result.isMatch, result.matchString);
      }
      if (result.isMatch) break out_;
    }
  }
  return result;
} unittest {
  Match answer = part2(["abcde",
                        "fghij",
                        "klmno",
                        "pqrst",
                        "fguij",
                        "axcye",
                        "wvxyz"]);
  assert(answer.isMatch == true);
  assert(answer.matchString == "fgij", "Got: " ~ answer.matchString);
}

void main()
{
  import aoc2018: getStrings;
  import std.stdio: writefln;

  string[] input = getStrings("../inputs/2018-02");

  {
    int answer = part1(input);
    writefln("Part1: (answer = %d)", answer);
    assert(answer == 7134);
  }
  {
    Match answer = part2(input);
    writefln("Part2: (answer = %s)", answer.matchString);
    assert(answer.matchString == "kbqwtcvzhmhpoelrnaxydifyb");
  }
}

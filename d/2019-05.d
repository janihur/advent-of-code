module aoc19day05;

import std.algorithm : map;
import std.array : array;
import std.conv : to;
import std.stdio : writefln;
import std.string : chomp, split;

import intcode19v2;

int part1(immutable int[] program, int[] input)
{
  auto computer = IntCode19(program, input);
  computer.run;
  debug writefln("(computer.output = %s)", computer.output);
  // diagnostic test results (0 is ok)
  assert(computer.output[0 .. $-1] == [0, 0, 0, 0, 0, 0, 0, 0, 0]);
  // diagnostic code
  return computer.output[$-1];
}

int part2(immutable int[] program, int[] input)
{
  auto computer = IntCode19(program, input);
  computer.run;
  debug writefln("(computer.output = %s)", computer.output);
  return computer.output[0];
} unittest {
}

void main()
{
  immutable string[] puzzleData = import("2019-05").chomp.split(",");
  immutable int[] program = puzzleData.map!(to!int).array;
  // writefln("(program = %s)", program);

  {
    auto answer = part1(program, [1]);
    writefln("Part1: (answer = %s)", answer);
    assert(answer == 13978427);
  }
  {
    auto answer = part2(program, [5]);
    writefln("Part2: (answer = %s)", answer);
    assert(answer == 11189491);
  }
}

# My Advent of Code with Different Languages

I do the puzzles also when the actual competition has been over. For some weird reason I end up solving the puzzles with niche languages that are new for me :)

The languages are:

* [Ballerina](https://ballerina.io/)
* [D](https://dlang.org/)
* [Emacs Lisp](https://www.gnu.org/software/emacs/manual/html_node/elisp/) (Elisp)
* [Elixir](https://elixir-lang.org/)
* InterSystems [ObjectScript](https://docs.intersystems.com/irislatest/csp/docbook/DocBook.UI.Page.cls?KEY=PAGE_objectscript)
* [V](https://vlang.io/)

## Highlights Since 2021

* [Advent of Code 2024 Highlights](2024.md)
* [Advent of Code 2023 Highlights](2023.md)
* [Advent of Code 2022 Highlights](2022.md)
* [Advent of Code 2021 Highlights](2021.md)

## Highlights of 2020

I run into weird error in day 11 with Ballerina (I was using Swan Lake Preview 5) that might even be a compiler bug. I asked about the problem in [Stack Overflow](https://stackoverflow.com/q/65357940/272735) without success. Because I was unable to work it over I had to switch the language. I picked V and succeeded to solve both parts essentially the same way than I tried with Ballerina.

Day 13 part 2 was the showstopper for me this year. I don't understand [Chinese remainder theorem](https://en.wikipedia.org/wiki/Chinese_remainder_theorem) at all, so the state-of-the-art solution is not for me. However I read about one alternative solution that looks very similar to what I tried and might try to apply that later.

## Highlights of 2019

I found intcode computer fun as it was not too difficult.

After reading a quite much about basic trigonometry I finally managed to solve day 10 in mid-January. I got the hint that lead me to the correct path by reading the others' solutions.

## Highlights of 2018

First 6 days originally done in Dec 2018 - Mar 2019 with D. Elixir solutions added in 2023. ObjectScript solutions added in 2024.

### Day 5 with Elixir (March 2023)

As I was just learning Elixir I had a lot of troubles to find the working functional implementation. I couldn't have solved this without consulting the [solution thread](https://elixirforum.com/t/advent-of-code-day-5/) in [Elixir forum](https://elixirforum.com/). The thread has still solutions that I don't fully understand.

One trick I have learned during my AoC years is that in [ASCII](https://en.wikipedia.org/wiki/ASCII) the difference of upper and lowercase letters is always 32. E.g. A = 65, a = 97 and thus `abs(65 - 97) = 32`.

## Completed Puzzles

The day is marked as solved (`X`) only if both parts have been solved.

|2024  |ObjectScript|
|------|:----------:|
|Day  7|`X`|
|Day  6|`X`|
|Day  5|`X`|
|Day  4|`X`|
|Day  3|`X`|
|Day  2|`X`|
|Day  1|`X`|

|2023  |Ballerina|
|------|:-------:|
|Day  4|`X`|
|Day  3|`1)`|
|Day  2|`X`|
|Day  1|`X`|

1\) Part 2 skipped because a compiler [bug](https://github.com/ballerina-platform/ballerina-lang/issues/41819).

|2022  |Ballerina|
|------|:-------:|
|Day 10|`X`|
|Day  9|`X`|
|Day  8|`X`|
|Day  7|`X`|
|Day  6|`X`|
|Day  5|`X`|
|Day  4|`X`|
|Day  3|`X`|
|Day  2|`X`|
|Day  1|`X`|

|2021  |Ballerina|
|------|:-------:|
|Day 10|`X`|
|Day  9|`X`|
|Day  8|`X`|
|Day  7|`X`|
|Day  6|`X`|
|Day  5|`X`|
|Day  4|`X`|
|Day  3|`X`|
|Day  2|`X`|
|Day  1|`X`|

|2020  |Ballerina|V  |
|------|:-------:|:-:|
|Day 12||`X`|
|Day 11||`X`|
|Day 10|`X`||
|Day 9 |`X`||
|Day 8 |`X`||
|Day 7 |`X`||
|Day 6 |`X`||
|Day 5 |`X`||
|Day 4 |`X`||
|Day 3 |`X`||
|Day 2 |`X`||
|Day 1 |`X`||

|2019  |Ballerina|D  |
|------|:-------:|:-:|
|Day 10||`X`|
|Day 9 ||`X`|
|Day 8 ||`X`|
|Day 7 ||`X`|
|Day 6 ||`X`|
|Day 5 ||`X`|
|Day 4 |`X`||
|Day 3 ||`X`|
|Day 2 |`X`|`X`|
|Day 1 |`X`||

|2018  |Ballerina|D  |Elisp|Elixir|ObjectScript|
|------|:-------:|:-:|:---:|:----:|:----------:|
|Day 6 ||`X`||`X`|
|Day 5 ||`X`||`X`|`X`|
|Day 4 ||`X`||`X`|`X`|
|Day 3 ||`X`||`X`|`X`|
|Day 2 ||`X`|`X`|`X`|`X`|
|Day 1 |`X`|`X`|`X`|`X`|`X`|

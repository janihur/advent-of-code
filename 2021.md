# Advent of Code 2021 Highlights

The used language is [Ballerina](https://ballerina.io/) [Swan Lake Beta 4](https://ballerina.io/downloads/swan-lake-release-notes/swan-lake-beta4/).

On day 2 I figured a lot simpler way to read text file. Ballerina standard library has included new handy functions to [io-module](https://lib.ballerina.io/ballerina/io/1.0.1):

* `fileReadLines`
* `fileReadCsv`

On day 3 part 2 I didn't read the instructions careful enough so I wasted a lot of time. On day 4 part 1 I had a persistent bug - x and y coordinates where mixed when accessing two dimensional array (was `arr[x][y]` when should have `arr[y][x]`). I categorised that as a typo-bug. After those two days it was a relief that days 5 and 6 were smooth rides ! So far I have had to write a bunch of utility functions missing from Ballerina standard library. The quality of the utility functions is questionable but they get the job done for the puzzles !

On day 7 I figured out quickly that the optimal way is to somehow figure out the correct horizontal position and only then calculate the fuel consumption for that position. I simply printed fuel consumptions for all positions using given example data:

```
POS PART1 PART2
--- ----- -----
  0    49   290
  1    41   242
  2    37   206 <- correct answer for part 1
  3    39   183
  4    41   170
  5    45   168 <- correct answer for part 2
  6    49   176
  7    53   194
  8    59   223
  9    65   262
 10    71   311
 11    77   370
 12    83   439
 13    89   518
 14    95   607
 15   103   707
 16   111   817
```

When I stared the horizontal positions (sorted below):

```
             
0 1 1 2 2 2 4 7 14 16
        ^ ^ ^
        | | |
         \  --- average (4.9)
          ----- median (2)
```

and the printed results I simply realised that for part 1 the answer seems to be [the median](https://en.wikipedia.org/wiki/Median). I was a little bit of shocked when that turned to be the correct answer ! No idea why it works but happy with that.

I also see from the example data results that there is only one local minimum so for part 2 I simply reduced the problem area:

* starting from median (POS) check POS-1 and POS+1 to see in what direction the fuel consumption is increasing and decreasing.
* go to the direction of decreasing fuel consumption.
* the lowest fuel consumption level is the last position before the consumption starts to increase again.

Only later when I was browsing the internet I realised that for part 2 the key is to use [the average](https://en.wikipedia.org/wiki/Arithmetic_mean) (aka mean). The example data tells that too if you only know to look for it. No idea why average works.

Day 8 was an industrial effort. First I didn't understood he problem description at all. So I had to read the Internet to figure it out. I only understood the problem description when I read others' solutions for part 1. When I realised how the part 1 can be solved I understood the problem and could figure out a reasonable (but not optimal) solution for part 2. Too bad all this took more than a week.

Luckily after day 8 days 9 and 10 where again relatively easy except I had quite a few logic errors in my day 9 part 2 traversing logic. At this point we were already past Christmas so it might be that I won't do too many puzzles anymore.

Bugs by type:

|Day|Logic Errors|Off-by-one|Typos|
|---|------------|----------|-----|
|1|1|1|0|
|2|0|0|0|
|3|1|0|0|
|4|0|0|1|
|5|0|0|0|
|6|0|0|0|
|7|0|0|0|
|8|0|0|0|
|9|3|0|0|
|10|0|0|0|
